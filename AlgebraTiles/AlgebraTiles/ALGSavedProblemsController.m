//
//  ALGSavedProblemsController.m
//  AlgebraTiles
//
//  Created by Reese McLean on 3/27/14.
//  Copyright (c) 2014 Reese McLean. All rights reserved.
//

#import "ALGSavedProblemsController.h"

#import "ALGSavedProblem.h"

@implementation ALGSavedProblemsController

+(instancetype) sharedController {
    static dispatch_once_t once;
    static ALGSavedProblemsController *instance;
    dispatch_once(&once, ^{
        instance = [[ALGSavedProblemsController alloc] init];
    });
    return instance;
}

-(instancetype) init {
    
    self = [super init];
    if (self) {
        NSString *path = [self savePath];
        if (path) {
            
            if ([[NSFileManager defaultManager] fileExistsAtPath:path]) {
                _savedProblems = [[NSKeyedUnarchiver unarchiveObjectWithFile:path] mutableCopy];
            }
            
            if (!_savedProblems) {
                _savedProblems = [NSMutableArray array];
            }
            
        };
    }

    return self;
}

-(NSString *)savePath {
    NSArray *paths = NSSearchPathForDirectoriesInDomains( NSDocumentDirectory,    NSUserDomainMask ,YES );
    NSString *documentsDirectory = [paths firstObject];
    NSString *savePath = [documentsDirectory stringByAppendingPathComponent:@"data"];
    return savePath;
}

-(NSString *)previewImageSaveDirectory {
    NSArray *paths = NSSearchPathForDirectoriesInDomains( NSDocumentDirectory,    NSUserDomainMask ,YES );
    NSString *documentsDirectory = [paths firstObject];
    NSString *directoryPath = [documentsDirectory stringByAppendingPathComponent:@"previewImages"];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    BOOL isDir = YES;
    BOOL directoryExists = [fileManager fileExistsAtPath:directoryPath isDirectory:&isDir];
    if (!directoryExists) {
        NSError *error = nil;
        if (![fileManager createDirectoryAtPath:directoryPath withIntermediateDirectories:YES attributes:nil error:nil]) {
            NSLog(@"Error: %@", error);
        }
        
    }
    return directoryPath;
}

-(void) updateLastUsedDateForProblem:(ALGSavedProblem*)problem {
    problem.lastUsedDate = [NSDate date];
    [self saveToFile];
}

-(void) addProblem:(ALGSavedProblem*)problem {
    
    NSData *imageData = UIImagePNGRepresentation(problem.previewImage);
    NSError *imageSaveError = nil;
    if (![imageData writeToFile:problem.previewImageURL options:NSDataWritingAtomic error:&imageSaveError]) {
        NSLog(@"Could Not Save Image: %@", imageSaveError);
        return;
    }
    
    [self.savedProblems addObject:problem];
    [self saveToFile];
    
}

-(void) removeProblem:(ALGSavedProblem*)problem {
    
    if([[NSFileManager defaultManager] fileExistsAtPath:problem.previewImageURL]) {
        NSError *removeImageError = nil;
        if (![[NSFileManager defaultManager] removeItemAtURL:[NSURL fileURLWithPath:problem.previewImageURL] error:&removeImageError]) {
            NSLog(@"Could Not Remove Image: %@", removeImageError);
            return;
        }
    }
    
    [self.savedProblems removeObject:problem];
    [self saveToFile];
}

-(void) saveToFile {
    if (![NSKeyedArchiver archiveRootObject:self.savedProblems toFile:[self savePath]]) {
        NSLog(@"Could Not Save");
    }
}

@end
