//
//  ALGThemeExampleCell.m
//  AlgebraTiles
//
//  Created by Reese McLean on 3/27/14.
//  Copyright (c) 2014 Reese McLean. All rights reserved.
//

#import "ALGThemeExampleCell.h"

#import "ALGTileThemeManager.h"
#import "ALGTileTheme.h"
#import "ALGTileView.h"
#import "ALGTileModel.h"

@interface ALGThemeExampleCell ()

@property (strong, nonatomic) IBOutlet UILabel *nameLabel;
@property (strong, nonatomic) IBOutlet UIView *unitsContainerView;
@property (strong, nonatomic) IBOutlet UIView *negativeUnitsContainerView;
@property (strong, nonatomic) IBOutlet UIView *xContainerView;
@property (strong, nonatomic) IBOutlet UIView *yContainerView;
@property (strong, nonatomic) IBOutlet UIView *xyContainerView;
@property (strong, nonatomic) IBOutlet ALGTileView *unitsTileView;
@property (strong, nonatomic) IBOutlet ALGTileView *negativeUnitsTileView;
@property (strong, nonatomic) IBOutlet ALGTileView *xTileView;
@property (strong, nonatomic) IBOutlet ALGTileView *yTileView;
@property (strong, nonatomic) IBOutlet ALGTileView *xyTileView;

@end

@implementation ALGThemeExampleCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

-(void) awakeFromNib {
    [super awakeFromNib];
    
    //NEED TO LAYOUT IF I EVER CHANGE TILE SIZES
    ALGTileTheme *theme = [[ALGTileThemeManager sharedThemeManager] currentTheme];
    
    ALGTileModel *unitsTileModel = [ALGTileModel tileWithType:ALGTileTypeUnit];
    ALGTileView *unitsTilesView = [ALGTileView tileWithTileModel:unitsTileModel andTheme:theme];
    unitsTilesView.respondsToThemeChange = NO;
    self.unitsTileView = unitsTilesView;
    [self.unitsContainerView addSubview:unitsTilesView];
    
    ALGTileModel *negativeTileModel = [ALGTileModel tileWithType:ALGTileTypeUnit];
    negativeTileModel.negative = YES;
    ALGTileView *negativeTilesView = [ALGTileView tileWithTileModel:negativeTileModel andTheme:theme];
    negativeTilesView.respondsToThemeChange = NO;
    self.negativeUnitsTileView = negativeTilesView;
    [self.negativeUnitsContainerView addSubview:negativeTilesView];
    
    ALGTileModel *xTileModel = [ALGTileModel tileWithType:ALGTileTypeX];
    ALGTileView *xTileView = [ALGTileView tileWithTileModel:xTileModel andTheme:theme];
    xTileView.respondsToThemeChange = NO;
    self.xTileView = xTileView;
    [self.xContainerView addSubview:xTileView];
    
    ALGTileModel *yTileModel = [ALGTileModel tileWithType:ALGTileTypeY];
    ALGTileView *yTileView = [ALGTileView tileWithTileModel:yTileModel andTheme:theme];
    yTileView.respondsToThemeChange = NO;
    self.yTileView = yTileView;
    [self.yContainerView addSubview:yTileView];
    
    ALGTileModel *xyTileModel = [ALGTileModel tileWithType:ALGTileTypeXY];
    ALGTileView *xyTileView = [ALGTileView tileWithTileModel:xyTileModel andTheme:theme];
    xyTileView.respondsToThemeChange = NO;
    self.xyTileView = xyTileView;
    [self.xyContainerView addSubview:xyTileView];
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

//-(void) setHighlighted:(BOOL)highlighted animated:(BOOL)animated {
//    
//}

-(void) setTheme:(ALGTileTheme *)tileTheme {
    
    self.nameLabel.text = tileTheme.name;
    self.unitsTileView.tileTheme = tileTheme;
    self.negativeUnitsTileView.tileTheme = tileTheme;
    self.xTileView.tileTheme = tileTheme;
    self.yTileView.tileTheme = tileTheme;
    self.xyTileView.tileTheme = tileTheme;
    
}

@end
