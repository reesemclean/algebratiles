//
//  ALGSavedExpressionTerm.m
//  AlgebraTiles
//
//  Created by Reese McLean on 3/27/14.
//  Copyright (c) 2014 Reese McLean. All rights reserved.
//

#import "ALGSavedExpressionTerm.h"

NSString *const ALGTileTypeArchiveKey = @"ALGTileTypeArchiveKey";
NSString *const ALGNegativeArchiveKey = @"ALGNegativeArchiveKey";
NSString *const ALGCoefficientArchiveKey = @"ALGCoefficientArchiveKey";
NSString *const ALGCenterArchiveKey = @"ALGCenterArchiveKey";
NSString *const ALGVerticalArchiveKey = @"ALGVerticalArchiveKey";

@implementation ALGSavedExpressionTerm

-(instancetype) initWithCoder:(NSCoder *)aDecoder {
    
    self = [super init];
    if (self)
    {
        // Decode the property values by key, specifying the expected class
        _tileType = [[aDecoder decodeObjectOfClass:[NSNumber class] forKey:ALGTileTypeArchiveKey] intValue];
        _negative = [[aDecoder decodeObjectOfClass:[NSNumber class] forKey:ALGNegativeArchiveKey] boolValue];
        _vertical = [[aDecoder decodeObjectOfClass:[NSNumber class] forKey:ALGVerticalArchiveKey] boolValue];
        _coefficient = [[aDecoder decodeObjectOfClass:[NSNumber class] forKey:ALGCoefficientArchiveKey] integerValue];
        CGPoint point;
        [[aDecoder decodeObjectOfClass:[NSValue class] forKey:ALGCenterArchiveKey] getValue:&point];
        _center = point;
        
    }
    return self;
}

+(BOOL) supportsSecureCoding {
    return YES;
}

-(void) encodeWithCoder:(NSCoder *)aCoder {
    
    [aCoder encodeObject:@(_tileType) forKey:ALGTileTypeArchiveKey];
    [aCoder encodeObject:@(_negative) forKey:ALGNegativeArchiveKey];
    [aCoder encodeObject:@(_vertical) forKey:ALGVerticalArchiveKey];
    [aCoder encodeObject:@(_coefficient) forKey:ALGCoefficientArchiveKey];
    [aCoder encodeObject:[NSValue valueWithCGPoint:_center] forKey:ALGCenterArchiveKey];

}

@end
