//
//  ALGTileGroupModel.m
//  AlgebraTiles
//
//  Created by Reese McLean on 4/20/13.
//  Copyright (c) 2013 Reese McLean. All rights reserved.
//

#import "ALGTileGroupModel.h"

#import "ALGTileView.h"
#import "ALGExpressionTermModel.h"

@interface ALGTileGroupModel ()

@end

@implementation ALGTileGroupModel

-(id) initWithTiles:(NSArray*)tiles {
    self = [super init];
    if (self) {
        _tiles = [NSMutableArray arrayWithArray:tiles];
    }
    return self;
}

+(id) groupWithTiles:(NSArray*)tiles {
    return [[self alloc] initWithTiles:tiles];
}

-(BOOL)isSameTypeOfGroup:(ALGTileGroupModel*)anotherGroup {
    return [[self topTile] isSameTypeOfTile:[anotherGroup topTile]];
}

-(ALGTileView*)bottomTile {
    if ([self.tiles count] < 1) {
        return nil;
    }
    return [self.tiles objectAtIndex:0];
}

-(ALGTileView*) topTile {
    return [self.tiles lastObject];
}

-(void) addTile:(ALGTileView *)tile {
    [self.tiles addObject:tile];
}

-(void) removeTile:(ALGTileView*)tile {
    [self.tiles removeObject:tile];
}

-(BOOL) containsTile:(ALGTileView*)tile {
    return [self.tiles containsObject:tile];
}

-(UIView *) superview {
    return [[self topTile] superview];
}

-(CGPoint) centerOfGroup {
    
    if ([self.tiles count] < 1) {
        return CGPointZero;
    }
    
    ALGTileView *bottomTile = [self.tiles objectAtIndex:0];
    ALGTileView *topTile = [self.tiles lastObject];
    
    return CGPointMake((bottomTile.center.x + topTile.center.x)/2.0, topTile.center.y);
    
}

-(CGRect) rectForGroup {
    
    if ([self.tiles count] < 1) {
        return CGRectZero;
    }
    
    ALGTileView *bottomTile = [self.tiles objectAtIndex:0];
    ALGTileView *topTile = [self.tiles lastObject];
    
    return CGRectMake([bottomTile topLeftPoint].x, [bottomTile topRightPoint].y, [topTile topRightPoint].x - [bottomTile topLeftPoint].x, [topTile bottomRightPoint].y - [bottomTile topLeftPoint].y);
    
}

-(ALGExpressionTermModel*)termModel {
    
    ALGExpressionTermModel *termModel = [[ALGExpressionTermModel alloc] init];
    
    NSMutableSet *tileModels = [NSMutableSet set];
    for (ALGTileView *tileView in self.tiles) {
        [tileModels addObject:tileView.tileModel];
    }
    
    termModel.tileModels = tileModels;
    return termModel;
}

@end
