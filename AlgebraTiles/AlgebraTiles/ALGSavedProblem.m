//
//  ALGSavedProblem.m
//  AlgebraTiles
//
//  Created by Reese McLean on 3/27/14.
//  Copyright (c) 2014 Reese McLean. All rights reserved.
//

#import "ALGSavedProblem.h"

NSString *const ALGMatActivitityArchiveKey = @"ALGMatActivitityArchiveKey";
NSString *const ALGProblemUniqueIDArchiveKey = @"ALGProblemUniqueIDArchiveKey";
NSString *const ALGLastUsedDateArchiveKey = @"ALGLastUsedDateArchiveKey";
NSString *const ALGPreviewImageURLArchiveKey = @"ALGPreviewImageURLArchiveKey";
NSString *const ALGPositiveTermsMatOneArchiveKey = @"ALGPositiveTermsMatOneArchiveKey";
NSString *const ALGNegativeTermsMatOneArchiveKey = @"ALGNegativeTermsMatOneArchiveKey";
NSString *const ALGPositiveTermsMatTwoArchiveKey = @"ALGPositiveTermsMatTwoArchiveKey";
NSString *const ALGNegativeTermsMatTwoArchiveKey = @"ALGNegativeTermsMatTwoArchiveKey";
NSString *const ALGTilesAlongBottomArchiveKey = @"ALGTilesAlongBottomArchiveKey";
NSString *const ALGTilesAlongLeftArchiveKey = @"ALGTilesAlongLeftArchiveKey";
NSString *const ALGTileInCenterArchiveKey = @"ALGTileInCenterArchiveKey";

#import "ALGSavedProblemsController.h"

@implementation ALGSavedProblem

-(instancetype) initWithCoder:(NSCoder *)aDecoder {
    
    self = [super init];
    if (self)
    {
        // Decode the property values by key, specifying the expected class
        _matActivityID = [aDecoder decodeObjectOfClass:[NSString class] forKey:ALGMatActivitityArchiveKey];
        _uniqueID = [aDecoder decodeObjectOfClass:[NSString class] forKey:ALGProblemUniqueIDArchiveKey];
        _lastUsedDate = [aDecoder decodeObjectOfClass:[NSDate class] forKey:ALGLastUsedDateArchiveKey];
        _positiveTermsForMatOne = [aDecoder decodeObjectOfClass:[NSArray class] forKey:ALGPositiveTermsMatOneArchiveKey];
        _negativeTermsForMatOne = [aDecoder decodeObjectOfClass:[NSArray class] forKey:ALGNegativeTermsMatOneArchiveKey];
        _positiveTermsForMatTwo = [aDecoder decodeObjectOfClass:[NSArray class] forKey:ALGPositiveTermsMatTwoArchiveKey];
        _negativeTermsForMatTwo = [aDecoder decodeObjectOfClass:[NSArray class] forKey:ALGNegativeTermsMatTwoArchiveKey];
        _tilesAlongBottom = [aDecoder decodeObjectOfClass:[NSArray class] forKey:ALGTilesAlongBottomArchiveKey];
        _tilesAlongLeft = [aDecoder decodeObjectOfClass:[NSArray class] forKey:ALGTilesAlongLeftArchiveKey];
        _tilesInCenter = [aDecoder decodeObjectOfClass:[NSArray class] forKey:ALGTileInCenterArchiveKey];

    }
    return self;
}

+(BOOL) supportsSecureCoding {
    return YES;
}

-(void) encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeObject:_matActivityID forKey:ALGMatActivitityArchiveKey];
    [aCoder encodeObject:_uniqueID forKey:ALGProblemUniqueIDArchiveKey];
    [aCoder encodeObject:_lastUsedDate forKey:ALGLastUsedDateArchiveKey];
    [aCoder encodeObject:_positiveTermsForMatOne forKey:ALGPositiveTermsMatOneArchiveKey];
    [aCoder encodeObject:_negativeTermsForMatOne forKey:ALGNegativeTermsMatOneArchiveKey];
    [aCoder encodeObject:_positiveTermsForMatTwo forKey:ALGPositiveTermsMatTwoArchiveKey];
    [aCoder encodeObject:_negativeTermsForMatTwo forKey:ALGNegativeTermsMatTwoArchiveKey];
    [aCoder encodeObject:_tilesAlongBottom forKey:ALGTilesAlongBottomArchiveKey];
    [aCoder encodeObject:_tilesAlongLeft forKey:ALGTilesAlongLeftArchiveKey];
    [aCoder encodeObject:_tilesInCenter forKey:ALGTileInCenterArchiveKey];
}

-(instancetype) init {
    self = [super init];
    if (self) {
        _uniqueID = [[NSUUID UUID] UUIDString];
        _lastUsedDate = [NSDate date];
    }
    return self;
}

-(UIImage *) previewImage {
    
    if (!_previewImage) {
        _previewImage = [UIImage imageWithContentsOfFile:[self previewImageURL]];
    }
    return _previewImage;
}

-(NSString*) previewImageURL {
    NSString *directoryPath = [[ALGSavedProblemsController sharedController] previewImageSaveDirectory];
    NSString *imageName = [self.uniqueID stringByAppendingString:@".png"];
    NSString *imageURL = [directoryPath stringByAppendingFormat:@"/%@", imageName];
    return imageURL;
}

@end
