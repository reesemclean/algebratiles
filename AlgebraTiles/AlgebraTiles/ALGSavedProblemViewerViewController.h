//
//  ALGSavedProblemViewerViewController.h
//  AlgebraTiles
//
//  Created by Reese McLean on 3/30/14.
//  Copyright (c) 2014 Reese McLean. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ALGSavedProblem;
@protocol ALGSavedProblemViewerDelegate;

@interface ALGSavedProblemViewerViewController : UIViewController <UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout>

@property (nonatomic, weak) id<ALGSavedProblemViewerDelegate> delegate;

@end

@protocol ALGSavedProblemViewerDelegate <NSObject>

-(void) savedProblemViewer:(ALGSavedProblemViewerViewController*) savedProblemViewerViewController didSelectSavedProblem:(ALGSavedProblem*)savedProblem;

@end
