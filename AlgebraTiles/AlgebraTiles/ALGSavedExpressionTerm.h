//
//  ALGSavedExpressionTerm.h
//  AlgebraTiles
//
//  Created by Reese McLean on 3/27/14.
//  Copyright (c) 2014 Reese McLean. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "ALGTileConstants.h"

@interface ALGSavedExpressionTerm : NSObject <NSSecureCoding>

@property (nonatomic, assign) ALGTileType tileType;
@property (nonatomic, assign) BOOL negative;
@property (nonatomic, assign) NSInteger coefficient;
@property (nonatomic, assign) CGPoint center;
@property (nonatomic, assign) BOOL vertical;

@end
