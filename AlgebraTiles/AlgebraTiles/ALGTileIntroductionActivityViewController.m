//
//  ALGTileIntroductionViewController.m
//  AlgebraTiles
//
//  Created by Reese McLean on 5/8/13.
//  Copyright (c) 2013 Reese McLean. All rights reserved.
//

#import "ALGTileIntroductionActivityViewController.h"

#import <QuartzCore/QuartzCore.h>

#import "ALGInstructionStepModel.h"

#import "ALGExpressionMatViewController.h"
#import "ALGAreaMatViewController.h"

#import "ALGInstructionController.h"

@interface ALGTileIntroductionActivityViewController ()

@property (nonatomic, strong) ALGInstructionController* instructionController;

@property (nonatomic, strong) NSMutableSet *tilesBeingDragged;
@property (nonatomic, strong) NSMutableSet *tileGroupsBeingDragged;

@end

@implementation ALGTileIntroductionActivityViewController

+(instancetype)viewControllerForIntroductionFromStoryboard:(UIStoryboard*)storyboard {
    ALGTileIntroductionActivityViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"ALGTileIntroductionActivityViewStoryboardID"];
    return vc;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.tilesBeingDragged = [NSMutableSet set];
    self.tileGroupsBeingDragged = [NSMutableSet set];
    
    self.instructionController = [[ALGInstructionController alloc] initWithActivityController:self];

	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    self.instructionController.currentStep.setupBlock();
    
}

-(void) showSingleExpressionMatWithTopAndBottom:(BOOL)showTopAndBottom
                                       withSize:(CGSize)size
                                       animated:(BOOL)animated
                                 withCompletion:(ALGAnimationCompletionBlock)completionBlock {
    
    if (!self.leftExpressionMat) {
        self.leftExpressionMat = [self.storyboard instantiateViewControllerWithIdentifier:@"ALGExpressionMatViewController"];
        self.leftExpressionMat.hasTopAndBottom = showTopAndBottom;
        
        if (!CGSizeEqualToSize(size, CGSizeZero)) {
            self.leftExpressionMat.view.frame = CGRectMake(0, 0, size.width, size.height);
        }
        
        [self addChildViewController:self.leftExpressionMat];
        self.leftExpressionMat.view.alpha = 0.0;
        self.leftExpressionMat.view.center = CGPointMake(CGRectGetWidth(self.view.frame)/2.0, CGRectGetHeight(self.view.frame)/2.0);
        [self.view addSubview:self.leftExpressionMat.view];
        
        [UIView animateWithDuration:animated ? .1 : 0.0
                              delay:0.0
                            options:UIViewAnimationOptionCurveEaseInOut
                         animations:^{
                             self.leftExpressionMat.view.alpha = 1.0;
                         }
                         completion:^(BOOL finished){
                             [self.leftExpressionMat didMoveToParentViewController:self];
                             
                             if (completionBlock) {
                                 completionBlock();
                             }
                         }];
        
    }
    
    
}

-(void) hideSingleExpressionMatAnimated:(BOOL)animated
                         withCompletion:(ALGAnimationCompletionBlock)completionBlock {
    
    if (self.leftExpressionMat) {
        [self.leftExpressionMat willMoveToParentViewController:nil];
        
        [UIView animateWithDuration:animated ? .1 : 0.0
                              delay:0.0
                            options:UIViewAnimationOptionCurveEaseInOut
                         animations:^{
                             self.leftExpressionMat.view.alpha = 0.0;
                         }
                         completion:^(BOOL finished) {
                             
                             [self.leftExpressionMat removeFromParentViewController];
                             self.leftExpressionMat = nil;
                             if (completionBlock) {
                                 completionBlock();
                             }
                             
                         }];
    }
    
}

-(void) showDoubleExpressionMatWithTopAndBottom:(BOOL)showTopAndBottom animated:(BOOL)animated withCompletion:(ALGAnimationCompletionBlock)completionBlock {
    
}

-(void) showAreaMatWithSize:(CGSize)size
                   animated:(BOOL)animated
             withCompletion:(ALGAnimationCompletionBlock)completionBlock {

    if (!self.areaMat) {
        self.areaMat = [self.storyboard instantiateViewControllerWithIdentifier:@"ALGAreaMatViewController"];
        
        if (!CGSizeEqualToSize(size, CGSizeZero)) {
            self.areaMat.view.frame = CGRectMake(0, 0, size.width, size.height);
        } else {
            self.areaMat.view.frame = CGRectMake(0, 0, 400.0, 400.0);
        }
        
        [self addChildViewController:self.areaMat];
        self.areaMat.view.alpha = 0.0;
        self.areaMat.view.center = CGPointMake(CGRectGetWidth(self.view.frame)/2.0, CGRectGetHeight(self.view.frame)/2.0);
        [self.view addSubview:self.areaMat.view];
        
        [UIView animateWithDuration:animated ? .1 : 0.0
                              delay:0.0
                            options:UIViewAnimationOptionCurveEaseInOut
                         animations:^{
                             self.areaMat.view.alpha = 1.0;
                         }
                         completion:^(BOOL finished){
                             [self.areaMat didMoveToParentViewController:self];
                             
                             if (completionBlock) {
                                 completionBlock();
                             }
                         }];
        
    }
    
}

-(void) hideAreaMatAnimated:(BOOL)animated
                         withCompletion:(ALGAnimationCompletionBlock)completionBlock {
    
    if (self.areaMat) {
        [self.areaMat willMoveToParentViewController:nil];
        
        [UIView animateWithDuration:animated ? .1 : 0.0
                              delay:0.0
                            options:UIViewAnimationOptionCurveEaseInOut
                         animations:^{
                             self.areaMat.view.alpha = 0.0;
                         }
                         completion:^(BOOL finished) {
                             
                             [self.areaMat removeFromParentViewController];
                             self.areaMat = nil;
                             if (completionBlock) {
                                 completionBlock();
                             }
                             
                         }];
    }
    
}

#pragma mark - ALG Activity View Protocol

-(UIImage*) imageForSharing:(ALGContainerViewController *)actionVC {
    
    NSLog(@"Unimplemented Delegate");
    return nil;
    
}

-(ALGSavedProblem*) problemForSaving:(ALGContainerViewController *)actionVC {
    
    NSLog(@"Unimplemented Delegate");
    return nil;
    
}

#pragma mark - Tile Toolbox Delegate

-(BOOL) tileToolBoxVC:(ALGTileToolboxViewController *)toolboxVC shouldGiveTile:(ALGTileView *)tileView {
    return YES;
}

-(void) tileToolBoxVC:(ALGTileToolboxViewController *)toolboxVC didGiveTile:(ALGTileView *)tileView {
    
    tileView.center = [tileView.superview convertPoint:tileView.center toView:self.view];
    [self.view addSubview:tileView];
    tileView.canFlipToNegative = YES;
    tileView.touchDelegate = self;
    [self didStartTileViewPan:tileView];
    
}

-(ALGTileGroupModel*)groupModelBeingDraggedForTileView:(ALGTileView*)tileView {
    
    __block ALGTileGroupModel *foundGroup = nil;
    
    //Search TileGroups Being Dragged
    [self.tileGroupsBeingDragged enumerateObjectsUsingBlock:^(ALGTileGroupModel *groupModel, BOOL *stop) {
        if ([groupModel containsTile:tileView]) {
            foundGroup = groupModel;
            *stop = YES;
        }
    }];
    return foundGroup;
}


#pragma mark - Tile View Touch Delegate

-(void) didStartTileViewPan:(ALGTileView*)tileView {
    
    [self.areaMat didStartPanTile:tileView];
    
    [self.tilesBeingDragged addObject:tileView];
    
    ALGTileGroupModel *groupToManipulate = nil;
    
    //Search Expression Mats
    if (!groupToManipulate) {
        groupToManipulate = [self.leftExpressionMat groupForTileView:tileView];
    }
    
    if (!groupToManipulate) {
        groupToManipulate = [self.leftExpressionMat groupForTileView:tileView];
    }
    
    if (groupToManipulate) {
        //Check if top tile, if so remove from group and create a new one
        if ([[groupToManipulate tiles] count] > 1 && tileView == [groupToManipulate topTile]) {
            [groupToManipulate removeTile:tileView];
            groupToManipulate = [ALGTileGroupModel groupWithTiles:@[ tileView ]];
        }
    } else {
        //Still haven't found a group, must not have been placed in one yet
        groupToManipulate = [ALGTileGroupModel groupWithTiles:@[ tileView ]];
    }
    
    [self.leftExpressionMat unclaimGroupIfOwned:groupToManipulate];
    [self.rightExpressionMat unclaimGroupIfOwned:groupToManipulate];
    
    [self.tileGroupsBeingDragged addObject:groupToManipulate];
    
    for (ALGTileView *view in groupToManipulate.tiles) {
        [view addShadowAnimated:YES];
        [self.view bringSubviewToFront:view];
    }
    
}

-(void) didPanTileView:(ALGTileView*)tileView withTranslation:(CGPoint)translation {
    
    ALGTileGroupModel *groupForTile = [self groupModelBeingDraggedForTileView:tileView];
    
    for (ALGTileView *view in groupForTile.tiles) {
        [view translateTileCenter:translation];
    }

}

-(void) didFinishTileViewPan:(ALGTileView*)tileView {
    
    ALGTileGroupModel *groupForTile = [self groupModelBeingDraggedForTileView:tileView];
    
    for (ALGTileView *view in groupForTile.tiles) {
        [view removeShadowAnimated:YES];
    }
    
    [self.tileGroupsBeingDragged removeObject:groupForTile];
    [self.tilesBeingDragged removeObject:tileView];
    
    if ([self.leftExpressionMat willClaimTileGroup:groupForTile]) {

        [self.leftExpressionMat claimTileGroup:groupForTile];
        
    } else if ([self.rightExpressionMat willClaimTileGroup:groupForTile]) {
        
        [self.rightExpressionMat claimTileGroup:groupForTile];
        
    } else {
        if (![self.areaMat willClaimTile:tileView]) {
            
            for (ALGTileView *view in groupForTile.tiles) {
                [view removeTileAnimated:YES];
            }
        }
    }

    [self.areaMat didFinishTilePan:tileView];

    if(self.instructionController.currentStep.isStepCompleteBlock()) {
        
        self.instructionController.currentStep.didCompleteStepBlock();
        
    }
}

-(void) didFlipTile:(ALGTileView *)tileView {
    
    ALGTileGroupModel *groupToManipulate = nil;
    
    //Search Expression Mats
    if (!groupToManipulate) {
        groupToManipulate = [self.leftExpressionMat groupForTileView:tileView];
    }
    
    if (!groupToManipulate) {
        groupToManipulate = [self.leftExpressionMat groupForTileView:tileView];
    }
    
    for (ALGTileView *view in groupToManipulate.tiles) {
        if (view != tileView) {
            [view flipTileAnimated:YES];
        }
    }
    
    if(self.instructionController.currentStep.isStepCompleteBlock()) {
        
        self.instructionController.currentStep.didCompleteStepBlock();
        
    }
        
}

#pragma mark - List View Container Actions

- (IBAction)listButtonPushed:(id)sender {
}

- (IBAction)backButtonPushed:(id)sender {
    
    
    ALGInstructionStepModel *step = [self.instructionController currentStep];
    step.undoStepBlock();
    [self.instructionController previousStep];

}

- (IBAction)nextButtonPushed:(id)sender {
    
    ALGInstructionStepModel *step = [self.instructionController nextStep];
    if (step) {
        step.setupBlock();
    }
    
}

@end
