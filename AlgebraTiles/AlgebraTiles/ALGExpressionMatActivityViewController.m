//
//  ALGExpressionMatActivityViewController.m
//  AlgebraTiles
//
//  Created by Reese McLean on 4/20/13.
//  Copyright (c) 2013 Reese McLean. All rights reserved.
//

#import "ALGExpressionMatActivityViewController.h"

#import <QuartzCore/QuartzCore.h>

#import "ALGTileModel.h"
#import "ALGTileView.h"
#import "ALGTileGroupModel.h"
#import "ALGExpressionMatViewController.h"

#import "ALGSavedProblem.h"
#import "ALGSavedExpressionTerm.h"

#import "ALGMenuViewController.h"

#import "ALGTileThemeManager.h"

@interface ALGExpressionMatActivityViewController ()

@property (weak, nonatomic) IBOutlet UIButton *menuButton;
@property (weak, nonatomic) IBOutlet UIButton *actionButton;

@property (nonatomic, strong) NSMutableSet *tileGroupsBeingDragged;

@property (nonatomic, strong) ALGExpressionMatViewController *expressionMatVC;
@property (weak, nonatomic) IBOutlet UILabel *expressionLabel;

@end

@implementation ALGExpressionMatActivityViewController

+(instancetype)viewControllerForFreePlayMatFromStoryboard:(UIStoryboard*)storyboard withProblem:(ALGSavedProblem*)savedProblem {
    ALGExpressionMatActivityViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"ALGExpressionMatActivityViewController"];
    vc.hasTopAndBottom = NO;
    vc.initialProblem = savedProblem;
    return vc;
}

+(instancetype)viewControllerForExpressionMatFromStoryboard:(UIStoryboard*)storyboard withProblem:(ALGSavedProblem*)savedProblem {
    ALGExpressionMatActivityViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"ALGExpressionMatActivityViewController"];
    vc.hasTopAndBottom = YES;
    vc.initialProblem = savedProblem;
    return vc;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.tileGroupsBeingDragged = [NSMutableSet set];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([segue.identifier isEqualToString:@"ExpressionMatEmbedSegueID"]) {
        self.expressionMatVC = segue.destinationViewController;
        self.expressionMatVC.hasTopAndBottom = self.hasTopAndBottom;
    }
    
}

-(void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    if (self.initialProblem) {
        [self setupInitialProblem];
    }
    
    [self updateExpressionLabel];
}

#pragma mark - ALG Activity View Protocol

-(UIImage*) imageForSharing:(ALGContainerViewController *)actionVC {

    self.actionButton.hidden = YES;
    self.menuButton.hidden = YES;
    
    UIGraphicsBeginImageContextWithOptions(self.view.bounds.size, YES, 0.0);
    [self.view.layer renderInContext:UIGraphicsGetCurrentContext()];
    
    UIImage * img = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    self.actionButton.hidden = NO;
    self.menuButton.hidden = NO;
    
    return img;
    
}

-(ALGSavedProblem*) problemForSaving:(ALGContainerViewController *)actionVC {
    
    NSArray *positiveExpressionTerms = [self.expressionMatVC positiveSavedExpressionTerms];
    NSArray *negativeExpressionTerms = [self.expressionMatVC negativeSavedExpressionTerms];

    ALGSavedProblem *problem = [[ALGSavedProblem alloc] init];
    
    UIImage *fullSizeImage = [self imageForSharing:nil];
    CGSize newSize = CGSizeMake(fullSizeImage.size.width * .5, fullSizeImage.size.height * .5);
    UIGraphicsBeginImageContextWithOptions(newSize, YES, 0.0);
    [fullSizeImage drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage* scaledImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    problem.previewImage = scaledImage;
    problem.matActivityID = ALGExpressionMatActivityID;
    problem.positiveTermsForMatOne = positiveExpressionTerms;
    problem.negativeTermsForMatOne = negativeExpressionTerms;
    
    return problem;
}

#pragma mark - Expression Label

-(void) updateExpressionLabel {
    
    ALGExpressionModel *expressionModel = [self.expressionMatVC expressionModel];
    NSString *stringValue = [expressionModel expressionStringValue];
    
    self.expressionLabel.text = stringValue;
    
}

#pragma mark - Tile Toolbox Delegate

-(BOOL) tileToolBoxVC:(ALGTileToolboxViewController *)toolboxVC shouldGiveTile:(ALGTileView *)tileView {
    return YES;
}

-(void) tileToolBoxVC:(ALGTileToolboxViewController *)toolboxVC didGiveTile:(ALGTileView *)tileView {
    
    tileView.center = [tileView.superview convertPoint:tileView.center toView:self.view];
    [self.view addSubview:tileView];
    tileView.canFlipToNegative = YES;
    tileView.touchDelegate = self;
    [self didStartTileViewPan:tileView];

}

#pragma Tile View Group Helper

-(ALGTileGroupModel*)groupModelBeingDraggedForTileView:(ALGTileView*)tileView {
    
    __block ALGTileGroupModel *foundGroup = nil;
    
    //Search TileGroups Being Dragged
    [self.tileGroupsBeingDragged enumerateObjectsUsingBlock:^(ALGTileGroupModel *groupModel, BOOL *stop) {
        if ([groupModel containsTile:tileView]) {
            foundGroup = groupModel;
            *stop = YES;
        }
    }];
    return foundGroup;
}

#pragma mark - Tile View Touch Delegate

-(void) didStartTileViewPan:(ALGTileView*)tileView {

    ALGTileGroupModel *groupToManipulate = nil;

    //Search Expression Mats
    if (!groupToManipulate) {
        groupToManipulate = [self.expressionMatVC groupForTileView:tileView];
    }
    
    if (groupToManipulate) {
        //Check if top tile, if so remove from group and create a new one
        if ([[groupToManipulate tiles] count] > 1 && tileView == [groupToManipulate topTile]) {
            [groupToManipulate removeTile:tileView];
            groupToManipulate = [ALGTileGroupModel groupWithTiles:@[ tileView ]];
        }
    } else {
        //Still haven't found a group, must not have been placed in one yet
        groupToManipulate = [ALGTileGroupModel groupWithTiles:@[ tileView ]];
    }
    
    [self.expressionMatVC unclaimGroupIfOwned:groupToManipulate];
    
    //Need to somehow remove tile groups from the expression mats
    //Even when just moving the top tile
    
    [self.tileGroupsBeingDragged addObject:groupToManipulate];
    
    for (ALGTileView *view in groupToManipulate.tiles) {
        [view addShadowAnimated:YES];
        [self.view bringSubviewToFront:view];
    }

}

-(void) didPanTileView:(ALGTileView*)tileView withTranslation:(CGPoint)translation {
    
    ALGTileGroupModel *groupForTile = [self groupModelBeingDraggedForTileView:tileView];
    
    for (ALGTileView *view in groupForTile.tiles) {
        [view translateTileCenter:translation];
    }

}

-(void) didFinishTileViewPan:(ALGTileView*)tileView {
    
    ALGTileGroupModel *groupForTile = [self groupModelBeingDraggedForTileView:tileView];
    
    for (ALGTileView *view in groupForTile.tiles) {
        [view removeShadowAnimated:YES];
    }
    
    [self.tileGroupsBeingDragged removeObject:groupForTile];
    
    if ([self.expressionMatVC willClaimTileGroup:groupForTile]) {
        //claim it
        [self.expressionMatVC claimTileGroup:groupForTile];
        
    } else {
        for (ALGTileView *view in groupForTile.tiles) {
            [view removeTileAnimated:YES];
        }
    }
    
    [self updateExpressionLabel];
    
}

-(void) didFlipTile:(ALGTileView*)tileView {
    
    ALGTileGroupModel *groupToManipulate = nil;
    
    //Search Expression Mats
    if (!groupToManipulate) {
        groupToManipulate = [self.expressionMatVC groupForTileView:tileView];
    }
    
    for (ALGTileView *view in groupToManipulate.tiles) {
        if (view != tileView) {
            [view flipTileAnimated:YES];
        }
    }
    
    [self updateExpressionLabel];

}

-(void) setupInitialProblem {
    
    for (ALGSavedExpressionTerm *savedTerm in self.initialProblem.positiveTermsForMatOne) {
        NSMutableArray *tilesToBeAdded = [NSMutableArray array];
        for (int i = 0; i < savedTerm.coefficient; i++) {
            ALGTileModel *tileModel = [ALGTileModel tileWithType:savedTerm.tileType];
            tileModel.negative = savedTerm.negative;
            ALGTileView *tileView = [ALGTileView tileWithTileModel:tileModel andTheme:[[ALGTileThemeManager sharedThemeManager] currentTheme]];
            [self.view addSubview:tileView];
            tileView.canFlipToNegative = YES;
            tileView.touchDelegate = self;
            tileView.center = savedTerm.center;
            [tilesToBeAdded addObject:tileView];
        }
        
        ALGTileGroupModel *group = [ALGTileGroupModel groupWithTiles:tilesToBeAdded];
        if ([self.expressionMatVC willClaimTileGroup:group]) {
            [self.expressionMatVC claimTileGroup:group];
        } else {
            NSLog(@"Not Claimed by expression mat");
        }
    }
    
    for (ALGSavedExpressionTerm *savedTerm in self.initialProblem.negativeTermsForMatOne) {
        NSMutableArray *tilesToBeAdded = [NSMutableArray array];
        for (int i = 0; i < savedTerm.coefficient; i++) {
            ALGTileModel *tileModel = [ALGTileModel tileWithType:savedTerm.tileType];
            tileModel.negative = savedTerm.negative;
            ALGTileView *tileView = [ALGTileView tileWithTileModel:tileModel andTheme:[[ALGTileThemeManager sharedThemeManager] currentTheme]];
            [self.view addSubview:tileView];
            tileView.canFlipToNegative = YES;
            tileView.touchDelegate = self;
            tileView.center = savedTerm.center;
            [tilesToBeAdded addObject:tileView];
        }
        
        ALGTileGroupModel *group = [ALGTileGroupModel groupWithTiles:tilesToBeAdded];
        if ([self.expressionMatVC willClaimTileGroup:group]) {
            [self.expressionMatVC claimTileGroup:group];
        } else {
            NSLog(@"Not Claimed by expression mat");
        }
    }
    
    [self updateExpressionLabel];

}

@end
