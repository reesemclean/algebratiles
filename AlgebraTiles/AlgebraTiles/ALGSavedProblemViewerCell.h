//
//  ALGSavedProblemViewerCell.h
//  AlgebraTiles
//
//  Created by Reese McLean on 3/30/14.
//  Copyright (c) 2014 Reese McLean. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ALGSavedProblemViewerCell : UICollectionViewCell

-(void) setEditing:(BOOL)editing animated:(BOOL)animated;

@property (strong, nonatomic) IBOutlet UIImageView *imageView;
@property (strong, nonatomic) IBOutlet UILabel *dateLabel;
@property (strong, nonatomic) IBOutlet UIButton *deleteButton;

@end
