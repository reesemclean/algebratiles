//
//  ALGActionRootViewController.h
//  AlgebraTiles
//
//  Created by Reese McLean on 3/28/14.
//  Copyright (c) 2014 Reese McLean. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ALGSavedProblem;

@protocol ALGActionDelegate;

@interface ALGActionRootViewController : UITableViewController

@property (nonatomic, weak) id<ALGActionDelegate> delegate;

@end

@protocol ALGActionDelegate <NSObject>

-(void) didSelectShareImageInActionVC:(ALGActionRootViewController*)actionVC;
-(void) didSelectSaveProblemInActionVC:(ALGActionRootViewController*)actionVC;

@end


