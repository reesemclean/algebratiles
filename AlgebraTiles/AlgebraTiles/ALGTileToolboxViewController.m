//
//  ALGTileToolboxViewController.m
//  AlgebraTiles
//
//  Created by Reese McLean on 4/20/13.
//  Copyright (c) 2013 Reese McLean. All rights reserved.
//

#import "ALGTileToolboxViewController.h"

#import "ALGTileModel.h"
#import "ALGTileToolboxCell.h"

@interface ALGTileToolboxViewController ()

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@property (nonatomic, strong) NSArray *availableTiles;

@end

@implementation ALGTileToolboxViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Data Model Setup

-(NSArray*)availableTiles {
    
    if (!_availableTiles) {
        _availableTiles = @[ [ALGTileModel tileWithType:ALGTileTypeUnit],
                             [ALGTileModel tileWithType:ALGTileTypeX],
                             [ALGTileModel tileWithType:ALGTileTypeXSquared],
                             [ALGTileModel tileWithType:ALGTileTypeY],
                             [ALGTileModel tileWithType:ALGTileTypeYSquared],
                             [ALGTileModel tileWithType:ALGTileTypeXY] ];
    }
    
    return _availableTiles;
}

#pragma mark - Tile View Touch Delegate

-(void) didStartTileViewPan:(ALGTileView*)tileView {
    
    if ([self.delegate respondsToSelector:@selector(tileToolBoxVC:shouldGiveTile:)] &&
        ![self.delegate tileToolBoxVC:self shouldGiveTile:tileView]) {
        NSLog(@"Return");
        return;
    }
    
    CGPoint centerInCollectionView = [tileView.superview convertPoint:tileView.center toView:self.collectionView];
    NSIndexPath *indexPathOfCell = [self.collectionView indexPathForItemAtPoint:centerInCollectionView];
    ALGTileToolboxCell *cell = (ALGTileToolboxCell*)[self.collectionView cellForItemAtIndexPath:indexPathOfCell];
    
    [self.delegate tileToolBoxVC:self didGiveTile:tileView];
    cell.tileView = nil;

    [cell replaceTileViewAnimated:YES withTouchDelegate:self];
    
}

#pragma mark - UICollectionView Datasource
- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section {
    return [self.availableTiles count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)cv cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    ALGTileToolboxCell *cell = (ALGTileToolboxCell*)[cv dequeueReusableCellWithReuseIdentifier:@"TileToolboxCellID"
                                                                                forIndexPath:indexPath];
    cell.backgroundColor = [UIColor clearColor];
    
    ALGTileModel *tileModel = [self.availableTiles objectAtIndex:indexPath.row];
    [cell setTileModel:tileModel withTouchDelegate:self];
    
    return cell;
}

#pragma mark - UICollectionViewDelegate
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    // TODO: Select Item
}

#pragma mark – UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout*)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    ALGTileModel *tileModel = [self.availableTiles objectAtIndex:indexPath.row];
    
    return CGSizeMake(self.collectionView.frame.size.width, [ALGTileToolboxCell cellHeightForTileModel:tileModel]);
    
}

@end
