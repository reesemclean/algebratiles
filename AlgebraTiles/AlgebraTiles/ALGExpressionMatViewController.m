//
//  ALGExpressionMatViewController.m
//  AlgebraTiles
//
//  Created by Reese McLean on 4/20/13.
//  Copyright (c) 2013 Reese McLean. All rights reserved.
//

#import "ALGExpressionMatViewController.h"

#import "ALGTileGroupModel.h"
#import "ALGTileView.h"
#import "ALGExpressionMatView.h"
#import "ALGSavedExpressionTerm.h"
#import "ALGExpressionTermModel.h"

@interface ALGExpressionMatViewController ()

@property (nonatomic, strong, readwrite) NSMutableSet *tileGroups;
@property (nonatomic, strong) ALGExpressionMatView *matView;

@end

@implementation ALGExpressionMatViewController

-(void) awakeFromNib {
    [super awakeFromNib];
    self.hasTopAndBottom = YES;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    self.matView = (ALGExpressionMatView*)self.view;
    self.matView.hasTopAndBottom = self.hasTopAndBottom;
    
    self.tileGroups = [NSMutableSet set];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) unclaimGroupIfOwned:(ALGTileGroupModel*)group {
    
    if ([self.tileGroups containsObject:group]) {
        [self.tileGroups removeObject:group];
    } else {
        
        //Check if partial group
        for (ALGTileView *tileView in group.tiles) {
            ALGTileGroupModel *foundGroup = [self groupModelBeingDraggedForTileView:tileView];
            [foundGroup removeTile:tileView];
        }
        
    }
    
}

-(BOOL) willClaimTileGroup:(ALGTileGroupModel*)group {
    
    CGPoint convertedCenter = [[group superview] convertPoint:[group centerOfGroup] toView:self.view];
    
    if (CGRectContainsPoint(self.view.bounds, convertedCenter)) {
        return YES;
    } else {
        return NO;
    }
    
}

-(void) claimTileGroup:(ALGTileGroupModel*)incomingGroup {
    
    ALGTileGroupModel *groupToAdjust = incomingGroup;
    
    __block ALGTileGroupModel *foundGroup = nil;
    
    //Check if group should join another group
    [self.tileGroups enumerateObjectsUsingBlock:^(ALGTileGroupModel* groupToTest, BOOL *stop) {
        
        CGRect groupRectToTest = [groupToTest rectForGroup];
        
        if (CGRectContainsPoint(groupRectToTest, [incomingGroup centerOfGroup])) {
            //Check if same type of tiles
            if ([groupToTest isSameTypeOfGroup:incomingGroup]) {
                foundGroup = groupToTest;
                *stop  = YES;
            }
            
        }
        
    }];
    
    if (foundGroup) {
        for (ALGTileView *view in incomingGroup.tiles) {
            [foundGroup addTile:view];
        }
        groupToAdjust = foundGroup;
        
    } else {
        [self.tileGroups addObject:incomingGroup];
    }
    
    //Line up group
    if (groupToAdjust) {
        
        [UIView animateWithDuration:.2
                              delay:0.0
                            options:UIViewAnimationOptionAllowUserInteraction
                         animations:^{
                             
                             ALGTileView *bottomTile = [groupToAdjust bottomTile];
                             CGFloat horizontalOverlap = 1*[bottomTile trueWidth]/4.0;
                             CGFloat verticalOverlap = 8.0;
                             ALGTileView *previousTile = nil;
                             
                             for (ALGTileView *tile in groupToAdjust.tiles) {
                                 
                                 if (previousTile) {
                                     tile.center = CGPointMake(previousTile.center.x + horizontalOverlap, previousTile.center.y + verticalOverlap);
                                 }
                                 
                                 previousTile = tile;
                             }
                         }
                         completion:^(BOOL finished) {
                             
                         }];
        
        
        
    }
    
    //Check if group is on mat, and adjust
    if (groupToAdjust) {
        
        CGFloat distanceToMoveHorizontally = 0.0;
        CGFloat distanceToMoveVertically = 0.0;
        CGRect groupFrame = [groupToAdjust.superview convertRect:[groupToAdjust rectForGroup] toView:self.view];
        CGPoint groupCenter = [groupToAdjust.superview convertPoint:[groupToAdjust centerOfGroup] toView:self.view];
        
        if (!CGRectContainsRect(self.view.bounds, groupFrame) && CGRectContainsPoint(self.view.bounds, groupCenter)) {
            
            //(x)
            if (CGRectGetMinX(groupFrame) < 1) {
                distanceToMoveHorizontally = fabs(CGRectGetMinX(groupFrame)) + 1.0;
            } else if (CGRectGetMaxX(groupFrame) > CGRectGetWidth(self.view.bounds) - 1.0) {
                distanceToMoveHorizontally = -(CGRectGetMaxX(groupFrame) - (CGRectGetWidth(self.view.bounds) -1));
            }
            
            //(y)
            if (CGRectGetMinY(groupFrame) < 1) {
                distanceToMoveVertically = fabs(CGRectGetMinY(groupFrame)) + 1.0;
            } else if (CGRectGetMaxY(groupFrame) > CGRectGetHeight(self.view.bounds) - 1) {
                distanceToMoveVertically = -(CGRectGetMaxY(groupFrame) - (CGRectGetHeight(self.view.bounds) -1));
            }
            
            
        }
        
        if (self.hasTopAndBottom) {
            //Check midline
            CGRect midlineRect = CGRectMake(0.0, self.view.bounds.size.height/2.0 - .5, self.view.bounds.size.width, 1.0);
            
            CGRect newGroupFrame = CGRectMake(groupFrame.origin.x + distanceToMoveHorizontally,
                                              groupFrame.origin.y + distanceToMoveVertically,
                                              groupFrame.size.width,
                                              groupFrame.size.height);
            
            //(y) midline
            if (CGRectIntersectsRect(midlineRect, newGroupFrame)) {
                
                if (CGRectGetMidY(newGroupFrame) < self.view.bounds.size.height/2.0 - .5) {
                    //Adjust Up
                    distanceToMoveVertically -= CGRectGetMaxY(newGroupFrame) - self.view.bounds.size.height/2.0 + 1.0;
                } else {
                    //Adjust Down
                    distanceToMoveVertically += self.view.bounds.size.height/2.0 - CGRectGetMinY(newGroupFrame);
                }
                
            }
        }
        
        [UIView animateWithDuration:.1
                              delay:0.0
                            options:UIViewAnimationOptionAllowUserInteraction | UIViewAnimationOptionCurveEaseOut
                         animations:^{
                             for (ALGTileView *tileView in groupToAdjust.tiles) {
                                 
                                 tileView.center = CGPointMake(tileView.center.x + distanceToMoveHorizontally,
                                                               tileView.center.y + distanceToMoveVertically);
                                 
                             }
                         }
                         completion:^(BOOL finished) {
                             
                         }];
        
        
    }
    
}

-(ALGTileGroupModel*)groupModelBeingDraggedForTileView:(ALGTileView*)tileView {
    
    __block ALGTileGroupModel *foundGroup = nil;
    
    //Search TileGroups Being Dragged
    [self.tileGroups enumerateObjectsUsingBlock:^(ALGTileGroupModel *groupModel, BOOL *stop) {
        if ([groupModel containsTile:tileView]) {
            foundGroup = groupModel;
            *stop = YES;
        }
    }];
    return foundGroup;
}

-(ALGTileGroupModel*)groupForTileView:(ALGTileView*)tileView {
    return [self groupModelBeingDraggedForTileView:tileView];
}

#pragma mark - Expression Model

-(ALGExpressionModel*) expressionModel {
    
    NSMutableArray *positiveTileTerms = [NSMutableArray array];
    NSMutableArray *negativeTileTerms = [NSMutableArray array];
    
    for (ALGTileGroupModel *group in self.tileGroups) {
        
        CGPoint groupCenter = [group.superview convertPoint:[group centerOfGroup] toView:self.view];
        
        if ([group.tiles count] > 0) {
            
            if (self.hasTopAndBottom) {
                if (groupCenter.y < self.view.bounds.size.height/2.0 - .5) {
                    [positiveTileTerms addObject:[group termModel]];
                } else {
                    [negativeTileTerms addObject:[group termModel]];
                }
            } else {
                [positiveTileTerms addObject:[group termModel]];
            }
            
        }
        
    }
    
    return [ALGExpressionModel expressionWithPositiveTerms:positiveTileTerms andNegativeTerms:negativeTileTerms];
}

-(NSArray *)positiveSavedExpressionTerms {
    
    NSMutableArray *expressionTerms = [NSMutableArray array];
    
    for (ALGTileGroupModel *group in self.tileGroups) {
        
        CGPoint groupCenter = [group.superview convertPoint:[group centerOfGroup] toView:self.view];
        
        if ([group.tiles count] > 0) {
            
            if (self.hasTopAndBottom) {
                if (groupCenter.y < self.view.bounds.size.height/2.0 - .5) {
                    
                    ALGSavedExpressionTerm *term = [[ALGSavedExpressionTerm alloc] init];
                    term.tileType = [[group termModel] tileType];
                    term.negative = [[group termModel] negative];
                    term.coefficient = [[group termModel] coefficient];
                    term.center = [[group bottomTile] center];
                    term.vertical = NO;
                    
                    [expressionTerms addObject:term];
                    
                }
            } else {
                
                ALGSavedExpressionTerm *term = [[ALGSavedExpressionTerm alloc] init];
                term.tileType = [[group termModel] tileType];
                term.negative = [[group termModel] negative];
                term.coefficient = [[group termModel] coefficient];
                term.center = [[group bottomTile] center];
                term.vertical = NO;
                [expressionTerms addObject:term];
            }
            
        }
        
    }
    return [expressionTerms copy];
}

-(NSArray *)negativeSavedExpressionTerms {
    
    if (!self.hasTopAndBottom) {
        return @[];
    }
    
    NSMutableArray *expressionTerms = [NSMutableArray array];
    
    for (ALGTileGroupModel *group in self.tileGroups) {
        
        CGPoint groupCenter = [group.superview convertPoint:[group centerOfGroup] toView:self.view];
        
        if ([group.tiles count] > 0) {
            
            if (groupCenter.y >= self.view.bounds.size.height/2.0 - .5) {
                
                ALGSavedExpressionTerm *term = [[ALGSavedExpressionTerm alloc] init];
                term.tileType = [[group termModel] tileType];
                term.negative = [[group termModel] negative];
                term.coefficient = [[group termModel] coefficient];
                term.center = [group centerOfGroup];
                term.vertical = NO;

                [expressionTerms addObject:term];
                
            }
            
        }
        
    }
    return [expressionTerms copy];
    
}

@end
