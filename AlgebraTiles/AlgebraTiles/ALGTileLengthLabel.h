//
//  ATTileLengthLabel.h
//  AlgebraTiles
//
//  Created by Reese McLean on 12/17/12.
//  Copyright (c) 2012 Reese McLean. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ALGTileView.h"

@interface ALGTileLengthLabel : UIView

@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) ALGTileView *attachedTileView;

@end
