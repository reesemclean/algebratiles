//
//  ALGContainerViewController.h
//  AlgebraTiles
//
//  Created by Reese McLean on 4/20/13.
//  Copyright (c) 2013 Reese McLean. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ALGMenuViewController.h"
#import "ALGActionRootViewController.h"
#import "ALGSavedProblemViewerViewController.h"

@class ALGSavedProblem;

@protocol ALGActionDataSource;

@interface ALGContainerViewController : UIViewController <ALGMenuViewControllerDelegate, ALGActionDelegate, ALGSavedProblemViewerDelegate>

- (void)menuButtonPushed:(id)sender;
- (void)actionButtonPushed:(id)sender;

@property (nonatomic, weak) id<ALGActionDataSource> actionVCDataSource;

@end

@protocol ALGActionDataSource <NSObject>

-(UIImage *) imageForSharing:(ALGContainerViewController*)actionVC;
-(ALGSavedProblem*) problemForSaving:(ALGContainerViewController *)actionVC;

@end
