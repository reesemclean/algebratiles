//
//  ALGEquationMatActivityViewController.m
//  AlgebraTiles
//
//  Created by Reese McLean on 4/21/13.
//  Copyright (c) 2013 Reese McLean. All rights reserved.
//

#import "ALGEquationMatActivityViewController.h"

#import "ALGExpressionMatViewController.h"
#import "ALGSavedProblem.h"

@interface ALGEquationMatActivityViewController ()

@property (nonatomic, strong) ALGExpressionMatViewController *leftExpressionMatVC;
@property (nonatomic, strong) ALGExpressionMatViewController *rightExpressionMatVC;

@end

@implementation ALGEquationMatActivityViewController

+(instancetype)viewControllerForEquationMatFromStoryboard:(UIStoryboard*)storyboard withProblem:(ALGSavedProblem*)savedProblem {
    ALGEquationMatActivityViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"ALGEquationMatActivityViewController"];
    return vc;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(ALGSavedProblem*) problemForSaving:(ALGContainerViewController *)actionVC {
    
    NSArray *leftPositiveExpressionTerms = [self.leftExpressionMatVC positiveSavedExpressionTerms];
    NSArray *leftNegativeExpressionTerms = [self.leftExpressionMatVC negativeSavedExpressionTerms];
    NSArray *rightPositiveExpressionTerms = [self.rightExpressionMatVC positiveSavedExpressionTerms];
    NSArray *rightNegativeExpressionsTerms = [self.rightExpressionMatVC negativeSavedExpressionTerms];
    
    ALGSavedProblem *problem = [[ALGSavedProblem alloc] init];
    
    UIImage *fullSizeImage = [self imageForSharing:nil];
    CGSize newSize = CGSizeMake(fullSizeImage.size.width * .5, fullSizeImage.size.height * .5);
    UIGraphicsBeginImageContextWithOptions(newSize, YES, 0.0);
    [fullSizeImage drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage* scaledImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    problem.previewImage = scaledImage;
    problem.matActivityID = ALGComparisionMatActivityID;
    problem.positiveTermsForMatOne = leftPositiveExpressionTerms;
    problem.negativeTermsForMatOne = leftNegativeExpressionTerms;
    problem.positiveTermsForMatTwo = rightPositiveExpressionTerms;
    problem.negativeTermsForMatTwo = rightNegativeExpressionsTerms;
    
    return problem;
}

@end
