//
//  ALGExpressionMatView.h
//  AlgebraTiles
//
//  Created by Reese McLean on 4/20/13.
//  Copyright (c) 2013 Reese McLean. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ALGExpressionMatView : UIView

@property (nonatomic, assign) BOOL hasTopAndBottom;

@end
