//
//  ALGAppDelegate.h
//  AlgebraTiles
//
//  Created by Reese McLean on 4/19/13.
//  Copyright (c) 2013 Reese McLean. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ALGAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
