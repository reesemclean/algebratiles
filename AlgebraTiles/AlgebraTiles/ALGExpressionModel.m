//
//  ATExpressionModel.m
//  AlgebraTiles
//
//  Created by Reese McLean on 12/8/12.
//  Copyright (c) 2012 Reese McLean. All rights reserved.
//

#import "ALGExpressionModel.h"

#import "ALGTileConstants.h"

#import "ALGTileModel.h"
#import "ALGExpressionTermModel.h"

NSString * const ALG_COEFFICIENT_UNIT_KEY = @"ALG_COEFFICIENT_UNIT_KEY";
NSString * const ALG_COEFFICIENT_X_KEY = @"ALG_COEFFICIENT_X_KEY";
NSString * const ALG_COEFFICIENT_X_SQUARED_KEY = @"ALG_COEFFICIENT_X_SQUARED_KEY";
NSString * const ALG_COEFFICIENT_Y_KEY = @"ALG_COEFFICIENT_Y_KEY";
NSString * const ALG_COEFFICIENT_Y_SQUARED_KEY = @"ALG_COEFFICIENT_Y_SQUARED_KEY";
NSString * const ALG_COEFFICIENT_XY_KEY = @"ALG_COEFFICIENT_XY_KEY";
NSString * const ALG_COEFFICIENT_NEGATIVE_UNIT_KEY = @"ALG_COEFFICIENT_NEGATIVE_UNIT_KEY";
NSString * const ALG_COEFFICIENT_NEGATIVE_X_KEY = @"ALG_COEFFICIENT_NEGATIVE_X_KEY";
NSString * const ALG_COEFFICIENT_NEGATIVE_X_SQUARED_KEY = @"ALG_COEFFICIENT_NEGATIVE_X_SQUARED_KEY";
NSString * const ALG_COEFFICIENT_NEGATIVE_Y_KEY = @"ALG_COEFFICIENT_NEGATIVE_Y_KEY";
NSString * const ALG_COEFFICIENT_NEGATIVE_Y_SQUARED_KEY = @"ALG_COEFFICIENT_NEGATIVE_Y_SQUARED_KEY";
NSString * const ALG_COEFFICIENT_NEGATIVE_XY_KEY = @"ALG_COEFFICIENT_NEGATIVE_XY_KEY";

@implementation ALGExpressionModel

-(id) initWithPositiveTermsModels:(NSArray *)postiveModels andNegativeModels:(NSArray *)negativeModels {
    self = [super init];
    if (self) {
        self.positiveTerms = [postiveModels copy];
        self.negativeTerms = [negativeModels copy];
    }
    return self;
}



+(id) expressionWithPositiveTerms:(NSArray*)positiveTerms andNegativeTerms:(NSArray*)negativeTerms {
    return [[ALGExpressionModel alloc] initWithPositiveTermsModels:positiveTerms andNegativeModels:negativeTerms];
}

+(NSDictionary*) coefficientsInTileArray:(NSArray*)tileArray {
        
    int numberOfUnits = 0;
    int numberOfX = 0;
    int numberOfXSquared = 0;
    int numberOfY = 0;
    int numberOfYSquared = 0;
    int numberOfXY = 0;
    int numberOfNegativeUnits = 0;
    int numberOfNegativeX = 0;
    int numberOfNegativeXSquared = 0;
    int numberOfNegativeY = 0;
    int numberOfNegativeYSquared = 0;
    int numberOfNegativeXY = 0;
    
    for (ALGTileModel *tileModel in tileArray) {
        switch (tileModel.tileType) {
            case ALGTileTypeUnit:
                if (tileModel.negative) {
                    numberOfNegativeUnits++;
                } else {
                    numberOfUnits++;
                }
                break;
            case ALGTileTypeX:
                if (tileModel.negative) {
                    numberOfNegativeX++;
                } else {
                    numberOfX++;
                }
                break;
            case ALGTileTypeXSquared:
                if (tileModel.negative) {
                    numberOfNegativeXSquared++;
                } else {
                    numberOfXSquared++;
                }
                break;
            case ALGTileTypeY:
                if (tileModel.negative) {
                    numberOfNegativeY++;
                } else {
                    numberOfY++;
                }
                break;
            case ALGTileTypeYSquared:
                if (tileModel.negative) {
                    numberOfNegativeYSquared++;
                } else {
                    numberOfYSquared++;
                }
                break;
            case ALGTileTypeXY:
                if (tileModel.negative) {
                    numberOfNegativeXY++;
                } else {
                    numberOfXY++;
                }
                break;
            default:
                break;
        }
    }
    
    return @{ALG_COEFFICIENT_UNIT_KEY : @(numberOfUnits),
                ALG_COEFFICIENT_X_KEY : @(numberOfX),
    ALG_COEFFICIENT_X_SQUARED_KEY : @(numberOfXSquared),
    ALG_COEFFICIENT_Y_KEY : @(numberOfY),
    ALG_COEFFICIENT_Y_SQUARED_KEY : @(numberOfYSquared),
    ALG_COEFFICIENT_XY_KEY : @(numberOfXY),
    ALG_COEFFICIENT_NEGATIVE_UNIT_KEY : @(numberOfNegativeUnits),
    ALG_COEFFICIENT_NEGATIVE_X_KEY : @(numberOfNegativeX),
    ALG_COEFFICIENT_NEGATIVE_X_SQUARED_KEY : @(numberOfNegativeXSquared),
    ALG_COEFFICIENT_NEGATIVE_Y_KEY : @(numberOfNegativeY),
    ALG_COEFFICIENT_NEGATIVE_Y_SQUARED_KEY : @(numberOfNegativeYSquared),
    ALG_COEFFICIENT_NEGATIVE_XY_KEY : @(numberOfNegativeXY) };
}

-(NSString*) formatStringFromCoefficientDictionary:(NSDictionary*)dictionary {
    
    NSMutableString *string = [NSMutableString string];

    int positiveXSquared = [dictionary[ALG_COEFFICIENT_X_SQUARED_KEY] intValue];
    int positiveYSquared = [dictionary[ALG_COEFFICIENT_Y_SQUARED_KEY] intValue];
    int positiveXY = [dictionary[ALG_COEFFICIENT_XY_KEY] intValue];
    int positiveX = [dictionary[ALG_COEFFICIENT_X_KEY] intValue];
    int positiveY = [dictionary[ALG_COEFFICIENT_Y_KEY] intValue];
    int positiveUnits = [dictionary[ALG_COEFFICIENT_UNIT_KEY] intValue];
    
    int negativeXSquared = [dictionary[ALG_COEFFICIENT_NEGATIVE_X_SQUARED_KEY] intValue];
    int negativeYSquared = [dictionary[ALG_COEFFICIENT_NEGATIVE_Y_SQUARED_KEY] intValue];
    int negativeXY = [dictionary[ALG_COEFFICIENT_NEGATIVE_XY_KEY] intValue];
    int negativeX = [dictionary[ALG_COEFFICIENT_NEGATIVE_X_KEY] intValue];
    int negativeY = [dictionary[ALG_COEFFICIENT_NEGATIVE_Y_KEY] intValue];
    int negativeUnits = [dictionary[ALG_COEFFICIENT_NEGATIVE_UNIT_KEY] intValue];
    
    if (positiveXSquared != 0) {
        [string appendFormat:@"%dx²", positiveXSquared];
    }
    
    if (negativeXSquared != 0) {
        
        if ([string length] > 0) {
            [string appendString:@" - "];
            [string appendFormat:@"%dx²", negativeXSquared];
        } else {
            [string appendFormat:@"-%dx²", negativeXSquared];
        }
        
    }
    
    if (positiveYSquared != 0) {
        
        if ([string length] > 0) {
            [string appendString:@" + "];
        }
        
        [string appendFormat:@"%dy²", positiveYSquared];

    }
    
    if (negativeYSquared != 0) {
        
        if ([string length] > 0) {
            [string appendString:@" - "];
            [string appendFormat:@"%dy²", negativeYSquared];
        } else {
            [string appendFormat:@"-%dy²", negativeYSquared];
        }
        
    }
    
    if (positiveXY != 0) {
        if ([string length] > 0) {
            [string appendString:@" + "];
        }
        
        [string appendFormat:@"%dxy", positiveXY];

    }
    
    if (negativeXY != 0) {
        
        if ([string length] > 0) {
            [string appendString:@" - "];
            [string appendFormat:@"%dxy", negativeXY];
        } else {
            [string appendFormat:@"-%dxy", negativeXY];
        }
        
    }

    if (positiveX != 0) {
        if ([string length] > 0) {
            [string appendString:@" + "];
        } 
            [string appendFormat:@"%dx", positiveX];
    }
    
    if (negativeX != 0) {
        
        if ([string length] > 0) {
            [string appendString:@" - "];
            [string appendFormat:@"%dx", negativeX];
        } else {
            [string appendFormat:@"-%dx", negativeX];
        }
        
    }
    
    if (positiveY != 0) {
        if ([string length] > 0) {
            [string appendString:@" + "];
        }
            [string appendFormat:@"%dy", positiveY];
    }
    
    if (negativeY != 0) {
        
        if ([string length] > 0) {
            [string appendString:@" - "];
            [string appendFormat:@"%dy", negativeY];
        } else {
            [string appendFormat:@"-%dy", negativeY];
        }
        
    }
    
    if (positiveUnits != 0) {
        if ([string length] > 0) {
            [string appendString:@" + "];
        }
            [string appendFormat:@"%d", positiveUnits];
    }
    
    if (negativeUnits != 0) {
        
        if ([string length] > 0) {
            [string appendString:@" - "];
            [string appendFormat:@"%d", negativeUnits];
        } else {
            [string appendFormat:@"-%d", negativeUnits];
        }
        
    }
    
    return [string copy];
}

-(NSString *)stringForTileType:(ALGTileType) tileType {
    
    switch (tileType) {
        case ALGTileTypeUnit:
            return @"";
            break;
        case ALGTileTypeX:
            return @"x";
            break;
        case ALGTileTypeXSquared:
            return @"x²";
            break;
        case ALGTileTypeXY:
            return @"xy";
            break;
        case ALGTileTypeY:
            return @"y";
            break;
        case ALGTileTypeYSquared:
            return @"y²";
            break;
        default:
            break;
    }
    
}

-(NSString*)formatStringForTerms:(NSArray*)arrayOfTermModels {
    
    NSMutableString *string = [NSMutableString string];
    
    for (ALGExpressionTermModel *term in arrayOfTermModels) {
        
        if ([string length] > 0) {
            
            if ([term negative]) {
                [string appendString:@" - "];

            } else {
                [string appendString:@" + "];
            }
            
            [string appendFormat:@"%lu", (unsigned long)[term coefficient]];
            [string appendString:[self stringForTileType:[term tileType]]];
            
        } else {
            
            if ([term negative]) {
                [string appendString:@"-"];
                
            }
            
            [string appendFormat:@"%lu%@", (unsigned long)[term coefficient], [self stringForTileType:[term tileType]]];
            
        }
        
    }
    
    return [string copy];
    
}

-(NSString*)expressionStringValue {
        
    NSString *positiveString = [self formatStringForTerms:self.positiveTerms];
    NSString *negativeString = [self formatStringForTerms:self.negativeTerms];

    NSMutableString *string = [NSMutableString string];
    
    [string appendString:positiveString];
    
    if ([negativeString length] > 0) {
        
        if ([string length] > 0) {
            [string appendString:@" "];
        }
        
        [string appendFormat:@"- (%@)", negativeString];
    }
    
    if ([string length] < 1) {
        [string appendString:@"0"];
    }
    
    return [string copy];
}

@end
