//
//  ALGSavedProblemViewerCell.m
//  AlgebraTiles
//
//  Created by Reese McLean on 3/30/14.
//  Copyright (c) 2014 Reese McLean. All rights reserved.
//

#import "ALGSavedProblemViewerCell.h"

@implementation ALGSavedProblemViewerCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

-(void) setEditing:(BOOL)editing animated:(BOOL)animated {
    
    [UIView animateWithDuration:.1
                     animations:^{
                         self.deleteButton.alpha = editing ? 1.0 : 0.0;
                     }];
    
}

@end
