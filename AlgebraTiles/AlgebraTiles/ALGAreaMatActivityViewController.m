//
//  ALGAreaMatViewController.m
//  AlgebraTiles
//
//  Created by Reese McLean on 4/21/13.
//  Copyright (c) 2013 Reese McLean. All rights reserved.
//

#import "ALGAreaMatActivityViewController.h"

#import <QuartzCore/QuartzCore.h>

#import "ALGAreaMatViewController.h"
#import "ALGSavedProblem.h"
#import "ALGSavedExpressionTerm.h"
#import "ALGTileView.h"
#import "ALGTileModel.h"
#import "ALGTileThemeManager.h"

@interface ALGAreaMatActivityViewController ()

@property (weak, nonatomic) IBOutlet UIButton *menuButton;
@property (weak, nonatomic) IBOutlet UIButton *actionButton;

@property (nonatomic, strong) NSMutableSet *tilesBeingDragged;

@property (nonatomic, strong) ALGAreaMatViewController *areaMatVC;

@end

@implementation ALGAreaMatActivityViewController

+(instancetype)viewControllerForAreaMatFromStoryboard:(UIStoryboard*)storyboard withProblem:(ALGSavedProblem*)savedProblem {
    ALGAreaMatActivityViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"ALGAreaMatActivityViewController"];
    vc.initialProblem = savedProblem;
    return vc;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    self.tilesBeingDragged = [NSMutableSet set];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    if (self.initialProblem) {
        [self setupInitialProblem];
    }
    
}

-(void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([segue.identifier isEqualToString:@"AreaMatSegueID"]) {
        self.areaMatVC = segue.destinationViewController;
    }
    
}

#pragma mark - ALG Activity View Protocol

-(UIImage*) imageForSharing:(ALGContainerViewController *)actionVC {
    
    self.actionButton.hidden = YES;
    self.menuButton.hidden = YES;
    
    UIGraphicsBeginImageContextWithOptions(self.view.bounds.size, YES, 0.0);
    [self.view.layer renderInContext:UIGraphicsGetCurrentContext()];
    
    UIImage * img = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    self.actionButton.hidden = NO;
    self.menuButton.hidden = NO;
    
    return img;
    
}

-(ALGSavedProblem*) problemForSaving:(ALGContainerViewController *)actionVC {
        
    NSArray *tilesAlongBottom = [self.areaMatVC savedExpressionTermsAlongBottom];
    NSArray *tilesAlongLeft = [self.areaMatVC savedExpressionTermsAlongLeft];
    NSArray *tilesInCenter = [self.areaMatVC savedExpressionTermsInCenter];
    
    ALGSavedProblem *problem = [[ALGSavedProblem alloc] init];
    
    UIImage *fullSizeImage = [self imageForSharing:nil];
    CGSize newSize = CGSizeMake(fullSizeImage.size.width * .5, fullSizeImage.size.height * .5);
    UIGraphicsBeginImageContextWithOptions(newSize, YES, 0.0);
    [fullSizeImage drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage* scaledImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    problem.previewImage = scaledImage;
    problem.matActivityID = ALGAreaMatActivityID;
    problem.tilesAlongBottom = tilesAlongBottom;
    problem.tilesAlongLeft = tilesAlongLeft;
    problem.tilesInCenter = tilesInCenter;
    
    return problem;
}


#pragma mark - Tile Toolbox Delegate

-(BOOL) tileToolBoxVC:(ALGTileToolboxViewController *)toolboxVC shouldGiveTile:(ALGTileView *)tileView {
    return YES;
}

-(void) tileToolBoxVC:(ALGTileToolboxViewController *)toolboxVC didGiveTile:(ALGTileView *)tileView {
    
    tileView.center = [tileView.superview convertPoint:tileView.center toView:self.view];
    [self.view addSubview:tileView];
    tileView.canFlipToNegative = YES;
    tileView.touchDelegate = self;
    [self didStartTileViewPan:tileView];
    
}

#pragma mark - Tile View Touch Delegate

-(void) didStartTileViewPan:(ALGTileView*)tileView {
        
    [self.areaMatVC didStartPanTile:tileView];
    
    [self.tilesBeingDragged addObject:tileView];
    [tileView addShadowAnimated:YES];
    [self.view bringSubviewToFront:tileView];

}

-(void) didPanTileView:(ALGTileView*)tileView withTranslation:(CGPoint)translation {
    
    [tileView translateTileCenter:translation];
    [self.areaMatVC didPanTile:tileView];
    
}

-(void) didFinishTileViewPan:(ALGTileView*)tileView {
    
    
    [tileView removeShadowAnimated:YES];

    [self.tilesBeingDragged removeObject:tileView];
    
    if (![self.areaMatVC willClaimTile:tileView]) {
        [tileView removeTileAnimated:YES];

    }
    
    [self.areaMatVC didFinishTilePan:tileView];

}

-(void) setupInitialProblem {
    
    for (ALGSavedExpressionTerm *savedTerm in self.initialProblem.tilesAlongLeft) {
        
        ALGTileModel *tileModel = [ALGTileModel tileWithType:savedTerm.tileType];
        tileModel.negative = savedTerm.negative;
        ALGTileView *tileView = [ALGTileView tileWithTileModel:tileModel andTheme:[[ALGTileThemeManager sharedThemeManager] currentTheme]];
        tileView.canFlipToNegative = YES;
        tileView.touchDelegate = self;
        [self.areaMatVC addTile:tileView isVertical:savedTerm.vertical atCenter:savedTerm.center];
    }
    
    for (ALGSavedExpressionTerm *savedTerm in self.initialProblem.tilesAlongBottom) {
        
        ALGTileModel *tileModel = [ALGTileModel tileWithType:savedTerm.tileType];
        tileModel.negative = savedTerm.negative;
        ALGTileView *tileView = [ALGTileView tileWithTileModel:tileModel andTheme:[[ALGTileThemeManager sharedThemeManager] currentTheme]];
        tileView.canFlipToNegative = YES;
        tileView.touchDelegate = self;
        [self.areaMatVC addTile:tileView isVertical:savedTerm.vertical atCenter:savedTerm.center];
    }
    
    for (ALGSavedExpressionTerm *savedTerm in self.initialProblem.tilesInCenter) {
        
        ALGTileModel *tileModel = [ALGTileModel tileWithType:savedTerm.tileType];
        tileModel.negative = savedTerm.negative;
        ALGTileView *tileView = [ALGTileView tileWithTileModel:tileModel andTheme:[[ALGTileThemeManager sharedThemeManager] currentTheme]];
        tileView.canFlipToNegative = YES;
        tileView.touchDelegate = self;
        [self.areaMatVC addTile:tileView isVertical:savedTerm.vertical atCenter:savedTerm.center];
    }
    
    [self.areaMatVC adjustOutsideLabels];
}

@end
