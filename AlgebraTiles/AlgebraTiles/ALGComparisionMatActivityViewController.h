//
//  ALGComparisionMatActivityViewController.h
//  AlgebraTiles
//
//  Created by Reese McLean on 4/21/13.
//  Copyright (c) 2013 Reese McLean. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ALGContainerViewController.h"

#import "ALGTileView.h"
#import "ALGTileToolboxViewController.h"

@class ALGSavedProblem;

@interface ALGComparisionMatActivityViewController : UIViewController <ALGTileViewTouchDelegate, ALGTileToolboxDelegate, ALGActionDataSource>

@property (nonatomic, strong) ALGSavedProblem *initialProblem;

+(instancetype)viewControllerForComparisionMatFromStoryboard:(UIStoryboard*)storyboard withProblem:(ALGSavedProblem*)savedProblem;

@end
