//
//  ALGTileModel.h
//  AlgebraTiles
//
//  Created by Reese McLean on 4/20/13.
//  Copyright (c) 2013 Reese McLean. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "ALGTileConstants.h"

@interface ALGTileModel : NSObject

+(id) tileWithType:(ALGTileType)type;

@property (nonatomic, assign) BOOL negative;
@property (nonatomic, assign) ALGTileType tileType;

-(NSString*) tileTitle;

@end
