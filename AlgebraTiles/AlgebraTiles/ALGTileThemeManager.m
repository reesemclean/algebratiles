//
//  ALGTileTheme.m
//  AlgebraTiles
//
//  Created by Reese McLean on 3/27/14.
//  Copyright (c) 2014 Reese McLean. All rights reserved.
//

#import "ALGTileThemeManager.h"

#import "ALGTileTheme.h"

NSString *const ALGplistThemeKey = @"themes";
NSString *const ALGSavedThemeUniqueIDKey = @"ALGSavedThemeUniqueIDKey";
NSString *const ALGThemeDidChangeNotificationKey = @"ALGThemeDidChangeNotificationKey";

@interface ALGTileThemeManager ()

@property (nonatomic, readwrite) NSArray *availableThemes;

@end

@implementation ALGTileThemeManager

+(instancetype) sharedThemeManager {
    
    static dispatch_once_t once;
    static ALGTileThemeManager *instance;
    dispatch_once(&once, ^{
        instance = [[ALGTileThemeManager alloc] init];
    });
    return instance;
    
}

-(instancetype) init {
    
    self = [super init];
    if (self) {
        
        NSString *plistPath = [[NSBundle mainBundle] pathForResource:@"TileThemes" ofType:@"plist"];
        NSDictionary *dictionary = [[NSDictionary alloc] initWithContentsOfFile:plistPath];
        NSArray *themeDictionaries = dictionary[ALGplistThemeKey];
        
        NSMutableArray *tempThemeArray = [NSMutableArray array];
        for (NSDictionary *themeDictionary in themeDictionaries) {
            [tempThemeArray addObject:[ALGTileTheme tileThemeFromDictionary:themeDictionary]];
        }
        
        _availableThemes = [tempThemeArray copy];
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSInteger savedUniqueID = [defaults integerForKey:ALGSavedThemeUniqueIDKey];
        if (savedUniqueID) {
            __block ALGTileTheme *foundTheme = nil;
            [_availableThemes enumerateObjectsUsingBlock:^(ALGTileTheme *theme, NSUInteger idx, BOOL *stop) {
                if (theme.uniqueID == savedUniqueID) {
                    foundTheme = theme;
                    *stop = YES;
                }
            }];
            
            if (foundTheme) {
                _currentTheme = foundTheme;
            }
        }
        
        if (!_currentTheme) {
            _currentTheme = _availableThemes[0];
        }
        
    }
    return self;
}

-(void) setCurrentTheme:(ALGTileTheme *)currentTheme {
    _currentTheme = currentTheme;
    
    [[NSUserDefaults standardUserDefaults] setInteger:currentTheme.uniqueID forKey:ALGSavedThemeUniqueIDKey];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:ALGThemeDidChangeNotificationKey
                                                        object:currentTheme];
}

@end
