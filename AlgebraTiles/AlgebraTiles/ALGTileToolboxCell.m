//
//  ALGTileToolboxCell.m
//  AlgebraTiles
//
//  Created by Reese McLean on 4/20/13.
//  Copyright (c) 2013 Reese McLean. All rights reserved.
//

#import "ALGTileToolboxCell.h"

#import "ALGTileModel.h"
#import "ALGTileThemeManager.h"

#define top_bottom_margin 20.0
#define label_height 30.0

@interface ALGTileToolboxCell ()

@property (readwrite, strong, nonatomic) ALGTileModel *tileModel;

@end

@implementation ALGTileToolboxCell

-(void) setTileModel:(ALGTileModel *)tileModel withTouchDelegate:(id<ALGTileViewTouchDelegate>)touchDelegate {
    
    if (_tileModel != tileModel) {
        _tileModel = tileModel;
                
        self.label.text = [_tileModel tileTitle];
        
        [self replaceTileViewAnimated:NO withTouchDelegate:touchDelegate];

        self.label.frame = CGRectMake(0.0, top_bottom_margin * 2.0 + self.tileView.frame.size.height, self.frame.size.width, label_height);
        
    }
    
}

+(CGFloat) cellHeightForTileModel:(ALGTileModel*)tileModel {
    
    return top_bottom_margin + [ALGTileView sizeForTileModel:tileModel].height + top_bottom_margin + label_height + top_bottom_margin;
    
}

-(void) replaceTileViewAnimated:(BOOL)animated withTouchDelegate:(id<ALGTileViewTouchDelegate>)touchDelegate {
    
    [self.tileView removeFromSuperview];
    self.tileView = [ALGTileView tileWithTileModel:self.tileModel andTheme:[[ALGTileThemeManager sharedThemeManager] currentTheme]];
    self.tileView.touchDelegate = touchDelegate;
    self.tileView.frame = CGRectMake(0.0, top_bottom_margin, self.tileView.frame.size.width, self.tileView.frame.size.height);
    self.tileView.center = CGPointMake(self.frame.size.width/2.0, self.tileView.center.y);
    [self.contentView addSubview:self.tileView];
    
    if (animated) {
        
        self.tileView.alpha = 0.0;
        self.tileView.transform = CGAffineTransformMakeScale(.25, .25);

        [UIView animateWithDuration:animated ? .33 : 0.0
                              delay:0.1
                            options:UIViewAnimationOptionCurveEaseInOut
                         animations:^{
                             self.tileView.transform = CGAffineTransformIdentity;
                             self.tileView.alpha = 1.0;
                         }
                         completion:^(BOOL finished) {
                             
                         }];
    }
}

@end
