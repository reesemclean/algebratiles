//
//  ALGExpressionMatViewController.h
//  AlgebraTiles
//
//  Created by Reese McLean on 4/20/13.
//  Copyright (c) 2013 Reese McLean. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ALGTileGroupModel.h"
#import "ALGExpressionModel.h"

@class ALGSavedExpressionTerm;
@class ALGTileTheme;

@interface ALGExpressionMatViewController : UIViewController

@property (nonatomic, strong, readonly) NSMutableSet *tileGroups;

-(BOOL) willClaimTileGroup:(ALGTileGroupModel*)group;
-(void) claimTileGroup:(ALGTileGroupModel*)group;
-(ALGTileGroupModel*)groupForTileView:(ALGTileView*)tileView;

-(void) unclaimGroupIfOwned:(ALGTileGroupModel*)group;

-(ALGExpressionModel*) expressionModel;
-(NSArray *)positiveSavedExpressionTerms;
-(NSArray *)negativeSavedExpressionTerms;

@property (nonatomic, assign) BOOL hasTopAndBottom;

@end
