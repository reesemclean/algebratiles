//
//  ALGTileIntroductionViewController.h
//  AlgebraTiles
//
//  Created by Reese McLean on 5/8/13.
//  Copyright (c) 2013 Reese McLean. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ALGExpressionMatViewController;
@class ALGAreaMatViewController;

#import "ALGContainerViewController.h"

#import "ALGTileView.h"
#import "ALGTileToolboxViewController.h"

typedef void (^ALGAnimationCompletionBlock)();

@interface ALGTileIntroductionActivityViewController : UIViewController <ALGTileViewTouchDelegate, ALGTileToolboxDelegate, ALGActionDataSource>

+(instancetype)viewControllerForIntroductionFromStoryboard:(UIStoryboard*)storyboard;

@property (weak, nonatomic) IBOutlet UIButton *menuButton;
@property (weak, nonatomic) IBOutlet UIButton *actionButton;

@property (nonatomic, strong) ALGExpressionMatViewController *leftExpressionMat;
@property (nonatomic, strong) ALGExpressionMatViewController *rightExpressionMat;
@property (nonatomic, strong) ALGAreaMatViewController *areaMat;

-(void) hideSingleExpressionMatAnimated:(BOOL)animated
                         withCompletion:(ALGAnimationCompletionBlock)completionBlock;

-(void) showSingleExpressionMatWithTopAndBottom:(BOOL)showTopAndBottom
                                       withSize:(CGSize)size
                                       animated:(BOOL)animated
                                 withCompletion:(ALGAnimationCompletionBlock)completionBlock;
    
-(void) showDoubleExpressionMatWithTopAndBottom:(BOOL)showTopAndBottom animated:(BOOL)animated withCompletion:(ALGAnimationCompletionBlock)completionBlock;

-(void) showAreaMatWithSize:(CGSize)size
                   animated:(BOOL)animated
             withCompletion:(ALGAnimationCompletionBlock)completionBlock;

-(void) hideAreaMatAnimated:(BOOL)animated
             withCompletion:(ALGAnimationCompletionBlock)completionBlock;

@property (weak, nonatomic) IBOutlet UIView *instructionViewContainer;
@property (weak, nonatomic) IBOutlet UIButton *backButton;
@property (weak, nonatomic) IBOutlet UIButton *listButton;
@property (weak, nonatomic) IBOutlet UIButton *nextButton;
@property (weak, nonatomic) IBOutlet UILabel *instructionLabel;
- (IBAction)listButtonPushed:(id)sender;
- (IBAction)backButtonPushed:(id)sender;
- (IBAction)nextButtonPushed:(id)sender;

@end
