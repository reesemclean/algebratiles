//
//  ALGSavedProblem.h
//  AlgebraTiles
//
//  Created by Reese McLean on 3/27/14.
//  Copyright (c) 2014 Reese McLean. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ALGSavedProblem : NSObject <NSSecureCoding>

@property (nonatomic, strong) NSString *matActivityID;
@property (nonatomic, strong) NSString *uniqueID;
@property (nonatomic, strong) NSDate *lastUsedDate;
@property (nonatomic, strong) NSArray *positiveTermsForMatOne;
@property (nonatomic, strong) NSArray *negativeTermsForMatOne;
@property (nonatomic, strong) NSArray *positiveTermsForMatTwo;
@property (nonatomic, strong) NSArray *negativeTermsForMatTwo;

@property (nonatomic, strong) NSArray *tilesAlongBottom;
@property (nonatomic, strong) NSArray *tilesAlongLeft;
@property (nonatomic, strong) NSArray *tilesInCenter;

@property (nonatomic, strong) UIImage *previewImage;
@property (nonatomic, strong, readonly) NSString *previewImageURL;

@end
