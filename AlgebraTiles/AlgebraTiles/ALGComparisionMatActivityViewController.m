//
//  ALGComparisionMatActivityViewController.m
//  AlgebraTiles
//
//  Created by Reese McLean on 4/21/13.
//  Copyright (c) 2013 Reese McLean. All rights reserved.
//

#import "ALGComparisionMatActivityViewController.h"

#import <QuartzCore/QuartzCore.h>

#import "ALGExpressionMatViewController.h"

#import "ALGSavedProblem.h"
#import "ALGSavedExpressionTerm.h"
#import "ALGTileModel.h"
#import "ALGTileView.h"
#import "ALGTileThemeManager.h"

@interface ALGComparisionMatActivityViewController ()

@property (weak, nonatomic) IBOutlet UIButton *menuButton;
@property (weak, nonatomic) IBOutlet UIButton *actionButton;

@property (nonatomic, strong) NSMutableSet *tileGroupsBeingDragged;

@property (nonatomic, strong) ALGExpressionMatViewController *leftExpressionMatVC;
@property (nonatomic, strong) ALGExpressionMatViewController *rightExpressionMatVC;

@property (weak, nonatomic) IBOutlet UILabel *leftExpressionLabel;
@property (weak, nonatomic) IBOutlet UILabel *rightExpressionLabel;

@end

@implementation ALGComparisionMatActivityViewController

+(instancetype)viewControllerForComparisionMatFromStoryboard:(UIStoryboard*)storyboard withProblem:(ALGSavedProblem*)savedProblem {
    ALGComparisionMatActivityViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"ALGComparisionMatActivityViewController"];
    vc.initialProblem = savedProblem;
    return vc;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    self.tileGroupsBeingDragged = [NSMutableSet set];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    if (self.initialProblem) {
        [self setupInitialProblem];
    }
    
    [self updateExpressionLabels];
}

-(void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([segue.identifier isEqualToString:@"LeftExpressionMatEmbedSegueID"]) {
        self.leftExpressionMatVC = segue.destinationViewController;
    } else if ([segue.identifier isEqualToString:@"RightExpressionMatEmbedSegueID"]) {
        self.rightExpressionMatVC = segue.destinationViewController;
    }
    
}

#pragma mark - ALG Activity View Protocol

-(UIImage*) imageForSharing:(ALGContainerViewController *)actionVC {
    
    self.actionButton.hidden = YES;
    self.menuButton.hidden = YES;
    
    UIGraphicsBeginImageContextWithOptions(self.view.bounds.size, YES, 0.0);
    [self.view.layer renderInContext:UIGraphicsGetCurrentContext()];
    
    UIImage * img = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    self.actionButton.hidden = NO;
    self.menuButton.hidden = NO;
    
    return img;
    
}

-(ALGSavedProblem*) problemForSaving:(ALGContainerViewController *)actionVC {
    
    NSArray *leftPositiveExpressionTerms = [self.leftExpressionMatVC positiveSavedExpressionTerms];
    NSArray *leftNegativeExpressionTerms = [self.leftExpressionMatVC negativeSavedExpressionTerms];
    NSArray *rightPositiveExpressionTerms = [self.rightExpressionMatVC positiveSavedExpressionTerms];
    NSArray *rightNegativeExpressionsTerms = [self.rightExpressionMatVC negativeSavedExpressionTerms];
    
    ALGSavedProblem *problem = [[ALGSavedProblem alloc] init];
    
    UIImage *fullSizeImage = [self imageForSharing:nil];
    CGSize newSize = CGSizeMake(fullSizeImage.size.width * .5, fullSizeImage.size.height * .5);
    UIGraphicsBeginImageContextWithOptions(newSize, YES, 0.0);
    [fullSizeImage drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage* scaledImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    problem.previewImage = scaledImage;
    problem.matActivityID = ALGComparisionMatActivityID;
    problem.positiveTermsForMatOne = leftPositiveExpressionTerms;
    problem.negativeTermsForMatOne = leftNegativeExpressionTerms;
    problem.positiveTermsForMatTwo = rightPositiveExpressionTerms;
    problem.negativeTermsForMatTwo = rightNegativeExpressionsTerms;
    
    return problem;
}

#pragma mark - Expression Label

-(void) updateExpressionLabels {
    
    ALGExpressionModel *leftExpressionModel = [self.leftExpressionMatVC expressionModel];
    NSString *leftStringValue = [leftExpressionModel expressionStringValue];
    self.leftExpressionLabel.text = leftStringValue;
    
    ALGExpressionModel *rightExpressionModel = [self.rightExpressionMatVC expressionModel];
    NSString *rightStringValue = [rightExpressionModel expressionStringValue];
    self.rightExpressionLabel.text = rightStringValue;
    
}

#pragma mark - Tile Toolbox Delegate

-(BOOL) tileToolBoxVC:(ALGTileToolboxViewController *)toolboxVC shouldGiveTile:(ALGTileView *)tileView {
    return YES;
}

-(void) tileToolBoxVC:(ALGTileToolboxViewController *)toolboxVC didGiveTile:(ALGTileView *)tileView {
    
    tileView.center = [tileView.superview convertPoint:tileView.center toView:self.view];
    [self.view addSubview:tileView];
    tileView.canFlipToNegative = YES;
    tileView.touchDelegate = self;
    [self didStartTileViewPan:tileView];
    
}

#pragma Tile View Group Helper

-(ALGTileGroupModel*)groupModelBeingDraggedForTileView:(ALGTileView*)tileView {
    
    __block ALGTileGroupModel *foundGroup = nil;
    
    //Search TileGroups Being Dragged
    [self.tileGroupsBeingDragged enumerateObjectsUsingBlock:^(ALGTileGroupModel *groupModel, BOOL *stop) {
        if ([groupModel containsTile:tileView]) {
            foundGroup = groupModel;
            *stop = YES;
        }
    }];
    return foundGroup;
}

#pragma mark - Tile View Touch Delegate

-(void) didStartTileViewPan:(ALGTileView*)tileView {
    
    ALGTileGroupModel *groupToManipulate = nil;
    
    //Search Expression Mats
    if (!groupToManipulate) {
        groupToManipulate = [self.leftExpressionMatVC groupForTileView:tileView];
    }
    
    if (!groupToManipulate) {
        groupToManipulate = [self.rightExpressionMatVC groupForTileView:tileView];
    }
    
    if (groupToManipulate) {
        //Check if top tile, if so remove from group and create a new one
        if ([[groupToManipulate tiles] count] > 1 && tileView == [groupToManipulate topTile]) {
            [groupToManipulate removeTile:tileView];
            groupToManipulate = [ALGTileGroupModel groupWithTiles:@[ tileView ]];
        }
    } else {
        //Still haven't found a group, must not have been placed in one yet
        groupToManipulate = [ALGTileGroupModel groupWithTiles:@[ tileView ]];
    }
    
    [self.leftExpressionMatVC unclaimGroupIfOwned:groupToManipulate];
    [self.rightExpressionMatVC unclaimGroupIfOwned:groupToManipulate];
    
    [self.tileGroupsBeingDragged addObject:groupToManipulate];
    
    for (ALGTileView *view in groupToManipulate.tiles) {
        [view addShadowAnimated:YES];
        [self.view bringSubviewToFront:view];
    }
    
}

-(void) didPanTileView:(ALGTileView*)tileView withTranslation:(CGPoint)translation {
    
    ALGTileGroupModel *groupForTile = [self groupModelBeingDraggedForTileView:tileView];
    
    for (ALGTileView *view in groupForTile.tiles) {
        [view translateTileCenter:translation];
    }
    
}

-(void) didFinishTileViewPan:(ALGTileView*)tileView {
    
    ALGTileGroupModel *groupForTile = [self groupModelBeingDraggedForTileView:tileView];
    
    for (ALGTileView *view in groupForTile.tiles) {
        [view removeShadowAnimated:YES];
    }
    
    [self.tileGroupsBeingDragged removeObject:groupForTile];
    
    if ([self.leftExpressionMatVC willClaimTileGroup:groupForTile]) {
        //claim it
        [self.leftExpressionMatVC claimTileGroup:groupForTile];
        
    } else if ([self.rightExpressionMatVC willClaimTileGroup:groupForTile]) {
        
        [self.rightExpressionMatVC claimTileGroup:groupForTile];

    } else {
        for (ALGTileView *view in groupForTile.tiles) {
            [view removeTileAnimated:YES];
        }
    }
    
    [self updateExpressionLabels];
    
}

-(void) didFlipTile:(ALGTileView*)tileView {
    
    ALGTileGroupModel *groupToManipulate = nil;
    
    //Search Expression Mats
    if (!groupToManipulate) {
        groupToManipulate = [self.leftExpressionMatVC groupForTileView:tileView];
    }
    
    if (!groupToManipulate) {
        groupToManipulate = [self.rightExpressionMatVC groupForTileView:tileView];
    }
    
    for (ALGTileView *view in groupToManipulate.tiles) {
        if (view != tileView) {
            [view flipTileAnimated:YES];
        }
    }
    
    [self updateExpressionLabels];
    
}

-(void) setupInitialProblem {

    for (ALGSavedExpressionTerm *savedTerm in self.initialProblem.positiveTermsForMatOne) {
        NSMutableArray *tilesToBeAdded = [NSMutableArray array];
        for (int i = 0; i < savedTerm.coefficient; i++) {
            ALGTileModel *tileModel = [ALGTileModel tileWithType:savedTerm.tileType];
            tileModel.negative = savedTerm.negative;
            ALGTileView *tileView = [ALGTileView tileWithTileModel:tileModel andTheme:[[ALGTileThemeManager sharedThemeManager] currentTheme]];
            [self.view addSubview:tileView];
            tileView.canFlipToNegative = YES;
            tileView.touchDelegate = self;
            tileView.center = savedTerm.center;
            [tilesToBeAdded addObject:tileView];
        }
        
        ALGTileGroupModel *group = [ALGTileGroupModel groupWithTiles:tilesToBeAdded];
        if ([self.leftExpressionMatVC willClaimTileGroup:group]) {
            [self.leftExpressionMatVC claimTileGroup:group];
        } else {
            NSLog(@"Not Claimed by expression mat");
        }
    }
    
    for (ALGSavedExpressionTerm *savedTerm in self.initialProblem.negativeTermsForMatOne) {
        NSMutableArray *tilesToBeAdded = [NSMutableArray array];
        for (int i = 0; i < savedTerm.coefficient; i++) {
            ALGTileModel *tileModel = [ALGTileModel tileWithType:savedTerm.tileType];
            tileModel.negative = savedTerm.negative;
            ALGTileView *tileView = [ALGTileView tileWithTileModel:tileModel andTheme:[[ALGTileThemeManager sharedThemeManager] currentTheme]];
            [self.view addSubview:tileView];
            tileView.canFlipToNegative = YES;
            tileView.touchDelegate = self;
            tileView.center = savedTerm.center;
            [tilesToBeAdded addObject:tileView];
        }
        
        ALGTileGroupModel *group = [ALGTileGroupModel groupWithTiles:tilesToBeAdded];
        if ([self.leftExpressionMatVC willClaimTileGroup:group]) {
            [self.leftExpressionMatVC claimTileGroup:group];
        } else {
            NSLog(@"Not Claimed by expression mat");
        }
    }
    
    for (ALGSavedExpressionTerm *savedTerm in self.initialProblem.positiveTermsForMatTwo) {
        NSMutableArray *tilesToBeAdded = [NSMutableArray array];
        for (int i = 0; i < savedTerm.coefficient; i++) {
            ALGTileModel *tileModel = [ALGTileModel tileWithType:savedTerm.tileType];
            tileModel.negative = savedTerm.negative;
            ALGTileView *tileView = [ALGTileView tileWithTileModel:tileModel andTheme:[[ALGTileThemeManager sharedThemeManager] currentTheme]];
            [self.view addSubview:tileView];
            tileView.canFlipToNegative = YES;
            tileView.touchDelegate = self;
            tileView.center = savedTerm.center;
            [tilesToBeAdded addObject:tileView];
        }
        
        ALGTileGroupModel *group = [ALGTileGroupModel groupWithTiles:tilesToBeAdded];
        if ([self.rightExpressionMatVC willClaimTileGroup:group]) {
            [self.rightExpressionMatVC claimTileGroup:group];
        } else {
            NSLog(@"Not Claimed by expression mat");
        }
    }
    
    for (ALGSavedExpressionTerm *savedTerm in self.initialProblem.negativeTermsForMatTwo) {
        NSMutableArray *tilesToBeAdded = [NSMutableArray array];
        for (int i = 0; i < savedTerm.coefficient; i++) {
            ALGTileModel *tileModel = [ALGTileModel tileWithType:savedTerm.tileType];
            tileModel.negative = savedTerm.negative;
            ALGTileView *tileView = [ALGTileView tileWithTileModel:tileModel andTheme:[[ALGTileThemeManager sharedThemeManager] currentTheme]];
            [self.view addSubview:tileView];
            tileView.canFlipToNegative = YES;
            tileView.touchDelegate = self;
            tileView.center = savedTerm.center;
            [tilesToBeAdded addObject:tileView];
        }
        
        ALGTileGroupModel *group = [ALGTileGroupModel groupWithTiles:tilesToBeAdded];
        if ([self.rightExpressionMatVC willClaimTileGroup:group]) {
            [self.rightExpressionMatVC claimTileGroup:group];
        } else {
            NSLog(@"Not Claimed by expression mat");
        }
    }
    
    [self updateExpressionLabels];

}

@end
