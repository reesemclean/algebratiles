//
//  ALGTileThemeChooserViewController.m
//  AlgebraTiles
//
//  Created by Reese McLean on 3/27/14.
//  Copyright (c) 2014 Reese McLean. All rights reserved.
//

#import "ALGTileThemeChooserViewController.h"

#import "ALGTileThemeManager.h"
#import "ALGThemeExampleCell.h"

@interface ALGTileThemeChooserViewController ()

- (IBAction)doneButtonPressed:(id)sender;
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, strong) NSArray *themes;

@end

@implementation ALGTileThemeChooserViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.themes = [[ALGTileThemeManager sharedThemeManager] availableThemes];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)doneButtonPressed:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return [self.themes count];
    
}

-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 273.0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *ThemeExampleCellID = @"ThemeExampleCellID";

    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:ThemeExampleCellID forIndexPath:indexPath];

    return cell;
}

-(void) tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    ALGThemeExampleCell *themeCell = (ALGThemeExampleCell*)cell;
    [themeCell setTheme:self.themes[indexPath.row]];
    
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    [[ALGTileThemeManager sharedThemeManager] setCurrentTheme:self.themes[indexPath.row]];
}

@end
