//
//  ALGTileToolboxViewController.h
//  AlgebraTiles
//
//  Created by Reese McLean on 4/20/13.
//  Copyright (c) 2013 Reese McLean. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ALGTileView.h"

@protocol ALGTileToolboxDelegate;

@interface ALGTileToolboxViewController : UIViewController <ALGTileViewTouchDelegate, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout>

@property (nonatomic, weak) id<ALGTileToolboxDelegate> delegate;

@end

@protocol ALGTileToolboxDelegate <NSObject>

-(void) tileToolBoxVC:(ALGTileToolboxViewController*)toolboxVC didGiveTile:(ALGTileView*)tileView;

@optional
-(BOOL) tileToolBoxVC:(ALGTileToolboxViewController *)toolboxVC shouldGiveTile:(ALGTileView *)tileView;

@end
