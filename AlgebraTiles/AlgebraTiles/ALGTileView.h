//
//  ALGTileView.h
//  AlgebraTiles
//
//  Created by Reese McLean on 4/20/13.
//  Copyright (c) 2013 Reese McLean. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ALGTileModel;
@class ALGTileTheme;

@protocol ALGTileViewTouchDelegate;

@interface ALGTileView : UIView

@property (nonatomic, weak) id<ALGTileViewTouchDelegate> touchDelegate;

@property (nonatomic, strong) ALGTileTheme *tileTheme;

+(instancetype) tileWithTileModel:(ALGTileModel*)tileModel andTheme:(ALGTileTheme*)tileTheme;
+(CGSize) sizeForTileModel:(ALGTileModel*) tileModel;

-(void) translateTileCenter:(CGPoint) translation;

@property (readonly, copy, nonatomic) ALGTileModel *tileModel;

-(void) flipTileAnimated:(BOOL)animated;
-(void) addShadowAnimated:(BOOL)animated;
-(void) removeShadowAnimated:(BOOL)animated;
-(void) removeTileAnimated:(BOOL)animated;

-(BOOL) isSameTypeOfTile:(ALGTileView*)tileToTest;

@property (nonatomic, assign) BOOL canFlipToNegative;

@property (nonatomic, assign) BOOL vertical;
-(void) rotateToVertical:(BOOL)vertical animated:(BOOL)animated;
@property (nonatomic, readonly) BOOL isSnapableToOutSideAreaModel;
-(NSString*)tileLengthTitleForSide:(CGRectEdge) rectEdge;

@property (nonatomic, readonly, assign) CGPoint topLeftPoint;
@property (nonatomic, readonly, assign) CGPoint topRightPoint;
@property (nonatomic, readonly, assign) CGPoint bottomLeftPoint;
@property (nonatomic, readonly, assign) CGPoint bottomRightPoint;
@property (nonatomic, readonly, assign) CGFloat trueWidth;
@property (nonatomic, readonly, assign) CGFloat trueHeight;

@property (nonatomic, assign) BOOL respondsToThemeChange;

@end

@protocol ALGTileViewTouchDelegate <NSObject>

-(void) didStartTileViewPan:(ALGTileView*)tileView;

@optional
-(void) didPanTileView:(ALGTileView*)tileView withTranslation:(CGPoint)translation;
-(void) didFinishTileViewPan:(ALGTileView*)tileView;
-(void) didFlipTile:(ALGTileView*)tileView;

@end
