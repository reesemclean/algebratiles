//
//  ALGInstructionController.h
//  AlgebraTiles
//
//  Created by Reese McLean on 5/9/13.
//  Copyright (c) 2013 Reese McLean. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "ALGInstructionStepModel.h"

@class ALGTileIntroductionActivityViewController;

@interface ALGInstructionController : NSObject

-(NSUInteger) numberOfSteps;
-(NSUInteger) currentStepNumber;

-(ALGInstructionStepModel*)currentStep;
-(ALGInstructionStepModel*)nextStep;
-(ALGInstructionStepModel*)previousStep;

-(id) initWithActivityController:(ALGTileIntroductionActivityViewController*)activityVC;

@property (nonatomic, weak) ALGTileIntroductionActivityViewController *activityViewController;

@end
