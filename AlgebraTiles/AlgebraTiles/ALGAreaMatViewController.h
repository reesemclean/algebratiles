//
//  ALGAreaMatViewController.h
//  AlgebraTiles
//
//  Created by Reese McLean on 4/21/13.
//  Copyright (c) 2013 Reese McLean. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ALGTileView;

@interface ALGAreaMatViewController : UIViewController

-(BOOL) willClaimTile:(ALGTileView*)tileView;
-(void) didPanTile:(ALGTileView*)tileView;
-(void) didFinishTilePan:(ALGTileView*) tileView;
-(void) didStartPanTile:(ALGTileView*)tileView;

-(NSArray *)savedExpressionTermsAlongBottom;
-(NSArray *)savedExpressionTermsAlongLeft;
-(NSArray *)savedExpressionTermsInCenter;

-(void) addTile:(ALGTileView*) tileView isVertical:(BOOL)vertical atCenter:(CGPoint)center;

-(void) adjustOutsideLabels;

@end
