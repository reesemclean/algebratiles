//
//  ATExpressionModel.h
//  AlgebraTiles
//
//  Created by Reese McLean on 12/8/12.
//  Copyright (c) 2012 Reese McLean. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString * const ALG_COEFFICIENT_UNIT_KEY;
extern NSString * const ALG_COEFFICIENT_X_KEY;
extern NSString * const ALG_COEFFICIENT_X_SQUARED_KEY;
extern NSString * const ALG_COEFFICIENT_Y_KEY;
extern NSString * const ALG_COEFFICIENT_Y_SQUARED_KEY;
extern NSString * const ALG_COEFFICIENT_XY_KEY;
extern NSString * const ALG_COEFFICIENT_NEGATIVE_UNIT_KEY;
extern NSString * const ALG_COEFFICIENT_NEGATIVE_X_KEY;
extern NSString * const ALG_COEFFICIENT_NEGATIVE_X_SQUARED_KEY;
extern NSString * const ALG_COEFFICIENT_NEGATIVE_Y_KEY;
extern NSString * const ALG_COEFFICIENT_NEGATIVE_Y_SQUARED_KEY;
extern NSString * const ALG_COEFFICIENT_NEGATIVE_XY_KEY;

@interface ALGExpressionModel : NSObject

-(id) initWithPositiveTermsModels:(NSArray *)postiveModels andNegativeModels:(NSArray *)negativeModels;
+(id) expressionWithPositiveTerms:(NSArray*)positiveTerms andNegativeTerms:(NSArray*)negativeTerms;

@property (nonatomic, strong) NSArray *positiveTerms;
@property (nonatomic, strong) NSArray *negativeTerms;

-(NSString*)expressionStringValue;

@end
