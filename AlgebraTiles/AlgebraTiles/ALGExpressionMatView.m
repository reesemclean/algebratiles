//
//  ALGExpressionMatView.m
//  AlgebraTiles
//
//  Created by Reese McLean on 4/20/13.
//  Copyright (c) 2013 Reese McLean. All rights reserved.
//

#import "ALGExpressionMatView.h"

@implementation ALGExpressionMatView

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetStrokeColorWithColor(context, [[UIColor lightGrayColor] CGColor]);
    CGContextSetLineWidth(context, 1.0);
    
    CGRect insetRect = CGRectInset(self.bounds, .5, .5);
    CGContextStrokeRect(context, insetRect);
    
    if (_hasTopAndBottom) {
        CGMutablePathRef path = CGPathCreateMutable();
        CGFloat midY = self.bounds.size.height/2.0 - .5;
        CGPathMoveToPoint(path, NULL, .5, midY);
        CGPathAddLineToPoint(path, NULL, self.bounds.size.width - .5, midY);
        CGContextAddPath(context, path);
        
        CGContextStrokePath(context);
        
        CGPathRelease(path);
        
        CGContextSetStrokeColorWithColor(context, [[UIColor darkGrayColor] CGColor]);
        CGContextSetLineWidth(context, 2.0);
        
        CGMutablePathRef plusSignPath = CGPathCreateMutable();
        CGPathMoveToPoint(plusSignPath, NULL, 22.0, 12.0);
        CGPathAddLineToPoint(plusSignPath, NULL, 22.0, 32.0);
        CGPathMoveToPoint(plusSignPath, NULL, 12.0, 22.0);
        CGPathAddLineToPoint(plusSignPath, NULL, 32.0, 22.0);
        CGContextAddPath(context, plusSignPath);
        CGContextStrokePath(context);
        CGPathRelease(plusSignPath);
        
        CGMutablePathRef negativeSignPath = CGPathCreateMutable();
        CGPathMoveToPoint(negativeSignPath, NULL, 12.0, self.bounds.size.height - 22.0);
        CGPathAddLineToPoint(negativeSignPath, NULL, 32.0, self.bounds.size.height - 22.0);
        CGContextAddPath(context, negativeSignPath);
        CGContextStrokePath(context);
        CGPathRelease(negativeSignPath);
    }

}

@end
