//
//  ALGExpressionMatActivityViewController.h
//  AlgebraTiles
//
//  Created by Reese McLean on 4/20/13.
//  Copyright (c) 2013 Reese McLean. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ALGTileView.h"
#import "ALGTileToolboxViewController.h"

#import "ALGContainerViewController.h"

@class ALGSavedProblem;

@interface ALGExpressionMatActivityViewController : UIViewController <ALGTileViewTouchDelegate, ALGTileToolboxDelegate, ALGActionDataSource>

@property (nonatomic, assign) BOOL hasTopAndBottom;
@property (nonatomic, strong) ALGSavedProblem *initialProblem;

+(instancetype)viewControllerForFreePlayMatFromStoryboard:(UIStoryboard*)storyboard withProblem:(ALGSavedProblem*)savedProblem;
+(instancetype)viewControllerForExpressionMatFromStoryboard:(UIStoryboard*)storyboard withProblem:(ALGSavedProblem*)savedProblem;

@end
