//
//  ALGTileTheme.m
//  AlgebraTiles
//
//  Created by Reese McLean on 3/27/14.
//  Copyright (c) 2014 Reese McLean. All rights reserved.
//

#import "ALGTileTheme.h"

NSString *const ALGplistUniqueIDKey = @"uniqueID";
NSString *const ALGplistNameKey = @"name";
NSString *const ALGplistUnitColorRedKey = @"unitColorRed";
NSString *const ALGplistUnitColorGreenKey = @"unitColorGreen";
NSString *const ALGplistUnitColorBlueKey = @"unitColorBlue";
NSString *const ALGplistXColorRedKey = @"xColorRed";
NSString *const ALGplistXColorGreenKey = @"xColorGreen";
NSString *const ALGplistXColorBlueKey = @"xColorBlue";
NSString *const ALGplistYColorRedKey = @"yColorRed";
NSString *const ALGplistYColorGreenKey = @"yColorGreen";
NSString *const ALGplistYColorBlueKey = @"yColorBlue";
NSString *const ALGplistXYColorRedKey = @"xyColorRed";
NSString *const ALGplistXYColorGreenKey = @"xyColorGreen";
NSString *const ALGplistXYColorBlueKey = @"xyColorBlue";
NSString *const ALGplistNegativeColorRedKey = @"negativeColorRed";
NSString *const ALGplistNegativeColorGreenKey = @"negativeColorGreen";
NSString *const ALGplistNegativeColorBlueKey = @"negativeColorBlue";

@interface ALGTileTheme ()

@property (nonatomic, readwrite) NSInteger uniqueID;
@property (nonatomic, readwrite) NSString *name;
@property (nonatomic, readwrite) UIColor *unitColor;
@property (nonatomic, readwrite) UIColor *xColor;
@property (nonatomic, readwrite) UIColor *yColor;
@property (nonatomic, readwrite) UIColor *xyColor;
@property (nonatomic, readwrite) UIColor *negativeColor;

@end

@implementation ALGTileTheme

+(instancetype) tileThemeFromDictionary:(NSDictionary*) tileDictionary {
    
    ALGTileTheme *theme = [[ALGTileTheme alloc] init];
    theme.uniqueID = [tileDictionary[ALGplistUniqueIDKey] integerValue];
    theme.name = tileDictionary[ALGplistNameKey];
    
    UIColor *unitColor = [UIColor colorWithRed:[tileDictionary[ALGplistUnitColorRedKey] floatValue]/255.0
                                      green:[tileDictionary[ALGplistUnitColorGreenKey] floatValue]/255.0
                                       blue:[tileDictionary[ALGplistUnitColorBlueKey] floatValue]/255.0
                                      alpha:1.0];
    theme.unitColor = unitColor;
    
    UIColor *xColor = [UIColor colorWithRed:[tileDictionary[ALGplistXColorRedKey] floatValue]/255.0
                                      green:[tileDictionary[ALGplistXColorGreenKey] floatValue]/255.0
                                       blue:[tileDictionary[ALGplistXColorBlueKey] floatValue]/255.0
                                      alpha:1.0];
    theme.xColor = xColor;

    UIColor *yColor = [UIColor colorWithRed:[tileDictionary[ALGplistYColorRedKey] floatValue]/255.0
                                      green:[tileDictionary[ALGplistYColorGreenKey] floatValue]/255.0
                                       blue:[tileDictionary[ALGplistYColorBlueKey] floatValue]/255.0
                                      alpha:1.0];
    theme.yColor = yColor;
    
    UIColor *xyColor = [UIColor colorWithRed:[tileDictionary[ALGplistXYColorRedKey] floatValue]/255.0
                                      green:[tileDictionary[ALGplistXYColorGreenKey] floatValue]/255.0
                                       blue:[tileDictionary[ALGplistXYColorBlueKey] floatValue]/255.0
                                      alpha:1.0];
    theme.xyColor = xyColor;
    
    UIColor *negativeColor = [UIColor colorWithRed:[tileDictionary[ALGplistNegativeColorRedKey] floatValue]/255.0
                                      green:[tileDictionary[ALGplistNegativeColorGreenKey] floatValue]/255.0
                                       blue:[tileDictionary[ALGplistNegativeColorBlueKey] floatValue]/255.0
                                      alpha:1.0];
    theme.negativeColor = negativeColor;
    
    return theme;
}

@end
