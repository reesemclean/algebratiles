//
//  ALGTileToolboxCell.h
//  AlgebraTiles
//
//  Created by Reese McLean on 4/20/13.
//  Copyright (c) 2013 Reese McLean. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ALGTileView.h"

@class ALGTileModel;

@interface ALGTileToolboxCell : UICollectionViewCell

+(CGFloat) cellHeightForTileModel:(ALGTileModel*)tileModel;

@property (readonly, strong, nonatomic) ALGTileModel *tileModel;

-(void) setTileModel:(ALGTileModel *)tileModel withTouchDelegate:(id<ALGTileViewTouchDelegate>)touchDelegate;

@property (weak, nonatomic) IBOutlet UILabel *label;
@property (nonatomic, strong) ALGTileView *tileView;

-(void) replaceTileViewAnimated:(BOOL)animated withTouchDelegate:(id<ALGTileViewTouchDelegate>)touchDelegate;

@end
