//
//  ALGTileConstants.h
//  AlgebraTiles
//
//  Created by Reese McLean on 4/20/13.
//  Copyright (c) 2013 Reese McLean. All rights reserved.
//

#ifndef AlgebraTiles_TileConstants_h
#define AlgebraTiles_TileConstants_h

#define ALG_UNIT_LENGTH 44.0
#define ALG_X_LENGTH 116.0
#define ALG_Y_LENGTH 144.0

typedef enum ALGTileType {
    ALGTileTypeUnit,
    ALGTileTypeX,
    ALGTileTypeXSquared,
    ALGTileTypeY,
    ALGTileTypeYSquared,
    ALGTileTypeXY
} ALGTileType;

#endif
