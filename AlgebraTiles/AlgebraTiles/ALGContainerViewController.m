//
//  ALGContainerViewController.m
//  AlgebraTiles
//
//  Created by Reese McLean on 4/20/13.
//  Copyright (c) 2013 Reese McLean. All rights reserved.
//

#import "ALGContainerViewController.h"

#import "ALGTileToolboxViewController.h"

#import "ALGTileThemeChooserViewController.h"

#import <QuartzCore/QuartzCore.h>

#import "ALGTileIntroductionActivityViewController.h"
#import "ALGExpressionMatActivityViewController.h"
#import "ALGComparisionMatActivityViewController.h"
#import "ALGEquationMatActivityViewController.h"
#import "ALGAreaMatActivityViewController.h"

#import "ALGActionRootViewController.h"

#import "ALGSavedProblemsController.h"
#import "ALGSavedProblem.h"

#define kMenuAnimationSpeed .33

NSString * const TILE_TOOLBOX_ON_LEFT = @"TILE_TOOLBOX_ON_LEFT";
NSString * const LAST_USED_ACTIVITY_ID = @"LAST_USED_ACTIVITY_ID";

@interface ALGContainerViewController ()

@property (nonatomic, strong) NSString *currentActivityID;

@property (weak, nonatomic) IBOutlet UIView *toolboxContainerView;
@property (weak, nonatomic) IBOutlet UIView *activityContainerView;

@property (nonatomic, strong) ALGTileToolboxViewController *tileToolboxViewController;
@property (nonatomic, strong) UIViewController<ALGTileToolboxDelegate, ALGActionDataSource> *currentActivityViewController;

@property (nonatomic, strong) ALGMenuViewController *menuViewController;
@property (nonatomic, strong) UIView *blackoutView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentTrailingSpaceConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentLeadingSpaceConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *toolboxHorizontalSpaceConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *toolboxWidthConstraint;

@property (nonatomic, assign) BOOL toolboxOnLeft;

@property (nonatomic, strong) UIPopoverController *actionButtonPopoverController;
@property (nonatomic, strong) ALGActionRootViewController *actionButtonRootViewController;
@property (nonatomic, assign) CGRect savedActionButtonRect;

@property (nonatomic, strong) UIPopoverController *shareImagePopoverController;

@end

@implementation ALGContainerViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.tileToolboxViewController.delegate = self.currentActivityViewController;

	// Do any additional setup after loading the view.
    NSDictionary *defaults = @{TILE_TOOLBOX_ON_LEFT : @YES, LAST_USED_ACTIVITY_ID : ALGExpressionMatActivityID };
    
    [[NSUserDefaults standardUserDefaults] registerDefaults:defaults];
    [[NSUserDefaults standardUserDefaults] synchronize];
        
    self.toolboxOnLeft = [[NSUserDefaults standardUserDefaults] boolForKey:TILE_TOOLBOX_ON_LEFT];
    
    [self positionToolboxOnLeft:self.toolboxOnLeft animated:NO];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
}

-(void) viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    self.tileToolboxViewController.delegate = self.currentActivityViewController;
    
    [self positionToolboxOnLeft:self.toolboxOnLeft animated:NO];

    NSString *lastUsedActivityID = [[NSUserDefaults standardUserDefaults] stringForKey:LAST_USED_ACTIVITY_ID];
    if (lastUsedActivityID) {
        [self transitionToMatViewController:[self viewControllerForActivityID:lastUsedActivityID withProblem:nil] animated:YES];
    } else {
        lastUsedActivityID = ALGExpressionMatActivityID;
        [self transitionToMatViewController:[self viewControllerForActivityID:ALGExpressionMatActivityID withProblem:nil] animated:YES];
    }
    
    self.currentActivityID = lastUsedActivityID;
    
}

-(void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {

    if ([segue.identifier isEqualToString:@"ALGToolboxVCSegueID"]) {
        self.tileToolboxViewController = segue.destinationViewController;
    } else if ([segue.identifier isEqualToString:@"ALGEmbedActivityVCSegueID"]) {
        self.currentActivityViewController = segue.destinationViewController;
        self.tileToolboxViewController.delegate = self.currentActivityViewController;
    } else if ([segue.identifier isEqualToString:@"ALGSavedProblemViewerSequeID"]) {
        UINavigationController *navVC = [segue destinationViewController];
        ALGSavedProblemViewerViewController *vc = (ALGSavedProblemViewerViewController*)[navVC topViewController];
        vc.delegate = self;
    }
    
}

- (void)menuButtonPushed:(id)sender {
    
    [self showMenuAnimated:YES];
    
}

-(void) actionButtonPushed:(id)sender {
    
    self.actionButtonRootViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ALGActionButtonViewControllerID"];
    self.actionButtonRootViewController.delegate = self;

    UINavigationController *navVC = [[UINavigationController alloc] initWithRootViewController:self.actionButtonRootViewController];
    
    self.actionButtonPopoverController = [[UIPopoverController alloc] initWithContentViewController:navVC];
    
    CGRect rect = [self.view convertRect:[sender frame] fromView:[sender superview]];
    self.savedActionButtonRect = rect;
    [self.actionButtonPopoverController presentPopoverFromRect:rect
                                                            inView:self.view
                                          permittedArrowDirections:UIPopoverArrowDirectionAny
                                                          animated:YES];
    
}

#pragma mark - Action VC Delegate

-(void) didSelectShareImageInActionVC:(ALGActionRootViewController*)actionVC {
    
    [self.actionButtonPopoverController dismissPopoverAnimated:NO];
    
    UIImage *imageToShare = [self.actionVCDataSource imageForSharing:self];
    
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:@[ imageToShare ]
                                                                             applicationActivities:nil];
    activityVC.excludedActivityTypes = @[ UIActivityTypeAssignToContact, UIActivityTypePostToTwitter, UIActivityTypePostToFacebook, UIActivityTypePostToWeibo ];
    activityVC.completionWithItemsHandler = ^(UIActivityType  _Nullable activityType, BOOL completed, NSArray * _Nullable returnedItems, NSError * _Nullable activityError) {
        [self.shareImagePopoverController dismissPopoverAnimated:YES];
    };
    self.shareImagePopoverController = [[UIPopoverController alloc] initWithContentViewController:activityVC];
    
    [self.shareImagePopoverController presentPopoverFromRect:self.savedActionButtonRect
                                                        inView:self.view
                                      permittedArrowDirections:UIPopoverArrowDirectionAny
                                                      animated:YES];
    //self.actionButtonPopoverController.contentViewController = activityVC;
    
}

-(void) didSelectSaveProblemInActionVC:(ALGActionRootViewController*)actionVC {
    
    ALGSavedProblem *problem = [self.actionVCDataSource problemForSaving:self];
    [[ALGSavedProblemsController sharedController] addProblem:problem];
    [self.actionButtonPopoverController dismissPopoverAnimated:YES];
    
}

-(void) positionToolboxOnLeft:(BOOL) onLeft animated:(BOOL)animated {
    
    [self.view exchangeSubviewAtIndex:[self.view.subviews indexOfObject:self.toolboxContainerView]
                   withSubviewAtIndex:[self.view.subviews indexOfObject:self.activityContainerView]];
    
    [self.view removeConstraint:self.toolboxHorizontalSpaceConstraint];

    if (onLeft) {
        self.contentTrailingSpaceConstraint.constant = 0.0;
        self.contentLeadingSpaceConstraint.constant = self.toolboxWidthConstraint.constant;
        
        self.toolboxHorizontalSpaceConstraint = [NSLayoutConstraint constraintWithItem:self.toolboxContainerView
                                                                             attribute:NSLayoutAttributeLeft
                                                                             relatedBy:NSLayoutRelationEqual
                                                                                toItem:self.view
                                                                             attribute:NSLayoutAttributeLeft
                                                                            multiplier:1.0
                                                                              constant:0.0];
        
    } else {
        
        self.contentTrailingSpaceConstraint.constant = -(self.toolboxWidthConstraint.constant);
        self.contentLeadingSpaceConstraint.constant = 0.0;
        
        self.toolboxHorizontalSpaceConstraint = [NSLayoutConstraint constraintWithItem:self.toolboxContainerView
                                                                             attribute:NSLayoutAttributeRight
                                                                             relatedBy:NSLayoutRelationEqual
                                                                                toItem:self.view
                                                                             attribute:NSLayoutAttributeRight
                                                                            multiplier:1.0
                                                                              constant:0.0];
        
    }
    
    [self.view addConstraint:self.toolboxHorizontalSpaceConstraint];

    [UIView animateWithDuration:animated ? .5 : 0.0
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseInOut | UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         [self.view layoutIfNeeded];
                     }
                     completion:^(BOOL finished) {
                         [self.view exchangeSubviewAtIndex:[self.view.subviews indexOfObject:self.toolboxContainerView]
                                        withSubviewAtIndex:[self.view.subviews indexOfObject:self.activityContainerView]];
                     }];

    
}

-(void) transitionToMatViewController:(UIViewController<ALGTileToolboxDelegate, ALGActionDataSource>*) newMatViewController animated:(BOOL)animated {
    
    newMatViewController.view.frame = self.activityContainerView.bounds;
    
    [self addChildViewController:newMatViewController];
    [self.currentActivityViewController willMoveToParentViewController:nil];

    [UIView transitionWithView:self.activityContainerView
                      duration:animated ? kMenuAnimationSpeed : 0.0
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^{
                        
                        [self.activityContainerView addSubview:newMatViewController.view];
                        [self.currentActivityViewController.view removeFromSuperview];
                        
                    }
                    completion:^(BOOL finished) {
                        [self.currentActivityViewController removeFromParentViewController];
                        [newMatViewController didMoveToParentViewController:self];
                        self.currentActivityViewController = newMatViewController;
                        self.actionVCDataSource = self.currentActivityViewController;
                        self.tileToolboxViewController.delegate = self.currentActivityViewController;
                    }];
    
    
}

-(void) showMenuAnimated:(BOOL) animated {
    
    ALGMenuViewController *menuViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"MenuViewControllerID"];
    self.menuViewController = menuViewController;
    [self addChildViewController:menuViewController];
    
    CGFloat menuWidth = [ALGMenuViewController menuWidth];
    
    menuViewController.view.frame = CGRectMake(-menuWidth, 0.0, menuWidth, self.view.bounds.size.height);
    [self.view addSubview:menuViewController.view];
    
    menuViewController.view.layer.shadowRadius = 4.0;
    menuViewController.view.layer.shadowColor = [[UIColor blackColor] CGColor];
    menuViewController.view.layer.shadowOpacity = 0.0;

    CGPathRef shadowPath = CGPathCreateWithRect(menuViewController.view.bounds, NULL);
    
    menuViewController.view.layer.shadowPath = shadowPath;
    
    CGPathRelease(shadowPath);
    
    self.menuViewController = menuViewController;
    self.menuViewController.delegate = self;
    
    self.blackoutView = [[UIView alloc] initWithFrame:self.view.bounds];
    self.blackoutView.alpha = 0.0;
    self.blackoutView.backgroundColor = [UIColor blackColor];
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapInBlackoutView:)];
    [self.blackoutView addGestureRecognizer:tapGesture];
    
    [self.view insertSubview:self.blackoutView belowSubview:menuViewController.view];
    
    [UIView animateWithDuration:animated ? kMenuAnimationSpeed : 0.0
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         self.menuViewController.view.frame = CGRectMake(0.0, 0.0, self.menuViewController.view.frame.size.width, self.menuViewController.view.frame.size.height);
                         
                         self.blackoutView.alpha = .25;
                     }
                     completion:^(BOOL finished) {
                         menuViewController.view.layer.shadowOpacity = 1.0;
                         [self.menuViewController didMoveToParentViewController:self];
                     }];
    
}

-(void) hideMenuAnimated:(BOOL)animated {
    
    [self.menuViewController willMoveToParentViewController:nil];
    [UIView animateWithDuration:animated ? kMenuAnimationSpeed : 0.0
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         self.menuViewController.view.frame = CGRectMake(-self.menuViewController.view.frame.size.width, 0.0, self.menuViewController.view.frame.size.width, self.menuViewController.view.frame.size.height);
                         self.blackoutView.alpha = 0.0;
                     }
                     completion:^(BOOL finished) {
                         [self.menuViewController.view removeFromSuperview];
                         [self.menuViewController removeFromParentViewController];
                         self.menuViewController = nil;
                         [self.blackoutView removeFromSuperview];
                         self.blackoutView = nil;
                     }];
    
}

-(void) handleTapInBlackoutView:(UITapGestureRecognizer*)tapGestureRecognizer {
    [self hideMenuAnimated:YES];
}

-(UIViewController<ALGTileToolboxDelegate, ALGActionDataSource>*)viewControllerForActivityID:(NSString*)activityID withProblem:(ALGSavedProblem*)savedProblem {
    UIViewController<ALGTileToolboxDelegate, ALGActionDataSource> *vc = nil;
    
    if ([activityID isEqualToString:ALGIntroductionActivityID]) {
        vc = [ALGTileIntroductionActivityViewController viewControllerForIntroductionFromStoryboard:self.storyboard];
    } else if ([activityID isEqualToString:ALGFreePlayMatActivityID]) {
        vc = [ALGExpressionMatActivityViewController viewControllerForFreePlayMatFromStoryboard:self.storyboard withProblem:savedProblem];
    } else if ([activityID isEqualToString:ALGExpressionMatActivityID]) {
        vc = [ALGExpressionMatActivityViewController viewControllerForExpressionMatFromStoryboard:self.storyboard withProblem:savedProblem];
    } else if ([activityID isEqualToString:ALGComparisionMatActivityID]) {
        vc = [ALGComparisionMatActivityViewController viewControllerForComparisionMatFromStoryboard:self.storyboard withProblem:savedProblem];
    } else if ([activityID isEqualToString:ALGEquationMatActivityID]) {
        vc = [ALGEquationMatActivityViewController viewControllerForEquationMatFromStoryboard:self.storyboard withProblem:savedProblem];
    } else if ([activityID isEqualToString:ALGAreaMatActivityID]) {
        vc = [ALGAreaMatActivityViewController viewControllerForAreaMatFromStoryboard:self.storyboard withProblem:savedProblem];
    } else {
        return nil;
    }
    return vc;
}

#pragma mark - Menu View Delegate

-(void) menuViewdidPressToggleToolboxSide:(ALGMenuViewController *)menuVC {
    self.toolboxOnLeft = !self.toolboxOnLeft;
    [[NSUserDefaults standardUserDefaults] setBool:self.toolboxOnLeft forKey:TILE_TOOLBOX_ON_LEFT];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [self positionToolboxOnLeft:self.toolboxOnLeft animated:YES];
}

-(void) menuView:(ALGMenuViewController *)menuVC didSelectActivityWithID:(NSString *)activityID {
    
    if (![activityID isEqualToString:self.currentActivityID]) {
        self.currentActivityID = activityID;
        [[NSUserDefaults standardUserDefaults] setObject:activityID forKey:LAST_USED_ACTIVITY_ID];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [self transitionToMatViewController:[self viewControllerForActivityID:activityID withProblem:nil] animated:YES];
    }
    
    [self hideMenuAnimated:YES];
    
}

-(void) menuViewDidPressSelectTheme:(ALGMenuViewController *)menuVC {
    [self performSegueWithIdentifier:@"PresentThemeChooserSegueID" sender:nil];
}

-(void) menuViewDidPressSavedProblems:(ALGMenuViewController *)menuVC {
    [self performSegueWithIdentifier:@"ALGSavedProblemViewerSequeID" sender:nil];
}

#pragma mark - Saved Problem Viewer Delegate

-(void) savedProblemViewer:(ALGSavedProblemViewerViewController*) savedProblemViewerViewController didSelectSavedProblem:(ALGSavedProblem*)savedProblem {
    self.currentActivityID = savedProblem.matActivityID;
    [[NSUserDefaults standardUserDefaults] setObject:self.currentActivityID forKey:LAST_USED_ACTIVITY_ID];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [self transitionToMatViewController:[self viewControllerForActivityID:savedProblem.matActivityID withProblem:savedProblem] animated:YES];
    [self hideMenuAnimated:YES];
}


@end
