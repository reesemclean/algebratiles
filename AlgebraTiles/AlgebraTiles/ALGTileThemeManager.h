//
//  ALGTileTheme.h
//  AlgebraTiles
//
//  Created by Reese McLean on 3/27/14.
//  Copyright (c) 2014 Reese McLean. All rights reserved.
//

#import <Foundation/Foundation.h>

@class ALGTileTheme;

extern NSString *const ALGThemeDidChangeNotificationKey;

@interface ALGTileThemeManager : NSObject

@property (nonatomic, strong) ALGTileTheme *currentTheme;
@property (nonatomic, readonly) NSArray *availableThemes;
+(instancetype) sharedThemeManager;

@end
