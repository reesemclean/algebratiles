//
//  ALGAreaMatViewController.h
//  AlgebraTiles
//
//  Created by Reese McLean on 4/21/13.
//  Copyright (c) 2013 Reese McLean. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ALGTileView.h"
#import "ALGTileToolboxViewController.h"

#import "ALGContainerViewController.h"

@class ALGSavedProblem;

@interface ALGAreaMatActivityViewController : UIViewController <ALGTileViewTouchDelegate, ALGTileToolboxDelegate, ALGActionDataSource>

@property (nonatomic, strong) ALGSavedProblem *initialProblem;

+(instancetype)viewControllerForAreaMatFromStoryboard:(UIStoryboard*)storyboard withProblem:(ALGSavedProblem*)savedProblem;

@end
