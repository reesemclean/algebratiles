//
//  ALGTileModel.m
//  AlgebraTiles
//
//  Created by Reese McLean on 4/20/13.
//  Copyright (c) 2013 Reese McLean. All rights reserved.
//

#import "ALGTileModel.h"

@implementation ALGTileModel

-(id) initWithTileType:(ALGTileType)type {
    self = [super init];
    if (self) {
        _tileType = type;
        _negative = NO;
    }
    return self;
}

+(id) tileWithType:(ALGTileType)type {
    return [[ALGTileModel alloc] initWithTileType:type];
}

// In the implementation
-(id)copyWithZone:(NSZone *)zone
{
    ALGTileModel *anotherModel = [[ALGTileModel alloc] initWithTileType:self.tileType];
    anotherModel.negative = self.negative;
    
    return anotherModel;
}

-(NSString*) tileTitle {
    
    switch (self.tileType) {
        case ALGTileTypeUnit:
            return @"Unit";
            break;
        case ALGTileTypeX:
            return @"x";
            break;
        case ALGTileTypeXSquared:
            return @"x²";
            break;
        case ALGTileTypeXY:
            return @"xy";
            break;
        case ALGTileTypeY:
            return @"y";
            break;
        case ALGTileTypeYSquared:
            return @"y²";
            break;
        default:
            break;
    }
    
    return @"Unit";
    
}


@end
