//
//  ALGSavedProblemViewerViewController.m
//  AlgebraTiles
//
//  Created by Reese McLean on 3/30/14.
//  Copyright (c) 2014 Reese McLean. All rights reserved.
//

#import "ALGSavedProblemViewerViewController.h"

#import "ALGSavedProblemViewerCell.h"
#import "ALGSavedProblem.h"
#import "ALGSavedProblemsController.h"

@interface ALGSavedProblemViewerViewController ()

@property (strong, nonatomic) IBOutlet UICollectionView *collectionView;
@property (nonatomic, strong) NSArray *savedProblems;
- (IBAction)cancelButtonPressed:(id)sender;
@end

@implementation ALGSavedProblemViewerViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    NSArray *unorderedProblems = [[ALGSavedProblemsController sharedController] savedProblems];
    NSSortDescriptor* sortOrder = [NSSortDescriptor sortDescriptorWithKey: @"lastUsedDate" ascending: NO];
    self.savedProblems = [unorderedProblems sortedArrayUsingDescriptors:@[sortOrder]];
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) setEditing:(BOOL)editing animated:(BOOL)animated {
    
    [super setEditing:editing animated:animated];
    
    for (ALGSavedProblemViewerCell *cell in self.collectionView.visibleCells) {
        [cell setEditing:editing animated:animated];
    }
    
}

#pragma mark - UICollectionView Datasource

- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section {
    return self.savedProblems.count;
}

- (NSInteger)numberOfSectionsInCollectionView: (UICollectionView *)collectionView {
    return 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)cv cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    ALGSavedProblemViewerCell *cell = [cv dequeueReusableCellWithReuseIdentifier:@"ALGSavedProblemViewerCellID"
                                                                    forIndexPath:indexPath];
    
    ALGSavedProblem *problem = self.savedProblems[indexPath.row];
    cell.imageView.image = [problem previewImage];
    
    static NSDateFormatter *dateFormatter = nil;
    if (!dateFormatter) {
        dateFormatter = [[NSDateFormatter alloc] init];
        dateFormatter.dateStyle = NSDateFormatterMediumStyle;
        dateFormatter.timeStyle = NSDateFormatterNoStyle;
    }
    
    cell.dateLabel.text = [dateFormatter stringFromDate:problem.lastUsedDate];
    
    [cell setEditing:self.editing animated:NO];
    [cell.deleteButton addTarget:nil action:@selector(deleteButtonPushed:) forControlEvents:UIControlEventTouchUpInside];
    
    return cell;
}

-(void) deleteButtonPushed:(id)sender {
    
    UIButton *button = sender;
    CGPoint location = [self.collectionView convertPoint:button.center fromView:[button superview]];
    NSIndexPath *indexPath = [self.collectionView indexPathForItemAtPoint:location];
    ALGSavedProblem *problemToRemove = self.savedProblems[indexPath.row];
    
    [self.collectionView performBatchUpdates:^{
        [[ALGSavedProblemsController sharedController] removeProblem:problemToRemove];
        self.savedProblems = [[ALGSavedProblemsController sharedController] savedProblems];
        [self.collectionView deleteItemsAtIndexPaths:@[indexPath]];
    } completion:nil];
    
}

#pragma mark – UICollectionViewDelegateFlowLayout
#define cellWidth 250.0
#define cellHeight 246.0

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    return CGSizeMake(cellWidth, cellHeight);
    
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    
    CGFloat viewWidth = CGRectGetWidth(self.view.bounds);
    CGFloat availableWidth = viewWidth - cellWidth * 2.0;
    
    return floor(availableWidth/3.0);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return [self collectionView:collectionView layout:collectionViewLayout minimumInteritemSpacingForSectionAtIndex:section];
}

- (UIEdgeInsets)collectionView:
(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    CGFloat leftRightSpace = [self collectionView:collectionView layout:collectionViewLayout minimumInteritemSpacingForSectionAtIndex:section];
    return UIEdgeInsetsMake(18.0, leftRightSpace, 0, leftRightSpace);
}

- (IBAction)cancelButtonPressed:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - UICollectionViewDelegate

-(void) collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    [collectionView deselectItemAtIndexPath:indexPath animated:YES];
    
    ALGSavedProblem *problem = self.savedProblems[indexPath.row];
    [[ALGSavedProblemsController sharedController] updateLastUsedDateForProblem:problem];
    
    [self.delegate savedProblemViewer:self didSelectSavedProblem:problem];
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
