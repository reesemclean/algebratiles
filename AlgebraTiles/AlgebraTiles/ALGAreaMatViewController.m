//
//  ALGAreaMatViewController.m
//  AlgebraTiles
//
//  Created by Reese McLean on 4/21/13.
//  Copyright (c) 2013 Reese McLean. All rights reserved.
//

#import "ALGAreaMatViewController.h"

#import "ALGAreaMatView.h"

#import "ALGTileView.h"
#import "ALGTileModel.h"
#import "ALGTileConstants.h"

#import "ALGTileLengthLabel.h"

#import "ALGSavedExpressionTerm.h"

#define OUTSIDE_TILESPACE 82.0

@interface ALGAreaMatViewController ()

@property (nonatomic, strong) NSMutableSet *tilesBeingPanned;
@property (nonatomic, strong) NSMutableSet *leftTiles;
@property (nonatomic, strong) NSMutableSet *bottomTiles;
@property (nonatomic, strong) NSMutableSet *insideTiles;
@property (nonatomic, strong) NSMutableSet *lengthLabels;

@end

@implementation ALGAreaMatViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    ALGAreaMatView *myView = (ALGAreaMatView*)self.view;
    myView.outsideTilespace = OUTSIDE_TILESPACE;
    
    self.view.backgroundColor = [UIColor whiteColor];
    self.tilesBeingPanned = [NSMutableSet set];
    self.leftTiles = [NSMutableSet set];
    self.bottomTiles = [NSMutableSet set];
    self.insideTiles = [NSMutableSet set];
    self.lengthLabels = [NSMutableSet set];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(BOOL) willClaimTile:(ALGTileView*)tileView {
    
    CGPoint convertedCenter = [tileView.superview convertPoint:tileView.center toView:self.view];
    return CGRectContainsPoint(self.view.bounds, convertedCenter);
    
}

-(void) addTile:(ALGTileView*) tileView isVertical:(BOOL)vertical atCenter:(CGPoint)center {
    
    [self.view addSubview:tileView];
    tileView.center = center;
    
    if (vertical) {
        [tileView rotateToVertical:YES animated:YES];
    } else if (center.y > self.view.bounds.size.height - OUTSIDE_TILESPACE && center.y < self.view.bounds.size.height) {
        [tileView rotateToVertical:NO animated:YES];
    }
    
    BOOL willBeOnMat = [self willClaimTile:tileView];
    if (!willBeOnMat) {
        NSLog(@"Added tile was not on mat");
        [tileView removeFromSuperview];
        return;
    }
    
    if (center.x < OUTSIDE_TILESPACE && [tileView isSnapableToOutSideAreaModel]) {
        
        //On Left
        [self.leftTiles addObject:tileView];
        
    } else if (center.y > self.view.bounds.size.height - OUTSIDE_TILESPACE && [tileView isSnapableToOutSideAreaModel]) {
        
        //On Bottom
        [self.bottomTiles addObject:tileView];
        
    } else if (center.x >= ALG_UNIT_LENGTH && center.y <= self.view.bounds.size.height - OUTSIDE_TILESPACE) {
        
        //On Inside
        [self.insideTiles addObject:tileView];
        
    }
    
}

-(void) didStartPanTile:(ALGTileView*)tileView {
    
    [self.tilesBeingPanned addObject:tileView];
    CGPoint convertedCenter = [tileView.superview convertPoint:tileView.center toView:self.view];
    tileView.center = convertedCenter;
    [self.view addSubview:tileView];
    
    [self.leftTiles removeObject:tileView];
    [self.bottomTiles removeObject:tileView];
    [self.insideTiles removeObject:tileView];
    
    [self adjustOutsideLabels];
    
}

-(void) didPanTile:(ALGTileView*)tileView {

    CGPoint convertedCenter = [tileView.superview convertPoint:tileView.center toView:self.view];
    
    if (convertedCenter.x < OUTSIDE_TILESPACE && convertedCenter.x > 0.0) {
        [tileView rotateToVertical:YES animated:YES];
    } else if (convertedCenter.y > self.view.bounds.size.height - OUTSIDE_TILESPACE && convertedCenter.y < self.view.bounds.size.height) {
        [tileView rotateToVertical:NO animated:YES];
    }
    
    [self snapTilesToOutsideGrid];
    
}

-(void) didFinishTilePan:(ALGTileView*) tileView {
    
    BOOL willBeOnMat = [self willClaimTile:tileView];

    [self.tilesBeingPanned removeObject:tileView];
    
    CGPoint convertedCenter = [tileView.superview convertPoint:tileView.center toView:self.view];
    
    if (convertedCenter.x < OUTSIDE_TILESPACE && [tileView isSnapableToOutSideAreaModel] && willBeOnMat) {
        
        //On Left
        [self.leftTiles addObject:tileView];
        [self adjustOutsideLabels];
        
    } else if (convertedCenter.y > self.view.bounds.size.height - OUTSIDE_TILESPACE && [tileView isSnapableToOutSideAreaModel] && willBeOnMat) {
        
        //On Bottom
        [self.bottomTiles addObject:tileView];
        [self adjustOutsideLabels];
        
    } else if (convertedCenter.x >= ALG_UNIT_LENGTH && convertedCenter.y <= self.view.bounds.size.height - OUTSIDE_TILESPACE && willBeOnMat) {
        
        //On Inside
        [self.insideTiles addObject:tileView];
        [self snapTileToInside:tileView];
        
    }
    
    [self snapTilesToOutsideGrid];
    
}

CGFloat distanceSquaredBetweenPoints(CGPoint p1, CGPoint p2) {
    return (p1.x - p2.x)*(p1.x - p2.x) + (p1.y - p2.y)*(p1.y - p2.y);
}

-(void) snapTileToInside:(ALGTileView*)tileView {
    
    CGFloat distanceSquaredThreshold = (ALG_UNIT_LENGTH/2.0)*(ALG_UNIT_LENGTH/2.0);
    
    NSArray *leftTilesOrderedByY = [self tilesCurrentlyOnLeftOrderedByY];
    NSArray *bottomTilesOrderedByX = [self tilesCurrentlyOnBottomOrderedByX];
    
    __block CGPoint foundGridPoint = CGPointZero;
    __block BOOL found = NO;
    int outerIndex = 0;
    
    CGFloat xValueToTest = tileView.center.x - [tileView trueWidth]/2.0;
    CGFloat yValueToTest = tileView.center.y + [tileView trueHeight]/2.0;
    CGPoint pointToTestAgainst = CGPointMake(xValueToTest, yValueToTest);
    
    while (!found && outerIndex < [leftTilesOrderedByY count]) {
        
        ALGTileView *leftTile = [leftTilesOrderedByY objectAtIndex:outerIndex];
        CGFloat gridPointY = leftTile.center.y + leftTile.bounds.size.width/2.0;
        int innerIndex = 0;
        
        while (!found && innerIndex < [bottomTilesOrderedByX count]) {
            
            ALGTileView *bottomTile = [bottomTilesOrderedByX objectAtIndex:innerIndex];
            CGPoint gridPoint = CGPointMake(bottomTile.frame.origin.x, gridPointY);
            CGFloat distanceSquared = distanceSquaredBetweenPoints(gridPoint, pointToTestAgainst);
            if (distanceSquared <= distanceSquaredThreshold) {
                found = YES;
                foundGridPoint = gridPoint;
            } else {
                innerIndex++;
            }
            
        }
        
        outerIndex++;
    }
    
    if (!found) {
        
        //Next try to align to origin
        CGPoint originPoint = CGPointMake(OUTSIDE_TILESPACE, self.view.bounds.size.height - OUTSIDE_TILESPACE);
        CGFloat distanceSquared = distanceSquaredBetweenPoints(originPoint, pointToTestAgainst);
        if (distanceSquared <= distanceSquaredThreshold) {
            found = YES;
            foundGridPoint = originPoint;
        }
        
    }
    
    __block CGFloat smallestDistanceFound = CGFLOAT_MAX;
    
    if (!found) {
        
        [self.insideTiles enumerateObjectsUsingBlock:^(ALGTileView *insideTileView, BOOL *stop) {
            
            CGFloat distanceSquared;
            
            //Top Left (Stationary)
            CGPoint topLeftPoint = [insideTileView topLeftPoint];
            
            //Top Left vs bottom left
            distanceSquared = distanceSquaredBetweenPoints(topLeftPoint, [tileView bottomLeftPoint]);
            if ( distanceSquared < distanceSquaredThreshold && distanceSquared < smallestDistanceFound) {
                smallestDistanceFound = distanceSquared;
                found = YES;
                foundGridPoint = topLeftPoint;
            }
            
            //Top Left vs Bottom Right
            distanceSquared = distanceSquaredBetweenPoints(topLeftPoint, [tileView bottomRightPoint]);
            if ( distanceSquared < distanceSquaredThreshold && distanceSquared < smallestDistanceFound) {
                smallestDistanceFound = distanceSquared;
                found = YES;
                foundGridPoint = CGPointMake(topLeftPoint.x - [tileView trueWidth], topLeftPoint.y);
            }
            
            //Top Left vs Top Right
            distanceSquared = distanceSquaredBetweenPoints(topLeftPoint, [tileView topRightPoint]);
            if ( distanceSquared < distanceSquaredThreshold && distanceSquared < smallestDistanceFound) {
                smallestDistanceFound = distanceSquared;
                found = YES;
                foundGridPoint = CGPointMake(topLeftPoint.x - [tileView trueWidth], topLeftPoint.y + [tileView trueHeight]);
            }
            
            
            //Top Right (Stationary)
            CGPoint topRightPoint = [insideTileView topRightPoint];
            
            //Top Right vs bottom right
            distanceSquared = distanceSquaredBetweenPoints(topRightPoint, [tileView bottomRightPoint]);
            if ( distanceSquared < distanceSquaredThreshold && distanceSquared < smallestDistanceFound) {
                smallestDistanceFound = distanceSquared;
                found = YES;
                foundGridPoint = CGPointMake(topRightPoint.x - [tileView trueWidth], topRightPoint.y);
            }
            
            //Top Right vs bottom left
            distanceSquared = distanceSquaredBetweenPoints(topRightPoint, [tileView bottomLeftPoint]);
            if ( distanceSquared < distanceSquaredThreshold && distanceSquared < smallestDistanceFound) {
                smallestDistanceFound = distanceSquared;
                found = YES;
                foundGridPoint = CGPointMake(topRightPoint.x, topRightPoint.y);
            }
            
            //Top Right vs Top Left
            distanceSquared = distanceSquaredBetweenPoints(topRightPoint, [tileView topLeftPoint]);
            if ( distanceSquared < distanceSquaredThreshold && distanceSquared < smallestDistanceFound) {
                smallestDistanceFound = distanceSquared;
                found = YES;
                foundGridPoint = CGPointMake(topRightPoint.x, topRightPoint.y + [tileView trueHeight]);
            }
            
            //Bottom Left (Stationary)
            CGPoint bottomLeftPoint = [insideTileView bottomLeftPoint];
            
            //Bottom Left Vs Bottom Right
            distanceSquared = distanceSquaredBetweenPoints(bottomLeftPoint, [tileView bottomRightPoint]);
            if ( distanceSquared < distanceSquaredThreshold && distanceSquared < smallestDistanceFound) {
                smallestDistanceFound = distanceSquared;
                found = YES;
                foundGridPoint = CGPointMake(bottomLeftPoint.x - [tileView trueWidth], bottomLeftPoint.y);
            }
            
            //Bottom Left Vs Top Right
            distanceSquared = distanceSquaredBetweenPoints(bottomLeftPoint, [tileView topRightPoint]);
            if ( distanceSquared < distanceSquaredThreshold && distanceSquared < smallestDistanceFound) {
                smallestDistanceFound = distanceSquared;
                found = YES;
                foundGridPoint = CGPointMake(bottomLeftPoint.x - [tileView trueWidth], bottomLeftPoint.y + [tileView trueHeight]);
            }
            
            //Bottom Left Vs Top Left
            distanceSquared = distanceSquaredBetweenPoints(bottomLeftPoint, [tileView topLeftPoint]);
            if ( distanceSquared < distanceSquaredThreshold && distanceSquared < smallestDistanceFound) {
                smallestDistanceFound = distanceSquared;
                found = YES;
                foundGridPoint = CGPointMake(bottomLeftPoint.x, bottomLeftPoint.y + [tileView trueHeight]);
            }
            
            //Bottom Right (Stationary)
            CGPoint bottomRightPoint = [insideTileView bottomRightPoint];
            
            //Bottom Right Vs Top Right
            distanceSquared = distanceSquaredBetweenPoints(bottomRightPoint, [tileView topRightPoint]);
            if ( distanceSquared < distanceSquaredThreshold && distanceSquared < smallestDistanceFound) {
                smallestDistanceFound = distanceSquared;
                found = YES;
                foundGridPoint = CGPointMake(bottomRightPoint.x - [tileView trueWidth], bottomRightPoint.y + [tileView trueHeight]);
            }
            
            //Bottom Right vs Top Left
            distanceSquared = distanceSquaredBetweenPoints(bottomRightPoint, [tileView topLeftPoint]);
            if ( distanceSquared < distanceSquaredThreshold && distanceSquared < smallestDistanceFound) {
                smallestDistanceFound = distanceSquared;
                found = YES;
                foundGridPoint = CGPointMake(bottomRightPoint.x, bottomRightPoint.y + [tileView trueHeight]);
            }
            
            //Bottom Right vs Bottom Left
            distanceSquared = distanceSquaredBetweenPoints(bottomRightPoint, [tileView bottomLeftPoint]);
            if ( distanceSquared < distanceSquaredThreshold && distanceSquared < smallestDistanceFound) {
                smallestDistanceFound = distanceSquared;
                found = YES;
                foundGridPoint = CGPointMake(bottomRightPoint.x, bottomRightPoint.y);
            }
            
        }];
        
    }
    
    
    if (found) {
        
        [UIView animateWithDuration:.1
                              delay:0.0
                            options:UIViewAnimationOptionAllowAnimatedContent
                         animations:^{
                             
                             tileView.center = CGPointMake(foundGridPoint.x + [tileView trueWidth]/2.0, foundGridPoint.y - [tileView trueHeight]/2.0);
                             
                         }
                         completion:^(BOOL finished) {
                             
                         }];
        
    }
    
    
}

-(NSArray*)tilesCurrentlyOnLeftOrderedByY {
    
    NSSet *pannedTilesCurrentlyOnLeft = [self.tilesBeingPanned objectsPassingTest:^BOOL(ALGTileView *tileView, BOOL *stop) {
        
        CGPoint convertedCenter = [tileView.superview convertPoint:tileView.center toView:self.view];
        return (convertedCenter.x < OUTSIDE_TILESPACE) && (convertedCenter.x >= 0) && (convertedCenter.y <= self.view.bounds.size.height - OUTSIDE_TILESPACE) && [tileView isSnapableToOutSideAreaModel];
        
    }];
    
    NSSet *tilesCurrentlyOnLeft = [self.leftTiles setByAddingObjectsFromSet:pannedTilesCurrentlyOnLeft];
    NSArray *leftTilesOrderedByY = [[tilesCurrentlyOnLeft allObjects] sortedArrayUsingComparator:^NSComparisonResult (ALGTileView *tileView, ALGTileView *otherTileView) {
        
        CGPoint convertedCenter = [tileView.superview convertPoint:tileView.center toView:self.view];
        CGPoint otherConvertedCenter = [otherTileView.superview convertPoint:otherTileView.center toView:self.view];
        
        if (convertedCenter.y > otherConvertedCenter.y) {
            return NSOrderedAscending;
        } else {
            return NSOrderedDescending;
        }
        
    }];
    
    return leftTilesOrderedByY;
}

-(NSArray *) tilesCurrentlyOnBottomOrderedByX {
    NSSet *pannedTilesCurrentlyOnBottom = [self.tilesBeingPanned objectsPassingTest:^BOOL(ALGTileView *tileView, BOOL *stop) {
        
        CGPoint convertedCenter = [tileView.superview convertPoint:tileView.center toView:self.view];
        return (convertedCenter.y > self.view.bounds.size.height - OUTSIDE_TILESPACE) && (convertedCenter.y < self.view.bounds.size.height) && (convertedCenter.x >= OUTSIDE_TILESPACE) && [tileView isSnapableToOutSideAreaModel];
        
    }];
    
    NSSet *tilesCurrentlyOnBottom = [self.bottomTiles setByAddingObjectsFromSet:pannedTilesCurrentlyOnBottom];
    NSArray *bottomTilesOrderedByX = [[tilesCurrentlyOnBottom allObjects] sortedArrayUsingComparator:^NSComparisonResult (ALGTileView *tileView, ALGTileView *otherTileView) {
        
        CGPoint convertedCenter = [tileView.superview convertPoint:tileView.center toView:self.view];
        CGPoint otherConvertedCenter = [otherTileView.superview convertPoint:otherTileView.center toView:self.view];
        
        if (convertedCenter.x < otherConvertedCenter.x) {
            return NSOrderedAscending;
        } else {
            return NSOrderedDescending;
        }
        
    }];
    
    return bottomTilesOrderedByX;
}

-(void) snapTilesToOutsideGrid {
    
    //On Left
    //Iterate through left tiles & panning tile views
    NSArray *leftTilesOrderedByY = [self tilesCurrentlyOnLeftOrderedByY];
    NSArray *bottomTilesOrderedByX = [self tilesCurrentlyOnBottomOrderedByX];
    
    [UIView animateWithDuration:.1
                          delay:0.0
                        options:UIViewAnimationOptionAllowAnimatedContent
                     animations:^{
                         
                         //Left Tiles
                         CGFloat currentY = self.view.bounds.size.height - OUTSIDE_TILESPACE;
                         for (ALGTileView *tile in leftTilesOrderedByY) {
                             
                             if (![self.tilesBeingPanned containsObject:tile]) {
                                 CGFloat currentX = OUTSIDE_TILESPACE - [tile trueWidth]/2.0;
                                 tile.center = CGPointMake(currentX, currentY - [tile trueHeight]/2.0);
                                 
                             }
                             
                             currentY -= tile.bounds.size.width;
                         }
                         
                         //Bottom Tiles
                         CGFloat currentX = OUTSIDE_TILESPACE;
                         
                         for (ALGTileView *tile in bottomTilesOrderedByX) {
                             
                             if (![self.tilesBeingPanned containsObject:tile]) {
                                 CGFloat currentY = self.view.bounds.size.height - OUTSIDE_TILESPACE + tile.bounds.size.height/2.0;
                                 tile.center = CGPointMake(currentX + tile.bounds.size.width/2.0, currentY);
                                 
                             }
                             
                             currentX += tile.bounds.size.width;
                         }
                         
                         [self adjustOutsideLabels];
                         
                     }
                     completion:^(BOOL finished) {
                         
                     }];
    
    
    
}

-(void) adjustOutsideLabels {
    
    NSArray *leftTilesOrderedByY = [self tilesCurrentlyOnLeftOrderedByY];
    NSArray *bottomTilesOrderedByX = [self tilesCurrentlyOnBottomOrderedByX];
    
    NSMutableSet *labelsToRemove = [NSMutableSet set];
    for (ALGTileLengthLabel *label in self.lengthLabels) {
        
        if (![self.leftTiles containsObject:label.attachedTileView] && ![self.bottomTiles containsObject:label.attachedTileView]) {
            [labelsToRemove addObject:label];
        }
        
    }
    
    for (ALGTileLengthLabel *labelToRemove in labelsToRemove) {
        [labelToRemove removeFromSuperview];
        [self.lengthLabels removeObject:labelsToRemove];
    }
    
    for (ALGTileView *tileView in leftTilesOrderedByY) {
        
        if (![self.tilesBeingPanned containsObject:tileView]) {
            
            ALGTileLengthLabel *label = [self dequeueLengthLabelForTile:tileView];
            label.center = CGPointMake(label.bounds.size.width/2.0, tileView.center.y);
            label.titleLabel.text = [tileView tileLengthTitleForSide:CGRectMinXEdge];
        }
        
    }
    
    for (ALGTileView *tileView in bottomTilesOrderedByX) {
        
        if (![self.tilesBeingPanned containsObject:tileView]) {
            
            ALGTileLengthLabel *label = [self dequeueLengthLabelForTile:tileView];
            label.center = CGPointMake(tileView.center.x, self.view.bounds.size.height - label.bounds.size.height/2.0);
            label.titleLabel.text = [tileView tileLengthTitleForSide:CGRectMinYEdge];
        }
        
        
    }
    
}

-(ALGTileLengthLabel*)dequeueLengthLabelForTile:(ALGTileView*) tileView {
    
    __block ALGTileLengthLabel *labelToReturn = nil;
    [self.lengthLabels enumerateObjectsUsingBlock:^(ALGTileLengthLabel *label, BOOL *stop) {
        
        if (label.attachedTileView == tileView) {
            labelToReturn = label;
            *stop = YES;
        }
        
    }];
    
    if (!labelToReturn) {
        
        labelToReturn = [[ALGTileLengthLabel alloc] initWithFrame:CGRectMake(0.0, 0.0, OUTSIDE_TILESPACE - ALG_UNIT_LENGTH, OUTSIDE_TILESPACE - ALG_UNIT_LENGTH)];
        labelToReturn.backgroundColor = [UIColor clearColor];
        labelToReturn.alpha = 1.0;
        labelToReturn.attachedTileView = tileView;
    }
    
    [self.lengthLabels addObject:labelToReturn];
    [self.view addSubview:labelToReturn];
    
    return labelToReturn;
}

-(NSArray *)savedExpressionTermsAlongBottom {
    
    NSMutableArray *expressionTerms = [NSMutableArray array];
    for (ALGTileView *tileView in [self bottomTiles]) {
        ALGSavedExpressionTerm *term = [[ALGSavedExpressionTerm alloc] init];
        term.tileType = tileView.tileModel.tileType;
        term.negative = tileView.tileModel.negative;
        term.coefficient = 1;
        term.center = tileView.center;
        term.vertical = tileView.vertical;

        [expressionTerms addObject:term];
        
    }
    return expressionTerms;
}

-(NSArray *)savedExpressionTermsAlongLeft  {
    NSMutableArray *expressionTerms = [NSMutableArray array];
    for (ALGTileView *tileView in [self leftTiles]) {
        ALGSavedExpressionTerm *term = [[ALGSavedExpressionTerm alloc] init];
        term.tileType = tileView.tileModel.tileType;
        term.negative = tileView.tileModel.negative;
        term.coefficient = 1;
        term.center = tileView.center;
        term.vertical = tileView.vertical;

        [expressionTerms addObject:term];
        
    }
    return expressionTerms;
}

-(NSArray *)savedExpressionTermsInCenter {
    NSMutableArray *expressionTerms = [NSMutableArray array];
    for (ALGTileView *tileView in [self insideTiles]) {
        ALGSavedExpressionTerm *term = [[ALGSavedExpressionTerm alloc] init];
        term.tileType = tileView.tileModel.tileType;
        term.negative = tileView.tileModel.negative;
        term.coefficient = 1;
        term.center = tileView.center;
        term.vertical = tileView.vertical;

        [expressionTerms addObject:term];
        
    }
    return expressionTerms;
}

@end
