//
//  main.m
//  AlgebraTiles
//
//  Created by Reese McLean on 4/19/13.
//  Copyright (c) 2013 Reese McLean. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ALGAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([ALGAppDelegate class]));
    }
}
