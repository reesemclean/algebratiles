//
//  ALGInstructionController.m
//  AlgebraTiles
//
//  Created by Reese McLean on 5/9/13.
//  Copyright (c) 2013 Reese McLean. All rights reserved.
//

#import "ALGInstructionController.h"

#import "ALGTileIntroductionActivityViewController.h"
#import "ALGInstructionStepModel.h"
#import "ALGAreaMatViewController.h"
#import "ALGExpressionMatViewController.h"

#import "ALGExpressionModel.h"
#import "ALGExpressionTermModel.h"
#import "ALGTileView.h"

@interface ALGInstructionController ()

@property (nonatomic, strong) ALGInstructionStepModel *currentInstructionStep;
@property (nonatomic, strong) NSArray *instructionSteps;

@end

@implementation ALGInstructionController

-(id) initWithActivityController:(ALGTileIntroductionActivityViewController*)activityVC {
    self = [super init];
    if (self) {
        _activityViewController = activityVC;
        [self setupInstructionSteps];
        self.currentInstructionStep = [self.instructionSteps objectAtIndex:0];
    }
    return self;
}

-(NSUInteger) numberOfSteps {
    return [self.instructionSteps count];
}

-(NSUInteger) currentStepNumber {
    return [self.instructionSteps indexOfObject:self.currentInstructionStep];
}

-(ALGInstructionStepModel*)currentStep {
    return self.currentInstructionStep;
}

-(ALGInstructionStepModel*)nextStep {

    NSUInteger indexOfCurrentStep = [self.instructionSteps indexOfObject:self.currentInstructionStep];
    indexOfCurrentStep++;
    if(indexOfCurrentStep < [self.instructionSteps count]) {
		self.currentInstructionStep = [self.instructionSteps objectAtIndex:indexOfCurrentStep];
        return self.currentInstructionStep;
    } else {
        return nil;
	}

}

-(ALGInstructionStepModel*)previousStep {
    
    NSInteger indexOfCurrentStep = [self.instructionSteps indexOfObject:self.currentInstructionStep];
    indexOfCurrentStep--;
    if (indexOfCurrentStep < 0) {
        return nil;
    } else {
        self.currentInstructionStep = [self.instructionSteps objectAtIndex:indexOfCurrentStep];
        return self.currentInstructionStep;
    }
    
}

-(void) setupInstructionSteps {
    
    //Drag Unit Tile On
    __weak ALGInstructionStepModel *dragUnitStep = [ALGInstructionStepModel step];
    dragUnitStep.stepTitle = @"Adding Tiles to the Mat";
    dragUnitStep.instructionText = @"Drag a unit tile onto the mat.";
    dragUnitStep.feedbackText = @"Good Job! That's how you can add tiles to any of the mats.";
    dragUnitStep.setupBlock = ^{
      
        self.activityViewController.instructionLabel.text = dragUnitStep.instructionText;
        [self.activityViewController showSingleExpressionMatWithTopAndBottom:NO
                                                                    withSize:CGSizeMake(400, 400)
                                                                    animated:YES
                                                              withCompletion:nil];
        
    };
    dragUnitStep.undoStepBlock = ^{
        
    };
    dragUnitStep.isStepCompleteBlock = ^BOOL {
        
        ALGExpressionModel *expressonModel = [self.activityViewController.leftExpressionMat expressionModel];
        if ([[expressonModel negativeTerms] count] > 0) {
            return NO;
        }
        
        if ([[expressonModel positiveTerms] count] != 1) {
            return NO;
        }
        
        ALGExpressionTermModel *termModel = [[expressonModel positiveTerms] objectAtIndex:0];
        return termModel.tileType == ALGTileTypeUnit && !termModel.negative;
        
    };
    dragUnitStep.didCompleteStepBlock = ^{
        
        //Disable user Interaction
        self.activityViewController.instructionLabel.text = dragUnitStep.feedbackText;
        
        //Center Unit Tile?
        
    };
    
    //Trash Unit Tile
    __weak ALGInstructionStepModel *trashUnitStep = [ALGInstructionStepModel step];
    trashUnitStep.stepTitle = @"Removing Tiles from the Mat";
    trashUnitStep.instructionText = @"Now let's get rid of the tile you just added. Drag it off the mat.";
    trashUnitStep.feedbackText = @"Nice! That's how you remove tiles from any of the mats.";
    trashUnitStep.setupBlock = ^{
        
        self.activityViewController.instructionLabel.text = trashUnitStep.instructionText;
        
    };
    trashUnitStep.undoStepBlock = ^{
        self.activityViewController.instructionLabel.text = dragUnitStep.instructionText;
        for (ALGTileGroupModel *group in self.activityViewController.leftExpressionMat.tileGroups) {
            for (ALGTileView *view in group.tiles) {
                [view removeTileAnimated:YES];
            }
        }
    };
    trashUnitStep.isStepCompleteBlock = ^BOOL{
        ALGExpressionModel *expressonModel = [self.activityViewController.leftExpressionMat expressionModel];
        return [[expressonModel positiveTerms] count] == 0 && [[expressonModel negativeTerms] count] == 0;
    };
    trashUnitStep.didCompleteStepBlock = ^{
        //Show Annotation "Nice Job! That's how you remove tiles."
        self.activityViewController.instructionLabel.text = trashUnitStep.feedbackText;
    };
    
    //Flip Tile to negative
    __weak ALGInstructionStepModel *flipTilesSetupStep = [ALGInstructionStepModel step];
    flipTilesSetupStep.stepTitle = @"Flipping Tiles to Negative";
    flipTilesSetupStep.instructionText = @"Sometimes we want to represent opposite values. Drag an x-tile onto the mat. Then double tap it.";
    flipTilesSetupStep.feedbackText = @"Great! When you double tap a tile it flips to its opposite value. So your positive x-tile is now negative x-tile.";
    flipTilesSetupStep.setupBlock = ^{
        self.activityViewController.instructionLabel.text = flipTilesSetupStep.instructionText;
    };
    flipTilesSetupStep.undoStepBlock = ^{
        self.activityViewController.instructionLabel.text = trashUnitStep.instructionText;
        for (ALGTileGroupModel *group in self.activityViewController.leftExpressionMat.tileGroups) {
            ALGTileView *tileView = [group topTile];
            if (tileView.tileModel.negative) {
                [tileView flipTileAnimated:YES];
            }
        }
    };
    flipTilesSetupStep.isStepCompleteBlock = ^BOOL {
        ALGExpressionModel *expressonModel = [self.activityViewController.leftExpressionMat expressionModel];
        if ([[expressonModel negativeTerms] count] > 0) {
            return NO;
        }
        
        if ([[expressonModel positiveTerms] count] < 1 || [[expressonModel positiveTerms] count] > 1) {
            return NO;
        }
        
        ALGExpressionTermModel *termModel = [[expressonModel positiveTerms] objectAtIndex:0];
        return termModel.tileType == ALGTileTypeX && termModel.negative;
    };
    flipTilesSetupStep.didCompleteStepBlock = ^{
        self.activityViewController.instructionLabel.text = flipTilesSetupStep.feedbackText;
    };
    
    //Flip it back to positive
    __weak ALGInstructionStepModel *flipItBackStep = [ALGInstructionStepModel step];
    flipItBackStep.stepTitle = @"Flipping Tiles to Positive";
    flipItBackStep.instructionText = @"When tiles are negative you can make them positive again by double tapping. Try it by flipping the tile back to its positive side.";
    flipItBackStep.feedbackText = @"Well Done! Now it is back to representing positive x.";
    flipItBackStep.setupBlock = ^{
        self.activityViewController.instructionLabel.text = flipItBackStep.instructionText;
    };
    flipItBackStep.undoStepBlock = ^{
        self.activityViewController.instructionLabel.text = flipTilesSetupStep.instructionText;
        for (ALGTileGroupModel *group in self.activityViewController.leftExpressionMat.tileGroups) {
            ALGTileView *tileView = [group topTile];
            if (!tileView.tileModel.negative) {
                [tileView flipTileAnimated:YES];
            }
        }
        
    };
    flipItBackStep.isStepCompleteBlock = ^BOOL {
      
        ALGExpressionModel *expressonModel = [self.activityViewController.leftExpressionMat expressionModel];
        if ([[expressonModel negativeTerms] count] > 0) {
            return NO;
        }
        
        if ([[expressonModel positiveTerms] count] < 1 || [[expressonModel positiveTerms] count] > 1) {
            return NO;
        }
        
        ALGExpressionTermModel *termModel = [[expressonModel positiveTerms] objectAtIndex:0];
        return termModel.tileType == ALGTileTypeX && !termModel.negative;
        
    };
    flipItBackStep.didCompleteStepBlock = ^{
        self.activityViewController.instructionLabel.text = flipItBackStep.feedbackText;
    };
    
    //Add another positive x-tile and have student group them
    __weak ALGInstructionStepModel *groupThemStep = [ALGInstructionStepModel step];
    groupThemStep.stepTitle = @"Grouping Tiles";
    groupThemStep.instructionText = @"I've added another x-tile to the mat. Let's group them together! Drag one of them on top of the other.";
    groupThemStep.feedbackText = @"Now they can be moved and flipped together. To ungroup them drag the top tile away.";
    groupThemStep.setupBlock = ^{
        //Add another x-tile on
        self.activityViewController.instructionLabel.text = groupThemStep.instructionText;

    };
    groupThemStep.undoStepBlock = ^{
        //Remove extra x-tile, make sure the remaining one is on the positive side.
        self.activityViewController.instructionLabel.text = flipItBackStep.instructionText;
    };
    groupThemStep.isStepCompleteBlock = ^BOOL{
        ALGExpressionModel *expressonModel = [self.activityViewController.leftExpressionMat expressionModel];
        if ([[expressonModel negativeTerms] count] > 0) {
            return NO;
        }
        
        if ([[expressonModel positiveTerms] count] < 1 || [[expressonModel positiveTerms] count] > 1) {
            return NO;
        }
        
        ALGExpressionTermModel *termModel = [[expressonModel positiveTerms] objectAtIndex:0];
        return termModel.tileType == ALGTileTypeX && !termModel.negative && termModel.coefficient == 2;
    };
    groupThemStep.didCompleteStepBlock = ^{
        self.activityViewController.instructionLabel.text = groupThemStep.feedbackText;
    };
    
    self.instructionSteps = @[ dragUnitStep, trashUnitStep, flipTilesSetupStep, flipItBackStep, groupThemStep ];
    
}


@end
