//
//  ALGAreaMatView.m
//  AlgebraTiles
//
//  Created by Reese McLean on 4/21/13.
//  Copyright (c) 2013 Reese McLean. All rights reserved.
//

#import "ALGAreaMatView.h"

@implementation ALGAreaMatView

-(CGFloat) outsideTilespace {
    if (_outsideTilespace < 1) {
        return 80.0;
    }
    
    return _outsideTilespace;
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetStrokeColorWithColor(context, [[UIColor lightGrayColor] CGColor]);
    CGContextSetLineWidth(context, 1.0);
    
    CGRect insetRect = CGRectInset(self.bounds, .5, .5);
    CGContextStrokeRect(context, insetRect);
    
    CGContextSetStrokeColorWithColor(context, [[UIColor darkGrayColor] CGColor]);
    
    CGMutablePathRef verticalPath = CGPathCreateMutable();
    CGPathMoveToPoint(verticalPath, NULL, [self outsideTilespace] + .5, .5);
    CGPathAddLineToPoint(verticalPath, NULL, [self outsideTilespace] + .5, self.bounds.size.height - .5);
    CGContextAddPath(context, verticalPath);
    CGContextStrokePath(context);
    CGPathRelease(verticalPath);
    
    CGMutablePathRef horizontalPath = CGPathCreateMutable();
    CGPathMoveToPoint(horizontalPath, NULL, .5, self.bounds.size.height - [self outsideTilespace] - .5);
    CGPathAddLineToPoint(horizontalPath, NULL, self.bounds.size.width - .5, self.bounds.size.height - [self outsideTilespace] - .5);
    CGContextAddPath(context, horizontalPath);
    CGContextStrokePath(context);
    CGPathRelease(horizontalPath);
    
    CGContextSetFillColorWithColor(context, [[UIColor whiteColor] CGColor]);
    
    //CGRect insideRect = CGRectMake(OUTSIDE_TILESPACE + 1.0, 1.0, self.bounds.size.width - OUTSIDE_TILESPACE - 2.0, self.bounds.size.height - 2.0 - OUTSIDE_TILESPACE);
    //CGContextFillRect(context, insideRect);
    
}

@end
