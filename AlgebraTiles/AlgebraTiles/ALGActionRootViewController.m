//
//  ALGActionRootViewController.m
//  AlgebraTiles
//
//  Created by Reese McLean on 3/28/14.
//  Copyright (c) 2014 Reese McLean. All rights reserved.
//

#import "ALGActionRootViewController.h"

@interface ALGActionRootViewController ()

@end

@implementation ALGActionRootViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.title = @"Actions";
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    switch (indexPath.row) {
        case 0:
            [self.delegate didSelectSaveProblemInActionVC:self];
            break;
        case 1:
            [self.delegate didSelectShareImageInActionVC:self];
            break;
        default:
            break;
            
    }
    

}

@end
