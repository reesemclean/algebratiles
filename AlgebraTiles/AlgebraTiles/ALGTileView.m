//
//  ALGTileView.m
//  AlgebraTiles
//
//  Created by Reese McLean on 4/20/13.
//  Copyright (c) 2013 Reese McLean. All rights reserved.
//

#import "ALGTileView.h"

#import "ALGTileModel.h"
#import <QuartzCore/QuartzCore.h>
#import "ALGTileTheme.h"
#import "ALGTileThemeManager.h"

@interface ALGTileView ()

@property (readwrite, nonatomic, copy) ALGTileModel *tileModel;

@end

@implementation ALGTileView

+(instancetype) tileWithTileModel:(ALGTileModel*)tileModel andTheme:(ALGTileTheme*)tileTheme {
    
    CGSize size = [ALGTileView sizeForTileModel:tileModel];
    CGRect frame = CGRectMake(0, 0, size.width, size.height);
    ALGTileView *tileView = [[ALGTileView alloc] initWithFrame:frame];
    
    tileView.layer.shadowOffset = CGSizeMake(0.0, 0.0);
    tileView.layer.shadowColor = [[UIColor darkGrayColor] CGColor];
    tileView.layer.shadowRadius = 2.0;
    
    tileView.canFlipToNegative = NO;
    tileView.vertical = NO;
    
    tileView.tileModel = tileModel;
    tileView.tileTheme = tileTheme;
    tileView.respondsToThemeChange = YES;
    
    UIPanGestureRecognizer *panGestureRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:tileView
                                                                                           action:@selector(handlePanGesture:)];
    [tileView addGestureRecognizer:panGestureRecognizer];
    
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:tileView
                                                                                           action:@selector(handleDoubleTap:)];
    tapGestureRecognizer.numberOfTapsRequired = 2;
    [tileView addGestureRecognizer:tapGestureRecognizer];
    
    [[NSNotificationCenter defaultCenter] addObserver:tileView
                                             selector:@selector(tileThemeDidChange:)
                                                 name:ALGThemeDidChangeNotificationKey
                                               object:nil];
    
    return tileView;
}

-(void)tileThemeDidChange:(NSNotification*)notification {
    
    if (!self.respondsToThemeChange) {
        return;
    }
    ALGTileTheme *theme = [notification object];
    [self setTileTheme:theme];
}

-(void) setTileTheme:(ALGTileTheme *)tileTheme {
    _tileTheme = tileTheme;
    [self setNeedsDisplay];
}

-(void) dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:ALGThemeDidChangeNotificationKey object:nil];
}

-(BOOL) isSameTypeOfTile:(ALGTileView*)tileToTest {
    return [tileToTest.tileModel tileType] == [self.tileModel tileType] && [tileToTest.tileModel negative] == [self.tileModel negative];
}

#pragma mark - Drawing

- (void)drawRect:(CGRect)rect {
    // get the contect
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [[ALGTileView colorForTileModel:self.tileModel withTheme:self.tileTheme] CGColor]);
    CGContextFillRect(context, self.bounds);
    
    CGContextSetStrokeColorWithColor(context, [[UIColor darkGrayColor] CGColor]);
    CGContextSetLineWidth(context, 1.0);
    
    CGRect insetRect = CGRectInset(self.bounds, .5, .5);
    CGContextStrokeRect(context, insetRect);
    
}

#pragma mark - Touch Handler

- (void)handlePanGesture:(UIPanGestureRecognizer *)thePanGestureRecognizer {
    switch (thePanGestureRecognizer.state) {
        case UIGestureRecognizerStateBegan: {
            
            [self.touchDelegate didStartTileViewPan:self];
            
        }
            break;
        case UIGestureRecognizerStateChanged: {
            
            CGPoint theTranslation = [thePanGestureRecognizer translationInView:self.superview];
            
            [thePanGestureRecognizer setTranslation:CGPointZero inView:self.superview];
            [self.touchDelegate didPanTileView:self withTranslation:theTranslation];
            
        }
            break;
            
        case UIGestureRecognizerStateEnded: {
            
            [self.touchDelegate didFinishTileViewPan:self];
            
        }
            break;
        default:
            break;
    }
}

-(void) handleDoubleTap:(UITapGestureRecognizer*)tapGesture {
    
    if (self.canFlipToNegative) {
        
        [self flipTileAnimated:YES];
        
        if ([self.touchDelegate respondsToSelector:@selector(didFlipTile:)]) {
            [self.touchDelegate didFlipTile:self];
        }
    }
    
}

-(void) translateTileCenter:(CGPoint) translation {
    self.center = CGPointMake(self.center.x + translation.x, self.center.y + translation.y);
}

#pragma mark - Rotation

-(void) rotateToVertical:(BOOL)vertical animated:(BOOL)animated {
    
    if (self.vertical == vertical) {
        return; //Aleady correct orientation
    }
    
    if (vertical) {
        
        self.vertical = vertical;
        //Rotate to vertical
        [UIView animateWithDuration:animated ? .1 : 0.0
                              delay:0.0
                            options:UIViewAnimationOptionAllowUserInteraction
                         animations:^{
                             self.transform = CGAffineTransformMakeRotation(M_PI/2.0);
                         }
                         completion:^(BOOL finished) {
                             
                         }];
        
        
    } else {
        
        self.vertical = vertical;
        //Rotate to Horizontal
        [UIView animateWithDuration:animated ? .1 : 0.0
                              delay:0.0
                            options:UIViewAnimationOptionAllowUserInteraction
                         animations:^{
                             self.transform = CGAffineTransformIdentity;
                         }
                         completion:^(BOOL finished) {
                             
                         }];
    }
    
}

-(BOOL) isSnapableToOutSideAreaModel {
    switch (self.tileModel.tileType) {
        case ALGTileTypeUnit:
            return YES;
            break;
        case ALGTileTypeX:
            return YES;
            break;
        case ALGTileTypeXSquared:
            return NO;
            break;
        case ALGTileTypeXY:
            return NO;
            break;
        case ALGTileTypeY:
            return YES;
            break;
        case ALGTileTypeYSquared:
            return NO;
            break;
        default:
            return NO;
            break;
    }
}

-(NSString*)tileLengthTitleForSide:(CGRectEdge) rectEdge {
    
    switch (self.tileModel.tileType) {
        case ALGTileTypeUnit:
            return @"1";
            break;
        case ALGTileTypeX:
            
            if (rectEdge == CGRectMinXEdge || rectEdge == CGRectMaxXEdge) {
                if (self.vertical) {
                    return @"x";
                } else {
                    return @"1";
                }
            } else {
                if (self.vertical) {
                    return @"1";
                } else {
                    return @"x";
                }
            }
            
            break;
        case ALGTileTypeXSquared:
            return @"x";
            break;
        case ALGTileTypeXY:
            if (rectEdge == CGRectMinXEdge || rectEdge == CGRectMaxXEdge) {
                if (self.vertical) {
                    return @"x";
                } else {
                    return @"y";
                }
            } else {
                if (self.vertical) {
                    return @"y";
                } else {
                    return @"x";
                }
            }
            break;
        case ALGTileTypeY:
            if (rectEdge == CGRectMinXEdge || rectEdge == CGRectMaxXEdge) {
                if (self.vertical) {
                    return @"y";
                } else {
                    return @"1";
                }
            } else {
                if (self.vertical) {
                    return @"1";
                } else {
                    return @"y";
                }
            }
            break;
        case ALGTileTypeYSquared:
            return @"y";
            break;
        default:
            return nil;
            break;
    }
    
}

#pragma mark - Display Helpers

-(void) flipTileAnimated:(BOOL)animated {
    
    self.tileModel.negative = !self.tileModel.negative;
    
    [UIView transitionWithView:self
                      duration:animated ? .2 : 0.0
                       options:UIViewAnimationOptionAllowAnimatedContent |
                                UIViewAnimationCurveEaseIn |
                                UIViewAnimationOptionAllowAnimatedContent |
                                UIViewAnimationOptionTransitionFlipFromBottom
                    animations:^{
                        [self setNeedsDisplay];
                    }
                    completion:^(BOOL finished) {
                        
                    }];
    
}

-(void) addShadowAnimated:(BOOL)animated {
    
    CGPathRef shadowPath = CGPathCreateWithRect(self.bounds, NULL);
    
    self.layer.shadowPath = shadowPath;
    
    CGPathRelease(shadowPath);
    
    [UIView animateWithDuration:.1
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         self.layer.shadowOpacity = .66;
                     }
                     completion:^(BOOL finished) {
                     }];

    
    

}

-(void) removeShadowAnimated:(BOOL)animated {
    
    [UIView animateWithDuration:.1
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         self.layer.shadowOpacity = 0.0;
                     }
                     completion:^(BOOL finished) {
                     }];
}

-(void) removeTileAnimated:(BOOL)animated {
    
    [UIView animateWithDuration:.25
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         self.alpha = 0.0;
                         self.transform = CGAffineTransformMakeScale(.25, .25);
                     }
                     completion:^(BOOL finished) {
                         [self removeFromSuperview];
                     }];

    
}

+(UIColor*)colorForTileModel:(ALGTileModel*)tileModel withTheme:(ALGTileTheme*)tileTheme {
    
    UIColor *color;
    
    
    if (tileModel.negative) {
        return tileTheme.negativeColor;
    }
    
    switch (tileModel.tileType) {
        case ALGTileTypeUnit:
            color = tileTheme.unitColor;
            break;
        case ALGTileTypeX:
            color = tileTheme.xColor;
            break;
        case ALGTileTypeXSquared:
            color = tileTheme.xColor;
            break;
        case ALGTileTypeXY:
            color = tileTheme.xyColor;
            break;
        case ALGTileTypeY:
            color = tileTheme.yColor;
            break;
        case ALGTileTypeYSquared:
            color = tileTheme.yColor;
            break;
        default:
            color = tileTheme.unitColor;
            break;
    }
    
    return color;
    
}

+(CGSize) sizeForTileModel:(ALGTileModel*) tileModel {
    
    CGSize size;
    
    switch (tileModel.tileType) {
        case ALGTileTypeUnit:
            size = CGSizeMake(ALG_UNIT_LENGTH, ALG_UNIT_LENGTH);
            break;
        case ALGTileTypeX:
            size = CGSizeMake(ALG_X_LENGTH, ALG_UNIT_LENGTH);
            break;
        case ALGTileTypeXSquared:
            size = CGSizeMake(ALG_X_LENGTH, ALG_X_LENGTH);
            break;
        case ALGTileTypeXY:
            size = CGSizeMake(ALG_Y_LENGTH, ALG_X_LENGTH);
            break;
        case ALGTileTypeY:
            size = CGSizeMake(ALG_Y_LENGTH, ALG_UNIT_LENGTH);
            break;
        case ALGTileTypeYSquared:
            size = CGSizeMake(ALG_Y_LENGTH, ALG_Y_LENGTH);
            break;
        default:
            size = CGSizeMake(ALG_UNIT_LENGTH, ALG_UNIT_LENGTH);
            break;
    }
    
    return size;
}

#pragma mark Geometry Helpers

-(CGPoint) topLeftPoint {
    
    CGFloat x;
    if (self.vertical) {
        x = self.center.x - self.bounds.size.height/2.0;
    } else {
        x = self.center.x - self.bounds.size.width/2.0;
    }
    
    CGFloat y;
    if (self.vertical) {
        y = self.center.y - self.bounds.size.width/2.0;
    } else {
        y = self.center.y - self.bounds.size.height/2.0;
    }
    return CGPointMake(x, y);
}

-(CGPoint) topRightPoint {
    
    CGFloat x;
    if (self.vertical) {
        x = self.center.x + self.bounds.size.height/2.0;
    } else {
        x = self.center.x + self.bounds.size.width/2.0;
    }
    
    CGFloat y;
    if (self.vertical) {
        y = self.center.y - self.bounds.size.width/2.0;
    } else {
        y = self.center.y - self.bounds.size.height/2.0;
    }
    return CGPointMake(x, y);
    
}

-(CGPoint) bottomLeftPoint {
    CGFloat x;
    if (self.vertical) {
        x = self.center.x - self.bounds.size.height/2.0;
    } else {
        x = self.center.x - self.bounds.size.width/2.0;
    }
    
    CGFloat y;
    if (self.vertical) {
        y = self.center.y + self.bounds.size.width/2.0;
    } else {
        y = self.center.y + self.bounds.size.height/2.0;
    }
    return CGPointMake(x, y);
}

-(CGPoint) bottomRightPoint {
    CGFloat x;
    if (self.vertical) {
        x = self.center.x + self.bounds.size.height/2.0;
    } else {
        x = self.center.x + self.bounds.size.width/2.0;
    }
    
    CGFloat y;
    if (self.vertical) {
        y = self.center.y + self.bounds.size.width/2.0;
    } else {
        y = self.center.y + self.bounds.size.height/2.0;
    }
    return CGPointMake(x, y);
}

-(CGFloat) trueWidth {
    if (self.vertical) {
        return self.bounds.size.height;
    } else {
        return self.bounds.size.width;
    }
}

-(CGFloat) trueHeight {
    if (self.vertical) {
        return self.bounds.size.width;
    } else {
        return self.bounds.size.height;
    }
}


@end
