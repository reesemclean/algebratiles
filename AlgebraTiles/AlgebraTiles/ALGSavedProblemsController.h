//
//  ALGSavedProblemsController.h
//  AlgebraTiles
//
//  Created by Reese McLean on 3/27/14.
//  Copyright (c) 2014 Reese McLean. All rights reserved.
//

#import <Foundation/Foundation.h>

@class ALGSavedProblem;

@interface ALGSavedProblemsController : NSObject

+(instancetype) sharedController;
@property (nonatomic, strong) NSMutableArray *savedProblems;

-(void) addProblem:(ALGSavedProblem*)problem;
-(void) removeProblem:(ALGSavedProblem*)problem;
-(void) updateLastUsedDateForProblem:(ALGSavedProblem*)problem;

-(NSString *)previewImageSaveDirectory;

@end
