//
//  ALGTileTheme.h
//  AlgebraTiles
//
//  Created by Reese McLean on 3/27/14.
//  Copyright (c) 2014 Reese McLean. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ALGTileTheme : NSObject

+(instancetype) tileThemeFromDictionary:(NSDictionary*) tileDictionary;

@property (nonatomic, readonly) NSInteger uniqueID;
@property (nonatomic, readonly) NSString *name;
@property (nonatomic, readonly) UIColor *unitColor;
@property (nonatomic, readonly) UIColor *xColor;
@property (nonatomic, readonly) UIColor *yColor;
@property (nonatomic, readonly) UIColor *xyColor;
@property (nonatomic, readonly) UIColor *negativeColor;

@end
