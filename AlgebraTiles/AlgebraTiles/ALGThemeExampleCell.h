//
//  ALGThemeExampleCell.h
//  AlgebraTiles
//
//  Created by Reese McLean on 3/27/14.
//  Copyright (c) 2014 Reese McLean. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ALGTileTheme;

@interface ALGThemeExampleCell : UITableViewCell

-(void) setTheme:(ALGTileTheme *)tileTheme;

@end
