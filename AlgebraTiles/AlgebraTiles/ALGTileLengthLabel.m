//
//  ATTileLengthLabel.m
//  AlgebraTiles
//
//  Created by Reese McLean on 12/17/12.
//  Copyright (c) 2012 Reese McLean. All rights reserved.
//

#import "ALGTileLengthLabel.h"

@interface ALGTileLengthLabel ()

@end

@implementation ALGTileLengthLabel

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        _titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
        _titleLabel.backgroundColor = [UIColor clearColor];
        _titleLabel.font = [UIFont fontWithName:@"AvenirNext-Regular" size:17.0];
        _titleLabel.textAlignment = NSTextAlignmentCenter;
        _titleLabel.textColor = [UIColor darkGrayColor];
        [self addSubview:_titleLabel];
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
