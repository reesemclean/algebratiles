//
//  ALGEquationMatActivityViewController.h
//  AlgebraTiles
//
//  Created by Reese McLean on 4/21/13.
//  Copyright (c) 2013 Reese McLean. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ALGComparisionMatActivityViewController.h"

@class ALGSavedProblem;

@interface ALGEquationMatActivityViewController : ALGComparisionMatActivityViewController

+(instancetype)viewControllerForEquationMatFromStoryboard:(UIStoryboard*)storyboard withProblem:(ALGSavedProblem*)savedProblem;

@end
