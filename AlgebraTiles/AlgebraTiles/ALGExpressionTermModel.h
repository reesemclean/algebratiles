//
//  ATExpressionTermModel.h
//  AlgebraTiles
//
//  Created by Reese McLean on 12/20/12.
//  Copyright (c) 2012 Reese McLean. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "ALGTileConstants.h"

#import "ALGTileModel.h"

@interface ALGExpressionTermModel : NSObject

-(ALGTileType) tileType;
-(BOOL) negative;
-(NSUInteger) coefficient;

@property (nonatomic, copy) NSSet *tileModels;

@end
