//
//  ALGInstructionStepModel.h
//  AlgebraTiles
//
//  Created by Reese McLean on 5/8/13.
//  Copyright (c) 2013 Reese McLean. All rights reserved.
//

typedef void (^ALGSimpleBlock)();
typedef BOOL (^ALGBOOLBlock)();

#import <Foundation/Foundation.h>

@interface ALGInstructionStepModel : NSObject

+(id) step;

@property (nonatomic, copy) NSString *stepTitle;
@property (nonatomic, copy) NSString *feedbackText;
@property (nonatomic, copy) NSString *instructionText;

@property (nonatomic, copy) ALGSimpleBlock setupBlock;
@property (nonatomic, copy) ALGSimpleBlock undoStepBlock;
@property (nonatomic, copy) ALGBOOLBlock isStepCompleteBlock;
@property (nonatomic, copy) ALGSimpleBlock didCompleteStepBlock;

@end
