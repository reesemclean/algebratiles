//
//  ALGAreaMatView.h
//  AlgebraTiles
//
//  Created by Reese McLean on 4/21/13.
//  Copyright (c) 2013 Reese McLean. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ALGAreaMatView : UIView

@property (nonatomic, assign) CGFloat outsideTilespace;

@end
