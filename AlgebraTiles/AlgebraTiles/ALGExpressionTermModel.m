//
//  ATExpressionTermModel.m
//  AlgebraTiles
//
//  Created by Reese McLean on 12/20/12.
//  Copyright (c) 2012 Reese McLean. All rights reserved.
//

#import "ALGExpressionTermModel.h"

@implementation ALGExpressionTermModel

-(id) init {
    self = [super init];
    if (self) {
        self.tileModels = [NSSet set];
    }
    return self;
}

-(ALGTileType) tileType {
    return [[self.tileModels anyObject] tileType];
}

-(BOOL) negative {
    return [[self.tileModels anyObject] negative];
}

-(NSUInteger) coefficient {
    return [self.tileModels count];
}

@end
