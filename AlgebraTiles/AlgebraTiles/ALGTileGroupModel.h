//
//  ALGTileGroupModel.h
//  AlgebraTiles
//
//  Created by Reese McLean on 4/20/13.
//  Copyright (c) 2013 Reese McLean. All rights reserved.
//

#import <Foundation/Foundation.h>

@class ALGExpressionTermModel;

@class ALGTileView;

@interface ALGTileGroupModel : NSObject

+(id) groupWithTiles:(NSArray*)tiles;

@property (nonatomic, strong) NSMutableArray *tiles;

-(ALGTileView*) topTile;
-(ALGTileView*) bottomTile;

-(void) addTile:(ALGTileView*)tile;
-(void) removeTile:(ALGTileView*)tile;
-(BOOL) containsTile:(ALGTileView*)tile;

-(CGPoint) centerOfGroup;
-(CGRect) rectForGroup;
-(UIView*) superview;

-(BOOL)isSameTypeOfGroup:(ALGTileGroupModel*)anotherGroup;

-(ALGExpressionTermModel *)termModel;

@end
