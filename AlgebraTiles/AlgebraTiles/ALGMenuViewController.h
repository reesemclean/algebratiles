//
//  ALGMenuViewController.h
//  AlgebraTiles
//
//  Created by Reese McLean on 4/21/13.
//  Copyright (c) 2013 Reese McLean. All rights reserved.
//

#import <UIKit/UIKit.h>

extern NSString *const ALGIntroductionActivityID;
extern NSString *const ALGFreePlayMatActivityID;
extern NSString *const ALGExpressionMatActivityID;
extern NSString *const ALGComparisionMatActivityID;
extern NSString *const ALGEquationMatActivityID;
extern NSString *const ALGAreaMatActivityID;

@protocol ALGMenuViewControllerDelegate;

@interface ALGMenuViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

+(CGFloat)menuWidth;

@property (nonatomic, weak) id<ALGMenuViewControllerDelegate> delegate;

@end

@protocol ALGMenuViewControllerDelegate <NSObject>

-(void) menuViewDidPressSelectTheme:(ALGMenuViewController*)menuVC;
-(void) menuViewdidPressToggleToolboxSide:(ALGMenuViewController*)menuVC;
-(void) menuView:(ALGMenuViewController*)menuVC didSelectActivityWithID:(NSString*)activityID;
-(void) menuViewDidPressSavedProblems:(ALGMenuViewController*)menuVC;
@end
