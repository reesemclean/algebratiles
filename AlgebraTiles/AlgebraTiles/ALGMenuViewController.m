//
//  ALGMenuViewController.m
//  AlgebraTiles
//
//  Created by Reese McLean on 4/21/13.
//  Copyright (c) 2013 Reese McLean. All rights reserved.
//

#import "ALGMenuViewController.h"

NSString *const ALGIntroductionActivityID = @"ALGIntroductionActivityID";
NSString *const ALGFreePlayMatActivityID = @"ALGFreePlayMatActivityID";
NSString *const ALGExpressionMatActivityID = @"ALGExpressionMatActivityID";
NSString *const ALGComparisionMatActivityID = @"ALGComparisionMatActivityID";
NSString *const ALGEquationMatActivityID = @"ALGEquationMatActivityID";
NSString *const ALGAreaMatActivityID = @"ALGAreaMatActivityID";

@interface ALGMenuViewController ()

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@end

@implementation ALGMenuViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

+(CGFloat)menuWidth {
    return 320.0;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

    switch (section) {
        case 0:
            return 5;
            break;
        case 1:
            return 1;
            break;
        case 2:
            return 2;
            break;
        default:
            break;
    }
    return 0;
    
}

-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 60.0;
}

-(CGFloat) tableView:(UITableView *)aTableView heightForHeaderInSection:(NSInteger)section {
    
    switch (section) {
        case 0:
            return 0.0;
            break;
        case 1:
            return 26.0;
            break;
        case 2:
            return 26.0;
            break;
        default:
            break;
    }
    return 0.0;
}

-(UIView *) tableView:(UITableView *)aTableView viewForHeaderInSection:(NSInteger)section {
    
    if (section == 0) {
        return nil;
    }
    
    UIView *containerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, [self tableView:aTableView heightForHeaderInSection:section])];
    containerView.backgroundColor = [UIColor colorWithRed:244.0/255.0
                                                    green:244.0/255.0
                                                     blue:244.0/255.0
                                                    alpha:.9];
    
    CGFloat headerPadding = 10.0;
    CGFloat yPadding = 6.0;
    
    UILabel *headerLabel = [[UILabel alloc] initWithFrame:CGRectMake(headerPadding, yPadding, CGRectGetWidth(self.view.bounds) - 2 * headerPadding, CGRectGetHeight(containerView.frame) - 2 * yPadding)];
    headerLabel.text = [self tableView:aTableView titleForHeaderInSection:section];
    headerLabel.font = [UIFont fontWithName:@"AvenirNext-DemiBold" size:14.0];
    headerLabel.textColor = [UIColor blackColor];
    headerLabel.backgroundColor = [UIColor clearColor];
    
    [containerView addSubview:headerLabel];
    
    return containerView;
    
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {

    switch (section) {
        case 0:
            return nil;
            break;
        case 1:
            return NSLocalizedString(@"Saved Problems",nil);
            break;
        case 2:
            return NSLocalizedString(@"Settings",nil);
            break;
        default:
            break;
    }
    return nil;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = nil;
    
    static NSString *MatSelectionCellID = @"MatSelectionCellID";
    static NSString *BasicSettingToggleCellID = @"BasicSettingToggleCellID";

    switch (indexPath.section) {
        case 0: {
            cell = [tableView dequeueReusableCellWithIdentifier:MatSelectionCellID forIndexPath:indexPath];
        }
            break;
        case 1: {
            cell = [tableView dequeueReusableCellWithIdentifier:MatSelectionCellID forIndexPath:indexPath];
        }
            break;
        case 2:
            
            switch (indexPath.row) {
                case 0: {
                    cell = [tableView dequeueReusableCellWithIdentifier:BasicSettingToggleCellID forIndexPath:indexPath];
                }
                    break;
                case 1: {
                    cell = [tableView dequeueReusableCellWithIdentifier:MatSelectionCellID forIndexPath:indexPath];
                }
                    break;
                default:
                    break;
            }
            
            break;
        default:
            break;
    }
    
    return cell;
}

-(void) tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    switch (indexPath.section) {
        case 0: {
            
            switch (indexPath.row) {
//                case 0:
//                    cell.textLabel.text = @"Tile Introduction";
//                    break;
                case 0:
                    cell.textLabel.text = @"Exploration";
                    break;
                case 1:
                    cell.textLabel.text = @"Expression Mat";
                    break;
                case 2:
                    cell.textLabel.text = @"Comparision Mat";
                    break;
                case 3:
                    cell.textLabel.text = @"Equation Mat";
                    break;
                case 4:
                    cell.textLabel.text = @"Area Mat";
                    break;
                default:
                    break;
            }
            
        }
            break;
        case 1:
            
            switch (indexPath.row) {
                case 0:
                    cell.textLabel.text = @"Saved Problems";
                    break;
                default:
                    break;
            }
            
            break;
        case 2:
            
            switch (indexPath.row) {
                case 0:
                    cell.textLabel.text = @"Themes";
                    break;
                case 1:
                    cell.textLabel.text = @"Toggle Toolbox Side";
                    break;
                default:
                    break;
            }
            
            break;
        default:
            break;
    }

    
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    switch (indexPath.section) {
        case 0: {
            
            switch (indexPath.row) {
//                case 0:
//                    //Introduction
//                    [self.delegate menuView:self didSelectActivityWithID:ALGIntroductionActivityID];
//                    break;
                case 0:
                    //Free Play mat
                    [self.delegate menuView:self didSelectActivityWithID:ALGFreePlayMatActivityID];
                    break;
                case 1:
                    //Expression mat
                    [self.delegate menuView:self didSelectActivityWithID:ALGExpressionMatActivityID];
                    break;
                case 2:
                    //Comparision Mat
                    [self.delegate menuView:self didSelectActivityWithID:ALGComparisionMatActivityID];
                    break;
                case 3:
                    //Equation mat
                    [self.delegate menuView:self didSelectActivityWithID:ALGEquationMatActivityID];
                    break;
                case 4:
                    //Area Mat
                    [self.delegate menuView:self didSelectActivityWithID:ALGAreaMatActivityID];
                    break;
                default:
                    break;
            }
            
        }
            break;
        case 1:
            
            switch (indexPath.row) {
                case 0:
                    //Themes
                    [self.delegate menuViewDidPressSavedProblems:self];
                    break;
                default:
                    break;
            }
            
            break;
        case 2:
            
            switch (indexPath.row) {
                case 0:
                    //Themes
                    [self.delegate menuViewDidPressSelectTheme:self];
                    break;
                case 1:
                    //Toggle Toolbox Side
                    [self.delegate menuViewdidPressToggleToolboxSide:self];
                    break;
                default:
                    break;
            }
            
            break;
        default:
            break;
    }

}

@end
