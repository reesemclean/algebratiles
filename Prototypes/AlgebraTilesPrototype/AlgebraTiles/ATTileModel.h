//
//  ATTileModel.h
//  AlgebraTiles
//
//  Created by Reese McLean on 12/7/12.
//  Copyright (c) 2012 Reese McLean. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "TileConstants.h"

@interface ATTileModel : NSObject <NSCopying>

+(id) tileWithType:(ATTileType)type;

@property (nonatomic, assign) BOOL negative;
@property (nonatomic, assign) ATTileType tileType;

-(NSString*) tileTitle;

@end
