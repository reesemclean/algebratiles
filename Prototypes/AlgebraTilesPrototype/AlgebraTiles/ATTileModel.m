//
//  ATTileModel.m
//  AlgebraTiles
//
//  Created by Reese McLean on 12/7/12.
//  Copyright (c) 2012 Reese McLean. All rights reserved.
//

#import "ATTileModel.h"

@implementation ATTileModel

-(id) initWithTileType:(ATTileType)type {
    self = [super init];
    if (self) {
        _tileType = type;
        _negative = NO;
    }
    return self;
}

+(id) tileWithType:(ATTileType)type {
    return [[ATTileModel alloc] initWithTileType:type];
}

// In the implementation
-(id)copyWithZone:(NSZone *)zone
{
    ATTileModel *anotherModel = [[ATTileModel alloc] initWithTileType:self.tileType];
    anotherModel.negative = self.negative;
    
    return anotherModel;
}

-(NSString*) tileTitle {
    
    switch (self.tileType) {
        case ATTileTypeUnit:
            return @"Unit";
            break;
        case ATTileTypeX:
            return @"x";
            break;
        case ATTileTypeXSquared:
            return @"x²";
            break;
        case ATTileTypeXY:
            return @"xy";
            break;
        case ATTileTypeY:
            return @"y";
            break;
        case ATTileTypeYSquared:
            return @"y²";
            break;
        default:
            break;
    }
    
    return @"Unit";

}

@end
