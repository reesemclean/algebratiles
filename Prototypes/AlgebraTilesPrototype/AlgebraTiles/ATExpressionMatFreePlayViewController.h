//
//  ATFreePlayMatViewController.h
//  AlgebraTiles
//
//  Created by Reese McLean on 12/8/12.
//  Copyright (c) 2012 Reese McLean. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ATBaseExpressionMatViewController.h"

@interface ATExpressionMatFreePlayViewController : ATBaseExpressionMatViewController

@property (weak, nonatomic) IBOutlet UILabel *expressionLabel;

@end
