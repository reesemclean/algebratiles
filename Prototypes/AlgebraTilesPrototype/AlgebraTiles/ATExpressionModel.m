//
//  ATExpressionModel.m
//  AlgebraTiles
//
//  Created by Reese McLean on 12/8/12.
//  Copyright (c) 2012 Reese McLean. All rights reserved.
//

#import "ATExpressionModel.h"

#import "ATTileModel.h"
#import "ATExpressionTermModel.h"

NSString * const AT_COEFFICIENT_UNIT_KEY = @"AT_COEFFICIENT_UNIT_KEY";
NSString * const AT_COEFFICIENT_X_KEY = @"AT_COEFFICIENT_X_KEY";
NSString * const AT_COEFFICIENT_X_SQUARED_KEY = @"AT_COEFFICIENT_X_SQUARED_KEY";
NSString * const AT_COEFFICIENT_Y_KEY = @"AT_COEFFICIENT_Y_KEY";
NSString * const AT_COEFFICIENT_Y_SQUARED_KEY = @"AT_COEFFICIENT_Y_SQUARED_KEY";
NSString * const AT_COEFFICIENT_XY_KEY = @"AT_COEFFICIENT_XY_KEY";
NSString * const AT_COEFFICIENT_NEGATIVE_UNIT_KEY = @"AT_COEFFICIENT_NEGATIVE_UNIT_KEY";
NSString * const AT_COEFFICIENT_NEGATIVE_X_KEY = @"AT_COEFFICIENT_NEGATIVE_X_KEY";
NSString * const AT_COEFFICIENT_NEGATIVE_X_SQUARED_KEY = @"AT_COEFFICIENT_NEGATIVE_X_SQUARED_KEY";
NSString * const AT_COEFFICIENT_NEGATIVE_Y_KEY = @"AT_COEFFICIENT_NEGATIVE_Y_KEY";
NSString * const AT_COEFFICIENT_NEGATIVE_Y_SQUARED_KEY = @"AT_COEFFICIENT_NEGATIVE_Y_SQUARED_KEY";
NSString * const AT_COEFFICIENT_NEGATIVE_XY_KEY = @"AT_COEFFICIENT_NEGATIVE_XY_KEY";

@implementation ATExpressionModel

-(id) initWithPositiveTermsModels:(NSArray *)postiveModels andNegativeModels:(NSArray *)negativeModels {
    self = [super init];
    if (self) {
        self.positiveTerms = [postiveModels copy];
        self.negativeTerms = [negativeModels copy];
    }
    return self;
}



+(id) expressionWithPositiveTerms:(NSArray*)positiveTerms andNegativeTerms:(NSArray*)negativeTerms {
    return [[ATExpressionModel alloc] initWithPositiveTermsModels:positiveTerms andNegativeModels:negativeTerms];
}

+(NSDictionary*) coefficientsInTileArray:(NSArray*)tileArray {
        
    int numberOfUnits = 0;
    int numberOfX = 0;
    int numberOfXSquared = 0;
    int numberOfY = 0;
    int numberOfYSquared = 0;
    int numberOfXY = 0;
    int numberOfNegativeUnits = 0;
    int numberOfNegativeX = 0;
    int numberOfNegativeXSquared = 0;
    int numberOfNegativeY = 0;
    int numberOfNegativeYSquared = 0;
    int numberOfNegativeXY = 0;
    
    for (ATTileModel *tileModel in tileArray) {
        switch (tileModel.tileType) {
            case ATTileTypeUnit:
                if (tileModel.negative) {
                    numberOfNegativeUnits++;
                } else {
                    numberOfUnits++;
                }
                break;
            case ATTileTypeX:
                if (tileModel.negative) {
                    numberOfNegativeX++;
                } else {
                    numberOfX++;
                }
                break;
            case ATTileTypeXSquared:
                if (tileModel.negative) {
                    numberOfNegativeXSquared++;
                } else {
                    numberOfXSquared++;
                }
                break;
            case ATTileTypeY:
                if (tileModel.negative) {
                    numberOfNegativeY++;
                } else {
                    numberOfY++;
                }
                break;
            case ATTileTypeYSquared:
                if (tileModel.negative) {
                    numberOfNegativeYSquared++;
                } else {
                    numberOfYSquared++;
                }
                break;
            case ATTileTypeXY:
                if (tileModel.negative) {
                    numberOfNegativeXY++;
                } else {
                    numberOfXY++;
                }
                break;
            default:
                break;
        }
    }
    
    return @{AT_COEFFICIENT_UNIT_KEY : @(numberOfUnits),
                AT_COEFFICIENT_X_KEY : @(numberOfX),
    AT_COEFFICIENT_X_SQUARED_KEY : @(numberOfXSquared),
    AT_COEFFICIENT_Y_KEY : @(numberOfY),
    AT_COEFFICIENT_Y_SQUARED_KEY : @(numberOfYSquared),
    AT_COEFFICIENT_XY_KEY : @(numberOfXY),
    AT_COEFFICIENT_NEGATIVE_UNIT_KEY : @(numberOfNegativeUnits),
    AT_COEFFICIENT_NEGATIVE_X_KEY : @(numberOfNegativeX),
    AT_COEFFICIENT_NEGATIVE_X_SQUARED_KEY : @(numberOfNegativeXSquared),
    AT_COEFFICIENT_NEGATIVE_Y_KEY : @(numberOfNegativeY),
    AT_COEFFICIENT_NEGATIVE_Y_SQUARED_KEY : @(numberOfNegativeYSquared),
    AT_COEFFICIENT_NEGATIVE_XY_KEY : @(numberOfNegativeXY) };
}

-(NSString*) formatStringFromCoefficientDictionary:(NSDictionary*)dictionary {
    
    NSMutableString *string = [NSMutableString string];

    int positiveXSquared = [dictionary[AT_COEFFICIENT_X_SQUARED_KEY] intValue];
    int positiveYSquared = [dictionary[AT_COEFFICIENT_Y_SQUARED_KEY] intValue];
    int positiveXY = [dictionary[AT_COEFFICIENT_XY_KEY] intValue];
    int positiveX = [dictionary[AT_COEFFICIENT_X_KEY] intValue];
    int positiveY = [dictionary[AT_COEFFICIENT_Y_KEY] intValue];
    int positiveUnits = [dictionary[AT_COEFFICIENT_UNIT_KEY] intValue];
    
    int negativeXSquared = [dictionary[AT_COEFFICIENT_NEGATIVE_X_SQUARED_KEY] intValue];
    int negativeYSquared = [dictionary[AT_COEFFICIENT_NEGATIVE_Y_SQUARED_KEY] intValue];
    int negativeXY = [dictionary[AT_COEFFICIENT_NEGATIVE_XY_KEY] intValue];
    int negativeX = [dictionary[AT_COEFFICIENT_NEGATIVE_X_KEY] intValue];
    int negativeY = [dictionary[AT_COEFFICIENT_NEGATIVE_Y_KEY] intValue];
    int negativeUnits = [dictionary[AT_COEFFICIENT_NEGATIVE_UNIT_KEY] intValue];
    
    if (positiveXSquared != 0) {
        [string appendFormat:@"%dx²", positiveXSquared];
    }
    
    if (negativeXSquared != 0) {
        
        if ([string length] > 0) {
            [string appendString:@" - "];
            [string appendFormat:@"%dx²", negativeXSquared];
        } else {
            [string appendFormat:@"-%dx²", negativeXSquared];
        }
        
    }
    
    if (positiveYSquared != 0) {
        
        if ([string length] > 0) {
            [string appendString:@" + "];
        }
        
        [string appendFormat:@"%dy²", positiveYSquared];

    }
    
    if (negativeYSquared != 0) {
        
        if ([string length] > 0) {
            [string appendString:@" - "];
            [string appendFormat:@"%dy²", negativeYSquared];
        } else {
            [string appendFormat:@"-%dy²", negativeYSquared];
        }
        
    }
    
    if (positiveXY != 0) {
        if ([string length] > 0) {
            [string appendString:@" + "];
        }
        
        [string appendFormat:@"%dxy", positiveXY];

    }
    
    if (negativeXY != 0) {
        
        if ([string length] > 0) {
            [string appendString:@" - "];
            [string appendFormat:@"%dxy", negativeXY];
        } else {
            [string appendFormat:@"-%dxy", negativeXY];
        }
        
    }

    if (positiveX != 0) {
        if ([string length] > 0) {
            [string appendString:@" + "];
        } 
            [string appendFormat:@"%dx", positiveX];
    }
    
    if (negativeX != 0) {
        
        if ([string length] > 0) {
            [string appendString:@" - "];
            [string appendFormat:@"%dx", negativeX];
        } else {
            [string appendFormat:@"-%dx", negativeX];
        }
        
    }
    
    if (positiveY != 0) {
        if ([string length] > 0) {
            [string appendString:@" + "];
        }
            [string appendFormat:@"%dy", positiveY];
    }
    
    if (negativeY != 0) {
        
        if ([string length] > 0) {
            [string appendString:@" - "];
            [string appendFormat:@"%dy", negativeY];
        } else {
            [string appendFormat:@"-%dy", negativeY];
        }
        
    }
    
    if (positiveUnits != 0) {
        if ([string length] > 0) {
            [string appendString:@" + "];
        }
            [string appendFormat:@"%d", positiveUnits];
    }
    
    if (negativeUnits != 0) {
        
        if ([string length] > 0) {
            [string appendString:@" - "];
            [string appendFormat:@"%d", negativeUnits];
        } else {
            [string appendFormat:@"-%d", negativeUnits];
        }
        
    }
    
    return [string copy];
}

-(NSString *)stringForTileType:(ATTileType) tileType {
    
    switch (tileType) {
        case ATTileTypeUnit:
            return @"";
            break;
        case ATTileTypeX:
            return @"x";
            break;
        case ATTileTypeXSquared:
            return @"x²";
            break;
        case ATTileTypeXY:
            return @"xy";
            break;
        case ATTileTypeY:
            return @"y";
            break;
        case ATTileTypeYSquared:
            return @"y²";
            break;
        default:
            break;
    }
    
}

-(NSString*)formatStringForTerms:(NSArray*)arrayOfTermModels {
    
    NSMutableString *string = [NSMutableString string];
    
    for (ATExpressionTermModel *term in arrayOfTermModels) {
        
        if ([string length] > 0) {
            
            if ([term negative]) {
                [string appendString:@" - "];

            } else {
                [string appendString:@" + "];
            }
            
            [string appendFormat:@"%d", [term coefficient]];
            [string appendString:[self stringForTileType:[term tileType]]];
            
        } else {
            
            if ([term negative]) {
                [string appendString:@"-"];
                
            }
            
            [string appendFormat:@"%d%@", [term coefficient], [self stringForTileType:[term tileType]]];
            
        }
        
    }
    
    return [string copy];
    
}

-(NSString*)expressionStringValue {
        
    NSString *positiveString = [self formatStringForTerms:self.positiveTerms];
    NSString *negativeString = [self formatStringForTerms:self.negativeTerms];

    NSMutableString *string = [NSMutableString string];
    
    [string appendString:positiveString];
    
    if ([negativeString length] > 0) {
        
        if ([string length] > 0) {
            [string appendString:@" "];
        }
        
        [string appendFormat:@"- (%@)", negativeString];
    }
    
    if ([string length] < 1) {
        [string appendString:@"0"];
    }
    
    return [string copy];
}

@end
