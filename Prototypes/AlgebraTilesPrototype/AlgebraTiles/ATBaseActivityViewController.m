//
//  ATBaseActivityViewController.m
//  AlgebraTiles
//
//  Created by Reese McLean on 12/22/12.
//  Copyright (c) 2012 Reese McLean. All rights reserved.
//

#import "ATBaseActivityViewController.h"

@interface ATBaseActivityViewController ()

@end

@implementation ATBaseActivityViewController

+(NSString*)storyboardID {
    NSLog(@"Override storyboard id please...");
    return nil;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [self.menuButton addTarget:nil action:@selector(menuButtonPushed:) forControlEvents:UIControlEventTouchUpInside];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
