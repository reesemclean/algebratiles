//
//  ATBaseAreaMatViewController.m
//  AlgebraTiles
//
//  Created by Reese McLean on 12/23/12.
//  Copyright (c) 2012 Reese McLean. All rights reserved.
//

#import "ATBaseAreaMatViewController.h"

#import "ATAreaMatView.h"
#import "ATTileView.h"

@interface ATBaseAreaMatViewController ()

@end

@implementation ATBaseAreaMatViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self setupTileNotifications];
}

-(void) viewWillDisappear:(BOOL)animated {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [super viewWillDisappear:animated];
}

-(void) setupTileNotifications {
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didSpawnTileNotification:)
                                                 name:kATDidSpawnTileViewNotification
                                               object:nil];
    
}

-(void) didSpawnTileNotification:(NSNotification*)notification {
    
    ATTileView *spawnedView = (ATTileView*)[notification object];
    //Add can flip = no here
    spawnedView.canFlipToNegative = NO;
    spawnedView.delegate = self;
    
    [self.view addSubview:spawnedView];
    [self.view bringSubviewToFront:spawnedView];
    
}

#pragma mark Tile View Delegate

-(void) didStartTileViewPan:(ATTileView*)tileView {
    
    [tileView.superview bringSubviewToFront:tileView];
    [self.areaMat didStartPanTile:tileView];
    
}

-(void) didPanTileView:(ATTileView*)tileView withTranslation:(CGPoint)translation {
    
    [self.areaMat didPanTile:tileView withTranslation:translation];
    
}

-(void) didFinishTileViewPan:(ATTileView*)tileView {
    
    if (![self.areaMat tryToClaimTile:tileView]) {
        [tileView removeAnimated:YES completion:nil];
    }
    
    [self.areaMat didFinishTilePan:tileView];
    
}

@end
