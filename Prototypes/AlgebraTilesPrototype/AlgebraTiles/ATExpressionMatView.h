//
//  ATExpressionMatView.h
//  AlgebraTiles
//
//  Created by Reese McLean on 12/8/12.
//  Copyright (c) 2012 Reese McLean. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ATTileView;
@class ATExpressionModel;
@class ATTileGroupCollectionModel;

@interface ATExpressionMatView : UIView

-(ATExpressionModel*) expressionModel;

-(void) removeGroupForTile:(ATTileView*)tileView animated:(BOOL)animated;

-(void) cleanupTiles;

-(BOOL) tryToClaimTile:(ATTileView*) tileView inGroup:(ATTileGroupCollectionModel*)group;

-(BOOL) ownsTile:(ATTileView*)tileView;

-(ATTileGroupCollectionModel *) groupForTile:(ATTileView*)tileView;

-(void) didPanTile:(ATTileView*)tileView withTranslation:(CGPoint)translation;
-(void) cleanupTilesWithDroppedTileView:(ATTileView*) tileView andGroup:(ATTileGroupCollectionModel*) group;
-(void) didFlipTile:(ATTileView*)tileView;

@end
