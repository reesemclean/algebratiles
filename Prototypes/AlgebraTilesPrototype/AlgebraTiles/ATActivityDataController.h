//
//  ATActivityDataController.h
//  AlgebraTiles
//
//  Created by Reese McLean on 12/22/12.
//  Copyright (c) 2012 Reese McLean. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "ATActivityModel.h"
#import "ATBaseActivityViewController.h"

@interface ATActivityDataController : NSObject

+(id) sharedController;

-(ATActivityModel *)lastPlayedActivity;
-(ATBaseActivityViewController*) lastPlayedActivityViewControllerForStoryboard:(UIStoryboard*)storyboard;
-(ATBaseActivityViewController*) viewControllerForActivty:(ATActivityModel*)activity
                                forStoryboard:(UIStoryboard*)storyboard;

-(ATActivityModel*)activityForActivityID:(NSString*)activityID;

-(void) didStartActivity:(ATActivityModel*) activityModel;

-(NSUInteger)numberOfSections;
-(NSString*)titleForSection:(NSUInteger)sectionIndex;
-(NSUInteger)numberOfRowsInSection:(NSUInteger)section;
-(ATActivityModel*)activityModelForIndexPath:(NSIndexPath*)indexPath;

@end
