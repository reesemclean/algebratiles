//
//  ATMenuViewController.h
//  AlgebraTiles
//
//  Created by Reese McLean on 12/8/12.
//  Copyright (c) 2012 Reese McLean. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum ATFreePlayMatType {
    ATFreePlayExpressionMatType,
    ATFreePlayAreaMatType,
    ATFreePlayMatTypeCount
} ATFreePlayMatType;

typedef enum ATGuidedPracticeType {
    
    ATGuidedPracticeSimplifyExpressionsType,
    ATGuidedPracticeTypeCount
    
} ATGuidedPracticeType;

@class ATActivityModel;
@protocol ATMenuViewControllerDelegate;

@interface ATMenuViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITableView *tableView;
+(CGFloat)menuWidth;

- (IBAction)toggleToolboxSidePressed:(id)sender;
@property (nonatomic, weak) id<ATMenuViewControllerDelegate> delegate;

@end

@protocol ATMenuViewControllerDelegate <NSObject>

-(void) shouldToggleToolboxSide:(ATMenuViewController*) menuViewController;
-(void) didSelectActivityModel:(ATActivityModel*)activity fromMenu:(ATMenuViewController*)menuViewController;

@end
