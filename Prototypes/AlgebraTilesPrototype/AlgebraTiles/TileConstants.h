//
//  TileConstants.h
//  AlgebraTiles
//
//  Created by Reese McLean on 12/7/12.
//  Copyright (c) 2012 Reese McLean. All rights reserved.
//

#ifndef AlgebraTiles_TileConstants_h
#define AlgebraTiles_TileConstants_h

#define AT_UNIT_LENGTH 44.0
#define AT_X_LENGTH 116.0
#define AT_Y_LENGTH 144.0

#define AT_UNIT_COLOR [UIColor colorWithRed:21.0/255.0 green:107.0/255.0 blue:193.0/255.0 alpha:1.0]
#define AT_X_COLOR [UIColor colorWithRed:36.0/255.0 green:165.0/255.0 blue:255.0/255.0 alpha:1.0]
#define AT_Y_COLOR [UIColor colorWithRed:19.0/255.0 green:193.0/255.0 blue:27.0/255.0 alpha:1.0]
#define AT_XY_COLOR [UIColor colorWithRed:151.0/255.0 green:61.0/255.0 blue:224.0/255.0 alpha:1.0]
#define AT_NEGATIVE_COLOR [UIColor colorWithRed:212.0/255.0 green:34.0/255.0 blue:0.0/255.0 alpha:1.0]

typedef enum ATTileType {
    ATTileTypeUnit,
    ATTileTypeX,
    ATTileTypeXSquared,
    ATTileTypeY,
    ATTileTypeYSquared,
    ATTileTypeXY
    } ATTileType;

#endif
