//
//  ATActivityModel.h
//  AlgebraTiles
//
//  Created by Reese McLean on 12/22/12.
//  Copyright (c) 2012 Reese McLean. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ATActivityModel : NSObject

@property (nonatomic, strong) NSString *menuName;
@property (nonatomic, strong) NSDictionary *options;
@property (nonatomic, strong) NSString *storyboardID;
@property (nonatomic, strong) NSString *activityID;

//-(CGFloat) progressForStudent:(ATStudent*)student;

+(id)activityModelWithMenuName:(NSString*) menuName andOptions:(NSDictionary*)options andStoryboardID:(NSString*) storyboardID andActivityID:(NSString*)activityID;

@end
