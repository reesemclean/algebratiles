//
//  ATAreaFreePlayViewController.h
//  AlgebraTiles
//
//  Created by Reese McLean on 12/12/12.
//  Copyright (c) 2012 Reese McLean. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ATBaseAreaMatViewController.h"

@interface ATAreaFreePlayViewController : ATBaseAreaMatViewController

@end
