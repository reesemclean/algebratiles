//
//  ATBaseAreaMatViewController.h
//  AlgebraTiles
//
//  Created by Reese McLean on 12/23/12.
//  Copyright (c) 2012 Reese McLean. All rights reserved.
//

#import "ATBaseActivityViewController.h"

#import "ATBaseActivityViewController.h"
#import "ATTileView.h"

@class ATAreaMatView;

@interface ATBaseAreaMatViewController : ATBaseActivityViewController <ATTileViewDelegate>

@property (weak, nonatomic) IBOutlet ATAreaMatView *areaMat;

@end
