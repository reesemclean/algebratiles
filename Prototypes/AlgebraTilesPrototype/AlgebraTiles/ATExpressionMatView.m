//
//  ATExpressionMatView.m
//  AlgebraTiles
//
//  Created by Reese McLean on 12/8/12.
//  Copyright (c) 2012 Reese McLean. All rights reserved.
//

#import "ATExpressionMatView.h"

#import "ATTileView.h"
#import "ATExpressionModel.h"
#import "ATTileGroupCollectionModel.h"
#import "ATTileModel.h"

@interface ATExpressionMatView ()

@property (nonatomic, strong) NSMutableSet *tiles;
@property (nonatomic, strong) NSMutableSet *groups;

@end

@implementation ATExpressionMatView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self commonSetup];
    }
    return self;
}

-(id) initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self commonSetup];
    }
    
    return self;
}

-(void) commonSetup {
    self.tiles = [NSMutableSet set];
    self.groups = [NSMutableSet set];
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetStrokeColorWithColor(context, [[UIColor lightGrayColor] CGColor]);
    CGContextSetLineWidth(context, 1.0);
    
    CGRect insetRect = CGRectInset(self.bounds, .5, .5);
    CGContextStrokeRect(context, insetRect);

    CGMutablePathRef path = CGPathCreateMutable();
    CGFloat midY = self.bounds.size.height/2.0 - .5;
    CGPathMoveToPoint(path, NULL, .5, midY);
    CGPathAddLineToPoint(path, NULL, self.bounds.size.width - .5, midY);
    CGContextAddPath(context, path);
    
    CGContextStrokePath(context);
    
    CGPathRelease(path);
    
    CGContextSetStrokeColorWithColor(context, [[UIColor darkGrayColor] CGColor]);
    CGContextSetLineWidth(context, 2.0);

    CGMutablePathRef plusSignPath = CGPathCreateMutable();
    CGPathMoveToPoint(plusSignPath, NULL, 22.0, 12.0);
    CGPathAddLineToPoint(plusSignPath, NULL, 22.0, 32.0);
    CGPathMoveToPoint(plusSignPath, NULL, 12.0, 22.0);
    CGPathAddLineToPoint(plusSignPath, NULL, 32.0, 22.0);
    CGContextAddPath(context, path);
    CGContextStrokePath(context);
    CGPathRelease(plusSignPath);
    
    CGMutablePathRef negativeSignPath = CGPathCreateMutable();
    CGPathMoveToPoint(negativeSignPath, NULL, 12.0, self.bounds.size.height - 22.0);
    CGPathAddLineToPoint(negativeSignPath, NULL, 32.0, self.bounds.size.height - 22.0);
    CGContextAddPath(context, negativeSignPath);
    CGContextStrokePath(context);
    CGPathRelease(negativeSignPath);

}

-(BOOL) tryToClaimTile:(ATTileView*) tileView inGroup:(ATTileGroupCollectionModel*)group {
    
    if (group) {
        CGPoint convertedCenter = [[group superview] convertPoint:[group centerOfGroup] toView:self];

        if (CGRectContainsPoint(self.bounds, convertedCenter)) {
            return YES;
        } else {
            return NO;
        }
    }
    
    CGPoint convertedCenter = [tileView.superview convertPoint:tileView.center toView:self];
    
    if (CGRectContainsPoint(self.bounds, convertedCenter)) {
        //Keep on mat
        
        return YES;

    }
    
    return NO;
}

-(void) didFlipTile:(ATTileView*)tileView {
    
    if (![self ownsTile:tileView]) {
        return;
    }
    
    ATTileGroupCollectionModel *foundGroup = [self groupForTile:tileView];

    BOOL newStatus = !tileView.tileModel.negative;
    
    if (foundGroup) {
        for (ATTileView *tile in foundGroup.tiles) {
            [tile.tileModel setNegative:newStatus];
            [tile updateTileColorAnimated:YES];
        }
    } else {
        [tileView.tileModel setNegative:newStatus];
        [tileView updateTileColorAnimated:YES];
    }
}

-(void) didPanTile:(ATTileView*)tileView withTranslation:(CGPoint)translation {

    ATTileGroupCollectionModel *foundGroup = [self groupForTile:tileView];
    
    if (foundGroup) {
        if (foundGroup.topTile == tileView) {
            
            [foundGroup removeTile:tileView];
            [tileView.superview bringSubviewToFront:tileView];

        } else {
            for (ATTileView *tile in foundGroup.tiles) {
                [ATTileView addShadowToTileView:tile];
                if (tile != tileView) {
                    tile.center = CGPointMake(tile.center.x + translation.x,
                                              tile.center.y + translation.y);
                    
                }
            }
        }
    }

}

-(ATTileGroupCollectionModel *) groupForTile:(ATTileView*)tileView {
    
    __block ATTileGroupCollectionModel *foundGroup = nil;

    [self.groups enumerateObjectsUsingBlock:^(ATTileGroupCollectionModel *group, BOOL *stop) {
        
        if ([group containsTile:tileView]) {
            foundGroup = group;
            *stop = YES;
        }
        
    }];
    return foundGroup;
}

-(ATExpressionModel*) expressionModel {
    
    NSMutableArray *positiveTileTerms = [NSMutableArray array];
    NSMutableArray *negativeTileTerms = [NSMutableArray array];
    
    for (ATTileGroupCollectionModel *group in self.groups) {
        
        if ([group.tiles count] > 0) {
            if ([group centerOfGroup].y < self.bounds.size.height/2.0 - .5) {
                [positiveTileTerms addObject:[group termModel]];
            } else {
                [negativeTileTerms addObject:[group termModel]];
            }
        }
        
    }
    
    return [ATExpressionModel expressionWithPositiveTerms:positiveTileTerms andNegativeTerms:negativeTileTerms];
}

-(void) removeGroupForTile:(ATTileView*)tileView animated:(BOOL)animated {
    
    if (![self.tiles containsObject:tileView]) {
        return;
    }
    
    ATTileGroupCollectionModel *group = [self groupForTile:tileView];
    
    if (group) {
        for (ATTileView *tile in group.tiles) {
            
            [tile removeAnimated:animated completion:nil];
            
        }
    } else {
        [tileView removeAnimated:animated completion:nil];
    }
    
}

-(BOOL) ownsTile:(ATTileView*)tileView {
    return [self.tiles containsObject:tileView];
}

-(void) cleanupTilesWithDroppedTileView:(ATTileView*) tileView andGroup:(ATTileGroupCollectionModel*) group {

    if ([self tryToClaimTile:tileView inGroup:group]) {
        
        if (group) {
            
            for (ATTileView *tile in group.tiles) {
                CGPoint convertedCenter = [tile.superview convertPoint:tile.center toView:self];
                tile.center = convertedCenter;
                [self addSubview:tile];
                [self.tiles addObject:tile];
            }
            
            [self.groups addObject:group];
            
        } else {
            CGPoint convertedCenter = [tileView.superview convertPoint:tileView.center toView:self];
            tileView.center = convertedCenter;
            [self addSubview:tileView];
            [self.tiles addObject:tileView];
        }
        
    } else {
        
        if (group) {
            
            for (ATTileView *tile in group.tiles) {

                [self.tiles removeObject:tile];
                
            }
            [self.groups removeObject:group];

            
        } else {
            [self.tiles removeObject:tileView];
        }
        
    }
    
    if (![self.tiles containsObject:tileView]) {
        return; //Has been dropped off mat
    }
    
    __block ATTileGroupCollectionModel *foundGroup = [self groupForTile:tileView];
    
    if (foundGroup) {
        for (ATTileView *tile in foundGroup.tiles) {
            [ATTileView removeShadowFromTileView:tile];
        }
        
        //Check if group should join another group
        [self.groups enumerateObjectsUsingBlock:^(ATTileGroupCollectionModel* group, BOOL *stop) {
            
            if (group != foundGroup) {
                CGRect groupRect = [group rectForGroup];
                
                if (CGRectContainsPoint(groupRect, [foundGroup centerOfGroup])) {
                    //Check if same type of tiles
                    if ([[group topTile] isSameTypeOfTile:[foundGroup topTile]]) {
                        
                        for (ATTileView *tile in foundGroup.tiles) {
                            [group addTile:tile];
                        }
                        [foundGroup.tiles removeAllObjects];
                        foundGroup = group;
                        *stop  = YES;
                    }
                    
                }

            }
            
        }];

        
    }
    
    if (!foundGroup) {
        //Check if individual tile should join group
        [self.groups enumerateObjectsUsingBlock:^(ATTileGroupCollectionModel* group, BOOL *stop) {
            CGRect groupRect = [group rectForGroup];
            
            if (CGRectContainsPoint(groupRect, tileView.center)) {
                //Check if same type of tiles
                if ([[group topTile] isSameTypeOfTile:tileView]) {
                    
                    [group addTile:tileView];
                    foundGroup = group;
                    *stop  = YES;
                }
                
            }
            
        }];
    }
    
    //Create group for itself if still dropped on mat
    if (!foundGroup) {
        ATTileGroupCollectionModel *newGroup = [[ATTileGroupCollectionModel alloc] initWithTiles:@[tileView]];
        [self.groups addObject:newGroup];
        foundGroup = newGroup;
    }
        
    //Line up group
    if (foundGroup) {
        
        [UIView animateWithDuration:.2
                              delay:0.0
                            options:UIViewAnimationOptionAllowUserInteraction
                         animations:^{
                             
                             ATTileView *bottomTile = [foundGroup bottomTile];
                             CGFloat horizontalOverlap = 1*[bottomTile trueWidth]/4.0;
                             CGFloat verticalOverlap = 8.0;
                             ATTileView *previousTile = nil;
                             
                             for (ATTileView *tile in foundGroup.tiles) {
                                 
                                 if (previousTile) {
                                     tile.center = CGPointMake(previousTile.center.x + horizontalOverlap, previousTile.center.y + verticalOverlap);
                                 }
                                 
                                 previousTile = tile;
                             }
                         }
                         completion:^(BOOL finished) {
                             
                         }];
        
        
        
    }
    
    //Check if group is on mat, and adjust
    if (foundGroup) {
        
        CGFloat distanceToMoveHorizontally = 0.0;
        CGFloat distanceToMoveVertically = 0.0;
        CGRect groupFrame = [foundGroup rectForGroup];
        CGPoint groupCenter = [foundGroup centerOfGroup];
        
        if (!CGRectContainsRect(self.bounds, groupFrame) && CGRectContainsPoint(self.bounds, groupCenter)) {
            
            //(x)
            if (CGRectGetMinX(groupFrame) < 1) {
                distanceToMoveHorizontally = fabsf(CGRectGetMinX(groupFrame)) + 1.0;
            } else if (CGRectGetMaxX(groupFrame) > CGRectGetWidth(self.bounds) - 1.0) {
                distanceToMoveHorizontally = -(CGRectGetMaxX(groupFrame) - (CGRectGetWidth(self.bounds) -1));
            }
            
            //(y)
            if (CGRectGetMinY(groupFrame) < 1) {
                distanceToMoveVertically = fabsf(CGRectGetMinY(groupFrame)) + 1.0;
            } else if (CGRectGetMaxY(groupFrame) > CGRectGetHeight(self.bounds) - 1) {
                distanceToMoveVertically = -(CGRectGetMaxY(groupFrame) - (CGRectGetHeight(self.bounds) -1));
            }

            
        }
                
        //Check midline
        CGRect midlineRect = CGRectMake(0.0, self.bounds.size.height/2.0 - .5, self.bounds.size.width, 1.0);

        CGRect newGroupFrame = CGRectMake(groupFrame.origin.x + distanceToMoveHorizontally,
                                          groupFrame.origin.y + distanceToMoveVertically,
                                          groupFrame.size.width,
                                          groupFrame.size.height);
        
        //(y) midline
        if (CGRectIntersectsRect(midlineRect, newGroupFrame)) {
            
            if (CGRectGetMidY(newGroupFrame) < self.bounds.size.height/2.0 - .5) {
                //Adjust Up
                distanceToMoveVertically -= CGRectGetMaxY(newGroupFrame) - self.bounds.size.height/2.0 + 1.0;
            } else {
                //Adjust Down
                distanceToMoveVertically += self.bounds.size.height/2.0 - CGRectGetMinY(newGroupFrame);
            }
            
        }

        [UIView animateWithDuration:.1
                              delay:0.0
                            options:UIViewAnimationOptionAllowUserInteraction | UIViewAnimationOptionCurveEaseOut
                         animations:^{
                             
                         }
                         completion:^(BOOL finished) {
                             for (ATTileView *tileView in foundGroup.tiles) {
                                 
                                 tileView.center = CGPointMake(tileView.center.x + distanceToMoveHorizontally,
                                                               tileView.center.y + distanceToMoveVertically);
                                 
                             }
                         }];
        
                
    }

    //Find groups with 0 tiles, remove them
    NSMutableSet *groupsToDelete = [NSMutableSet set];
    for (ATTileGroupCollectionModel *group in self.groups) {
        if ([group.tiles count] < 1) {
            [groupsToDelete addObject:group];
        }
    }
    for (ATTileGroupCollectionModel *group in groupsToDelete) {
        [self.groups removeObject:group];
    }
    
}

-(void) cleanupTiles {

    for (ATTileView *view in self.tiles) {
    
        CGRect viewFrame = view.frame;
        CGRect newFrame = view.frame;
        
        //Check outer bounds
        if (!CGRectContainsRect(self.bounds, viewFrame)) {
            
            if (CGRectContainsPoint(self.bounds, view.center)) {
                
                //Nudge back inside (x)
                if (CGRectGetMinX(viewFrame) < 1) {
                    newFrame = CGRectMake(1.0, newFrame.origin.y, newFrame.size.width, newFrame.size.height);
                } else if (CGRectGetMaxX(viewFrame) > CGRectGetWidth(self.bounds) - 1) {
                    newFrame = CGRectMake(CGRectGetWidth(self.bounds) - 1 - CGRectGetWidth(newFrame), newFrame.origin.y,
                                          newFrame.size.width, newFrame.size.height);
                }
                
                //(y)
                if (CGRectGetMinY(viewFrame) < 1) {
                    newFrame = CGRectMake(newFrame.origin.x, 1.0, newFrame.size.width, newFrame.size.height);
                } else if (CGRectGetMaxY(viewFrame) > CGRectGetHeight(self.bounds) - 1) {
                    newFrame = CGRectMake(newFrame.origin.x, CGRectGetHeight(self.bounds) - 1.0 - CGRectGetHeight(newFrame), newFrame.size.width, newFrame.size.height);
                }
                
            }
            
        }
        
        //Check midline
        CGRect midlineRect = CGRectMake(0.0, self.bounds.size.height/2.0 - .5, self.bounds.size.width, 1.0);
        
        if (CGRectIntersectsRect(midlineRect, newFrame)) {
            
            if (newFrame.origin.y + newFrame.size.height/2.0 < self.bounds.size.height/2.0 - .5) {
                // Adjust it up
                newFrame = CGRectMake(newFrame.origin.x, self.bounds.size.height/2.0 - newFrame.size.height - 1.0, newFrame.size.width, newFrame.size.height);
                
            } else {
                //Adjust it down
                newFrame = CGRectMake(newFrame.origin.x, self.bounds.size.height/2.0, newFrame.size.width, newFrame.size.height);
            }
            
        }
        
        [UIView animateWithDuration:.1
                              delay:0.0
                            options:UIViewAnimationOptionCurveEaseOut
                         animations:^{
                             view.frame = newFrame;
                         }
                         completion:^(BOOL finished) {
                             
                         }];
     
    }
    
}

@end
