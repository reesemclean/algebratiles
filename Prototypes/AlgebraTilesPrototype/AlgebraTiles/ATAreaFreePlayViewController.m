//
//  ATAreaFreePlayViewController.m
//  AlgebraTiles
//
//  Created by Reese McLean on 12/12/12.
//  Copyright (c) 2012 Reese McLean. All rights reserved.
//

#import "ATAreaFreePlayViewController.h"

#import "ATTileView.h"
#import "ATAreaMatView.h"

@interface ATAreaFreePlayViewController ()

@end

@implementation ATAreaFreePlayViewController

+(NSString*)storyboardID {
    return @"ATAreaFreePlayViewController";
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark Tile View Delegate

-(void) didStartTileViewPan:(ATTileView*)tileView {
    
    [super didStartTileViewPan:tileView];
    
}

-(void) didPanTileView:(ATTileView*)tileView withTranslation:(CGPoint)translation {

    [super didPanTileView:tileView withTranslation:translation];
}

-(void) didFinishTileViewPan:(ATTileView*)tileView {
    
    [super didFinishTileViewPan:tileView];
}

@end
