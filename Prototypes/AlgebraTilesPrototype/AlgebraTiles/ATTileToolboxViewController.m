//
//  ATTileToolboxViewController.m
//  AlgebraTiles
//
//  Created by Reese McLean on 12/7/12.
//  Copyright (c) 2012 Reese McLean. All rights reserved.
//

#import "ATTileToolboxViewController.h"

#import "ATTileModel.h"
#import "TileConstants.h"
#import "ATTileToolboxCell.h"
#import "ATTileView.h"

@interface ATTileToolboxViewController ()

@property (nonatomic, strong) NSArray *availableTiles;

@end

@implementation ATTileToolboxViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSArray*)availableTiles {
    
    if (!_availableTiles) {
        _availableTiles = @[ [ATTileModel tileWithType:ATTileTypeUnit],
        [ATTileModel tileWithType:ATTileTypeX],
        [ATTileModel tileWithType:ATTileTypeXSquared],
        [ATTileModel tileWithType:ATTileTypeY],
        [ATTileModel tileWithType:ATTileTypeYSquared],
        [ATTileModel tileWithType:ATTileTypeXY] ];
    }
    
    return _availableTiles;
}

#pragma mark - UICollectionView Datasource
- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section {
        return [self.availableTiles count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)cv cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    ATTileToolboxCell *cell = (ATTileToolboxCell*)[cv dequeueReusableCellWithReuseIdentifier:@"TileToolboxCellID"
                                                                                forIndexPath:indexPath];
    cell.backgroundColor = [UIColor clearColor];
    
    ATTileModel *tileModel = [self.availableTiles objectAtIndex:indexPath.row];
    [cell setTileModel:tileModel];
    
    return cell;
}

#pragma mark - UICollectionViewDelegate
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    // TODO: Select Item
}

#pragma mark – UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout*)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    ATTileModel *tileModel = [self.availableTiles objectAtIndex:indexPath.row];
    
    return CGSizeMake(self.collectionView.frame.size.width, [ATTileToolboxCell cellHeightForTileModel:tileModel]);

}

@end
