//
//  ATViewController.h
//  AlgebraTiles
//
//  Created by Reese McLean on 12/7/12.
//  Copyright (c) 2012 Reese McLean. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ATMenuViewController.h"
#import "ATBaseActivityViewController.h"

extern NSString * const DID_PRESS_MENU_BUTTON;

@interface ATContainerViewController : UIViewController <ATMenuViewControllerDelegate>

@property (weak, nonatomic) IBOutlet UIView *tileToolboxContainer;
@property (weak, nonatomic) IBOutlet UIView *matViewContainer;
@property (weak, nonatomic) IBOutlet UIView *contentContainer;
@property (nonatomic, strong) ATBaseActivityViewController *currentMatViewController;

@property (nonatomic, assign) BOOL tileToolboxOnLeft;

@property (nonatomic, strong) ATMenuViewController *menuViewController;
@property (nonatomic, strong) UIView *blackoutView;

- (IBAction)menuButtonPushed:(id)sender;

@end
