//
//  ATBaseActivityViewController.h
//  AlgebraTiles
//
//  Created by Reese McLean on 12/22/12.
//  Copyright (c) 2012 Reese McLean. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ATActivityModel;

@interface ATBaseActivityViewController : UIViewController

+(NSString*)storyboardID;

@property (nonatomic, strong) ATActivityModel *activityModel;
@property (weak, nonatomic) IBOutlet UIButton *menuButton;

@end
