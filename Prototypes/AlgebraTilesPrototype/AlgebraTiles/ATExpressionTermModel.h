//
//  ATExpressionTermModel.h
//  AlgebraTiles
//
//  Created by Reese McLean on 12/20/12.
//  Copyright (c) 2012 Reese McLean. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "ATTileModel.h"

@interface ATExpressionTermModel : NSObject

-(ATTileType) tileType;
-(BOOL) negative;
-(int) coefficient;

@property (nonatomic, copy) NSSet *tileModels;

@end
