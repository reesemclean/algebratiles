//
//  ATTileGroup.h
//  AlgebraTiles
//
//  Created by Reese McLean on 12/20/12.
//  Copyright (c) 2012 Reese McLean. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "ATTileView.h"
#import "ATExpressionTermModel.h"

@interface ATTileGroupCollectionModel : NSObject

-(id) initWithTiles:(NSArray *)tiles;

@property (nonatomic, strong) NSMutableArray *tiles;
-(ATTileView*) topTile;
-(ATTileView*) bottomTile;

-(void) addTile:(ATTileView*)tile;
-(void) removeTile:(ATTileView*)tile;
-(BOOL) containsTile:(ATTileView*)tile;

-(CGPoint) centerOfGroup;
-(CGRect) rectForGroup;
-(UIView*) superview;

-(ATExpressionTermModel *)termModel;

@end
