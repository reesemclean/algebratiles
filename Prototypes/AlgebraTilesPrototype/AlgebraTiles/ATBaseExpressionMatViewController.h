//
//  ATBaseExpressionMatViewController.h
//  AlgebraTiles
//
//  Created by Reese McLean on 12/23/12.
//  Copyright (c) 2012 Reese McLean. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ATBaseActivityViewController.h"
#import "ATTileView.h"
#import "ATExpressionMatView.h"

@interface ATBaseExpressionMatViewController : ATBaseActivityViewController <ATTileViewDelegate>

@property (weak, nonatomic) IBOutlet ATExpressionMatView *expressionMat;

@end
