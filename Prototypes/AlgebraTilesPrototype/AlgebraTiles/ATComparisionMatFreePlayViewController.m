//
//  ATComparisionMatFreePlayViewController.m
//  AlgebraTiles
//
//  Created by Reese McLean on 12/23/12.
//  Copyright (c) 2012 Reese McLean. All rights reserved.
//

#import "ATComparisionMatFreePlayViewController.h"

#import "ATExpressionMatView.h"
#import "ATExpressionModel.h"

@interface ATComparisionMatFreePlayViewController ()

@end

@implementation ATComparisionMatFreePlayViewController

+(NSString*)storyboardID {
    return @"ATComparisionMatFreePlayViewController";
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) updateExpressionLabels {
    
    ATExpressionModel *leftExpressionModel = [self.leftExpressionMat expressionModel];
    NSString *leftExpressionStringValue = [leftExpressionModel expressionStringValue];
    
    self.leftExpressionLabel.text = leftExpressionStringValue;
    
    ATExpressionModel *rightExpressionModel = [self.rightExpressionMat expressionModel];
    NSString *rightExpressionStringValue = [rightExpressionModel expressionStringValue];
    
    self.rightExpressionLabel.text = rightExpressionStringValue;
    
}

#pragma mark Tile View Delegate

-(void) didStartTileViewPan:(ATTileView*)tileView {
    
    [super didStartTileViewPan:tileView];
}

-(void) didPanTileView:(ATTileView*)tileView withTranslation:(CGPoint)translation {
    [super didPanTileView:tileView withTranslation:translation];
}

-(void) didFinishTileViewPan:(ATTileView*)tileView {
    
    [super didFinishTileViewPan:tileView];
    [self updateExpressionLabels];
}

-(void) didFlipTile:(ATTileView*)tileView {
    
    [super didFlipTile:tileView];
    
    [self updateExpressionLabels];
    
}


@end
