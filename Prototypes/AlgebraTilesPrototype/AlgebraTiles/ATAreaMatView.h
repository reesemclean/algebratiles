//
//  ATAreaMatView.h
//  AlgebraTiles
//
//  Created by Reese McLean on 12/13/12.
//  Copyright (c) 2012 Reese McLean. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ATTileView;

@interface ATAreaMatView : UIView

-(BOOL) tryToClaimTile:(ATTileView*) tileView;
-(void) didPanTile:(ATTileView*)tileView withTranslation:(CGPoint)translation;
-(void) didFinishTilePan:(ATTileView*) tileView;
-(void) didStartPanTile:(ATTileView*)tileView;

@property (nonatomic, assign) BOOL snapsToOutside;
@property (nonatomic, assign) BOOL snapsToInside;

@end
