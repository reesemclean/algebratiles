//
//  ATTileView.m
//  AlgebraTiles
//
//  Created by Reese McLean on 12/7/12.
//  Copyright (c) 2012 Reese McLean. All rights reserved.
//

#import "ATTileView.h"

#import "TileConstants.h"
#import "ATTileModel.h"
#import <QuartzCore/QuartzCore.h>

NSString * const kATDidSpawnTileViewNotification = @"kATDidSpawnTileViewNotification";

@interface ATTileView ()

@property (nonatomic, strong) ATTileView *spawnedView;

@end

@implementation ATTileView

+(id) tileWithTileModel:(ATTileModel*)tileModel {
    
    CGSize size = [ATTileView sizeForTileModel:tileModel];
    CGRect frame = CGRectMake(0, 0, size.width, size.height);
    ATTileView *tileView = [[ATTileView alloc] initWithFrame:frame];
    tileView.tileModel = tileModel;
    tileView.spawnsNewTiles = NO;
    tileView.canFlipToNegative = YES;
    tileView.vertical = NO;
    tileView.backgroundColor = [ATTileView colorForTileModel:tileModel];
    UIPanGestureRecognizer *panGestureRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:tileView action:@selector(handlePanGesture:)];
    [tileView addGestureRecognizer:panGestureRecognizer];
    
    return tileView;
}

-(BOOL) isSameTypeOfTile:(ATTileView*)tileToTest {
    return [tileToTest.tileModel tileType] == [self.tileModel tileType] && [tileToTest.tileModel negative] == [self.tileModel negative];
}

-(void) setSpawnsNewTiles:(BOOL)spawnsNewTiles {
    
    _spawnsNewTiles = spawnsNewTiles;
    
    [self setupFlipGesture];
    
}

-(void) setCanFlipToNegative:(BOOL)canFlipToNegative {
    _canFlipToNegative = canFlipToNegative;
    
    [self setupFlipGesture];
    
}

-(void) setupFlipGesture {
    
    if (!self.spawnsNewTiles && self.canFlipToNegative) {
        
        if (!self.tapGestureRecognizer) {
            self.tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleDoubleTapGesture:)];
            self.tapGestureRecognizer.numberOfTapsRequired = 2;
            [self addGestureRecognizer:self.tapGestureRecognizer];
        }
        
        self.tapGestureRecognizer.enabled = YES;
    } else {
        self.tapGestureRecognizer.enabled = NO;
    }

    
}

- (void)drawRect:(CGRect)rect {
    // get the contect
    CGContextRef context = UIGraphicsGetCurrentContext();
 
    CGContextSetStrokeColorWithColor(context, [[UIColor darkGrayColor] CGColor]);
    CGContextSetLineWidth(context, 1.0);

    CGRect insetRect = CGRectInset(self.bounds, .5, .5);
    CGContextStrokeRect(context, insetRect);
        
}

#pragma mark Shadow Management

+(void) addShadowToTileView:(ATTileView*)tileView {
    
    tileView.layer.shadowOffset = CGSizeMake(0.0, 0.0);
    tileView.layer.shadowColor = [[UIColor darkGrayColor] CGColor];
    tileView.layer.shadowOpacity = .66;
    
    CGPathRef shadowPath = CGPathCreateWithRect(tileView.bounds, NULL);
    
    tileView.layer.shadowPath = shadowPath;
    
    CGPathRelease(shadowPath);
    
    tileView.layer.shadowRadius = 2.0;
    //tileView.layer.shouldRasterize = YES;
    
}

+(void) removeShadowFromTileView:(ATTileView*)tileView {
    
    tileView.layer.shadowRadius = 0.0;
    tileView.layer.shadowColor = [[UIColor clearColor] CGColor];
    tileView.layer.shadowOpacity = 0.0;
    
}

#pragma mark Gesture Handlers

-(void) handleDoubleTapGesture:(UITapGestureRecognizer*)tapGestureRecognizer {
    
    if ([self.delegate respondsToSelector:@selector(didFlipTile:)]) {
        [self.delegate didFlipTile:self];
    }
    
}

- (void)handlePanGesture:(UIPanGestureRecognizer *)thePanGestureRecognizer {
    switch (thePanGestureRecognizer.state) {
        case UIGestureRecognizerStateBegan: {
            
            ATTileView *viewToMove = self;
            
            if (self.spawnsNewTiles) {
                CGPoint spawnedCenter = self.center;
                
                self.spawnedView = [ATTileView tileWithTileModel:self.tileModel];
                viewToMove = self.spawnedView;
                
                [[NSNotificationCenter defaultCenter] postNotificationName:kATDidSpawnTileViewNotification
                                                                    object:viewToMove
                                                                  userInfo:nil];
                
                self.spawnedView.center = [self.superview convertPoint:spawnedCenter toView:self.spawnedView.superview];
                
            }
            
            [viewToMove.delegate didStartTileViewPan:viewToMove];
            
            [ATTileView addShadowToTileView:viewToMove];
            
        }
            break;
        case UIGestureRecognizerStateChanged: {
            
            ATTileView *viewToMove = self.spawnedView ? self.spawnedView : self;
            
            CGPoint theTranslationInCollectionView = [thePanGestureRecognizer translationInView:self.superview];
            
            viewToMove.center = CGPointMake(viewToMove.center.x + theTranslationInCollectionView.x,
                                            viewToMove.center.y + theTranslationInCollectionView.y);
            
            [viewToMove.delegate didPanTileView:viewToMove withTranslation:theTranslationInCollectionView];
            [thePanGestureRecognizer setTranslation:CGPointZero inView:self.superview];
            
        }
            break;
            
        case UIGestureRecognizerStateEnded: {
            
            ATTileView *viewMoved = self;
            
            if (self.spawnedView) {
                
                viewMoved = self.spawnedView;
                self.spawnedView = nil;
            }
            
            [viewMoved.delegate didFinishTileViewPan:viewMoved];
            
            [ATTileView removeShadowFromTileView:viewMoved];
            
        }
            break;
        default:
            break;
    }
}

#pragma Animation Handling

-(void) rotateToVertical:(BOOL)vertical animated:(BOOL)animated {
    
    if (self.vertical == vertical) {
        return; //Aleady correct orientation
    }
    
    if (vertical) {
        
        self.vertical = vertical;
        //Rotate to vertical
        [UIView animateWithDuration:animated ? .1 : 0.0
                              delay:0.0
                            options:UIViewAnimationOptionAllowUserInteraction
                         animations:^{
                             self.transform = CGAffineTransformMakeRotation(M_PI/2.0);
                         }
                         completion:^(BOOL finished) {
                             
                         }];
        
        
    } else {
        
        self.vertical = vertical;
        //Rotate to Horizontal
        [UIView animateWithDuration:animated ? .1 : 0.0
                              delay:0.0
                            options:UIViewAnimationOptionAllowUserInteraction
                         animations:^{
                             self.transform = CGAffineTransformIdentity;
                         }
                         completion:^(BOOL finished) {
                             
                         }];
    }
    
}

-(void) updateTileColorAnimated:(BOOL) animated {
    
    [UIView transitionWithView:self
                      duration:animated ? .2 : 0.0
                       options:UIViewAnimationOptionAllowAnimatedContent |
                                UIViewAnimationCurveEaseIn |
                                UIViewAnimationOptionAllowAnimatedContent |
                                UIViewAnimationOptionTransitionFlipFromBottom
                    animations:^{
                        self.backgroundColor = [ATTileView colorForTileModel:self.tileModel];
                    }
                    completion:^(BOOL finished) {
                        
                    }];
    
}

-(void) removeAnimated:(BOOL)animated completion:(CompletionBlock)block {
    [UIView animateWithDuration:.25
                          delay:0.0
                        options:UIViewAnimationCurveEaseIn
                     animations:^{
                         self.alpha = 0.0;
                     }
                     completion:^(BOOL finished) {
                         [self removeFromSuperview];
                         
                         if (block) {
                             block();
                         }
                     }];

}

+(UIColor*)colorForTileModel:(ATTileModel*)tileModel {
    
    UIColor *color;
    
    if (tileModel.negative) {
        return AT_NEGATIVE_COLOR;
    }
    
    switch (tileModel.tileType) {
        case ATTileTypeUnit:
            color = AT_UNIT_COLOR;
            break;
        case ATTileTypeX:
            color = AT_X_COLOR;
            break;
        case ATTileTypeXSquared:
            color = AT_X_COLOR;
            break;
        case ATTileTypeXY:
            color = AT_XY_COLOR;
            break;
        case ATTileTypeY:
            color = AT_Y_COLOR;
            break;
        case ATTileTypeYSquared:
            color = AT_Y_COLOR;
            break;
        default:
            color = AT_UNIT_COLOR;
            break;
    }
    
    return color;
    
}

#pragma mark Geometry Helpers

-(CGPoint) topLeftPoint {

    CGFloat x;    
    if (self.vertical) {
        x = self.center.x - self.bounds.size.height/2.0;
    } else {
        x = self.center.x - self.bounds.size.width/2.0;
    }
    
    CGFloat y;
    if (self.vertical) {
        y = self.center.y - self.bounds.size.width/2.0;
    } else {
        y = self.center.y - self.bounds.size.height/2.0;
    }
    return CGPointMake(x, y);
}

-(CGPoint) topRightPoint {

    CGFloat x;
    if (self.vertical) {
        x = self.center.x + self.bounds.size.height/2.0;
    } else {
        x = self.center.x + self.bounds.size.width/2.0;
    }
    
    CGFloat y;
    if (self.vertical) {
        y = self.center.y - self.bounds.size.width/2.0;
    } else {
        y = self.center.y - self.bounds.size.height/2.0;
    }
    return CGPointMake(x, y);
    
}

-(CGPoint) bottomLeftPoint {
    CGFloat x;
    if (self.vertical) {
        x = self.center.x - self.bounds.size.height/2.0;
    } else {
        x = self.center.x - self.bounds.size.width/2.0;
    }
    
    CGFloat y;
    if (self.vertical) {
        y = self.center.y + self.bounds.size.width/2.0;
    } else {
        y = self.center.y + self.bounds.size.height/2.0;
    }
    return CGPointMake(x, y);
}

-(CGPoint) bottomRightPoint {
    CGFloat x;
    if (self.vertical) {
        x = self.center.x + self.bounds.size.height/2.0;
    } else {
        x = self.center.x + self.bounds.size.width/2.0;
    }
    
    CGFloat y;
    if (self.vertical) {
        y = self.center.y + self.bounds.size.width/2.0;
    } else {
        y = self.center.y + self.bounds.size.height/2.0;
    }
    return CGPointMake(x, y);
}

-(CGFloat) trueWidth {
    if (self.vertical) {
        return self.bounds.size.height;
    } else {
        return self.bounds.size.width;
    }
}

-(CGFloat) trueHeight {
    if (self.vertical) {
        return self.bounds.size.width;
    } else {
        return self.bounds.size.height;
    }
}

-(NSString*)tileLengthTitleForSide:(CGRectEdge) rectEdge {
    
    switch (self.tileModel.tileType) {
        case ATTileTypeUnit:
            return @"1";
            break;
        case ATTileTypeX:
            
            if (rectEdge == CGRectMinXEdge || rectEdge == CGRectMaxXEdge) {
                if (self.vertical) {
                    return @"x";
                } else {
                    return @"1";
                }
            } else {
                if (self.vertical) {
                    return @"1";
                } else {
                    return @"x";
                }
            }
            
            break;
        case ATTileTypeXSquared:
            return @"x";
            break;
        case ATTileTypeXY:
            if (rectEdge == CGRectMinXEdge || rectEdge == CGRectMaxXEdge) {
                if (self.vertical) {
                    return @"x";
                } else {
                    return @"y";
                }
            } else {
                if (self.vertical) {
                    return @"y";
                } else {
                    return @"x";
                }
            }
            break;
        case ATTileTypeY:
            if (rectEdge == CGRectMinXEdge || rectEdge == CGRectMaxXEdge) {
                if (self.vertical) {
                    return @"y";
                } else {
                    return @"1";
                }
            } else {
                if (self.vertical) {
                    return @"1";
                } else {
                    return @"y";
                }
            }
            break;
        case ATTileTypeYSquared:
            return @"y";
            break;
        default:
            return nil;
            break;
    }
    
}

-(BOOL) isSnapableToOutSideAreaModel {
    switch (self.tileModel.tileType) {
        case ATTileTypeUnit:
            return YES;
            break;
        case ATTileTypeX:
            return YES;
            break;
        case ATTileTypeXSquared:
            return NO;
            break;
        case ATTileTypeXY:
            return NO;
            break;
        case ATTileTypeY:
            return YES;
            break;
        case ATTileTypeYSquared:
            return NO;
            break;
        default:
            return NO;
            break;
    }
}

+(CGSize) sizeForTileModel:(ATTileModel*) tileModel {
    
    CGSize size;

    switch (tileModel.tileType) {
        case ATTileTypeUnit:
            size = CGSizeMake(AT_UNIT_LENGTH, AT_UNIT_LENGTH);
            break;
        case ATTileTypeX:
            size = CGSizeMake(AT_X_LENGTH, AT_UNIT_LENGTH);
            break;
        case ATTileTypeXSquared:
            size = CGSizeMake(AT_X_LENGTH, AT_X_LENGTH);
            break;
        case ATTileTypeXY:
            size = CGSizeMake(AT_Y_LENGTH, AT_X_LENGTH);
            break;
        case ATTileTypeY:
            size = CGSizeMake(AT_Y_LENGTH, AT_UNIT_LENGTH);
            break;
        case ATTileTypeYSquared:
            size = CGSizeMake(AT_Y_LENGTH, AT_Y_LENGTH);
            break;
        default:
            size = CGSizeMake(AT_UNIT_LENGTH, AT_UNIT_LENGTH);
            break;
    }

    return size;
}

@end
