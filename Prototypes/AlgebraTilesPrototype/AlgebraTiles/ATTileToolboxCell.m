//
//  ATTileToolboxCell.m
//  AlgebraTiles
//
//  Created by Reese McLean on 12/7/12.
//  Copyright (c) 2012 Reese McLean. All rights reserved.
//

#import "ATTileToolboxCell.h"

#import "ATTileModel.h"

#define top_bottom_margin 20.0
#define label_height 30.0

@implementation ATTileToolboxCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

-(void) setTileModel:(ATTileModel *)tileModel {
    
    if (_tileModel != tileModel) {
        _tileModel = tileModel;
        
        [self.tileView removeFromSuperview];
        
        self.label.text = tileModel.tileTitle;

        self.tileView = [ATTileView tileWithTileModel:tileModel];
        self.tileView.frame = CGRectMake(0.0, top_bottom_margin, self.tileView.frame.size.width, self.tileView.frame.size.height);
        self.tileView.center = CGPointMake(self.frame.size.width/2.0, self.tileView.center.y);
        self.tileView.spawnsNewTiles = YES;
        [self.contentView addSubview:self.tileView];
        
        self.label.frame = CGRectMake(0.0, top_bottom_margin * 2.0 + self.tileView.frame.size.height, self.frame.size.width, label_height);

    }
    
}

+(CGFloat) cellHeightForTileModel:(ATTileModel*)tileModel {
    
    return top_bottom_margin + [ATTileView sizeForTileModel:tileModel].height + top_bottom_margin + label_height + top_bottom_margin;
    
}

@end
