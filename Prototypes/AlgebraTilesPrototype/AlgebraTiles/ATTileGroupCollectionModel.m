//
//  ATTileGroup.m
//  AlgebraTiles
//
//  Created by Reese McLean on 12/20/12.
//  Copyright (c) 2012 Reese McLean. All rights reserved.
//

#import "ATTileGroupCollectionModel.h"

@implementation ATTileGroupCollectionModel

-(id) initWithTiles:(NSArray *)tiles {
    self = [super init];
    if (self) {
        _tiles = [NSMutableArray arrayWithArray:tiles];
    }
    return self;
}

-(ATTileView*)bottomTile {
    if ([self.tiles count] < 1) {
        return nil;
    }
    return [self.tiles objectAtIndex:0];
}

-(ATTileView*) topTile {
    return [self.tiles lastObject];
}

-(void) addTile:(ATTileView *)tile {
    [self.tiles addObject:tile];
}

-(void) removeTile:(ATTileView*)tile {
    [self.tiles removeObject:tile];
}

-(BOOL) containsTile:(ATTileView*)tile {
    return [self.tiles containsObject:tile];
}

-(UIView *) superview {
    return [[self topTile] superview];
}

-(CGPoint) centerOfGroup {
    
    if ([self.tiles count] < 1) {
        return CGPointZero;
    }
    
    ATTileView *bottomTile = [self.tiles objectAtIndex:0];
    ATTileView *topTile = [self.tiles lastObject];
    
    return CGPointMake((bottomTile.center.x + topTile.center.x)/2.0, topTile.center.y);
    
}

-(CGRect) rectForGroup {
    
    if ([self.tiles count] < 1) {
        return CGRectZero;
    }
    
    ATTileView *bottomTile = [self.tiles objectAtIndex:0];
    ATTileView *topTile = [self.tiles lastObject];
    
    return CGRectMake([bottomTile topLeftPoint].x, [bottomTile topRightPoint].y, [topTile topRightPoint].x - [bottomTile topLeftPoint].x, [topTile bottomRightPoint].y - [bottomTile topLeftPoint].y);
    
}

-(ATExpressionTermModel*)termModel {
    
    ATExpressionTermModel *termModel = [[ATExpressionTermModel alloc] init];
    
    NSMutableSet *tileModels = [NSMutableSet set];
    for (ATTileView *tileView in self.tiles) {
        [tileModels addObject:tileView.tileModel];
    }
    
    termModel.tileModels = tileModels;
    return termModel;
}

@end
