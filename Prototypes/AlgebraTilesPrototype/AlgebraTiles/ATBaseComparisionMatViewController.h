//
//  ATBaseComparisionMatViewController.h
//  AlgebraTiles
//
//  Created by Reese McLean on 12/23/12.
//  Copyright (c) 2012 Reese McLean. All rights reserved.
//

#import "ATBaseActivityViewController.h"

#import "ATTileView.h"
#import "ATExpressionMatView.h"

@interface ATBaseComparisionMatViewController : ATBaseActivityViewController <ATTileViewDelegate>

@property (weak, nonatomic) IBOutlet ATExpressionMatView *leftExpressionMat;
@property (weak, nonatomic) IBOutlet ATExpressionMatView *rightExpressionMat;

@end
