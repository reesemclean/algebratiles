//
//  ATActivityModel.m
//  AlgebraTiles
//
//  Created by Reese McLean on 12/22/12.
//  Copyright (c) 2012 Reese McLean. All rights reserved.
//

#import "ATActivityModel.h"

@implementation ATActivityModel

-(id) initWithMenuName:(NSString*) menuName andOptions:(NSDictionary*)options andStoryboardID:(NSString*) storyboardID andActivityID:(NSString*)activityID {
    self = [super init];
    if (self) {
        _menuName = menuName;
        _options = options;
        _storyboardID = storyboardID;
        _activityID = activityID;
    }
    return self;
}

+(id)activityModelWithMenuName:(NSString*) menuName
                    andOptions:(NSDictionary*)options
               andStoryboardID:(NSString*) storyboardID
                 andActivityID:(NSString*)activityID {
    return [[ATActivityModel alloc] initWithMenuName:menuName
                                          andOptions:options
                                     andStoryboardID:storyboardID
                                       andActivityID:activityID];
}

@end
