//
//  ATMenuViewController.m
//  AlgebraTiles
//
//  Created by Reese McLean on 12/8/12.
//  Copyright (c) 2012 Reese McLean. All rights reserved.
//

#import "ATMenuViewController.h"

#import "ATActivityDataController.h"
#import "ATActivityModel.h"

@interface ATMenuViewController ()

@end

@implementation ATMenuViewController

+(CGFloat)menuWidth {
    return 320.0;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)toggleToolboxSidePressed:(id)sender {
    
    if ([self.delegate respondsToSelector:@selector(shouldToggleToolboxSide:)]) {
        [self.delegate shouldToggleToolboxSide:self];
    }
    
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return [[ATActivityDataController sharedController] numberOfSections] + 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    if (section < [[ATActivityDataController sharedController] numberOfSections]) {
        return [[ATActivityDataController sharedController] numberOfRowsInSection:section];
    }
    
    //Settings
    return 1;
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    
    if (section < [[ATActivityDataController sharedController] numberOfSections]) {
        return [[ATActivityDataController sharedController] titleForSection:section];
    }

    return NSLocalizedString(@"Settings",nil);

}

-(CGFloat) tableView:(UITableView *)aTableView heightForHeaderInSection:(NSInteger)section {
    return 26.0;
}

-(UIView *) tableView:(UITableView *)aTableView viewForHeaderInSection:(NSInteger)section {
    
    UIView *containerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, [self tableView:aTableView heightForHeaderInSection:section])];
    //containerView.alpha = 1.0;
    containerView.backgroundColor = [UIColor colorWithRed:244.0/255.0
                                                    green:244.0/255.0
                                                     blue:244.0/255.0
                                                    alpha:.9];
    
    CGFloat headerPadding = 10.0;
    CGFloat yPadding = 6.0;
    
    UILabel *headerLabel = [[UILabel alloc] initWithFrame:CGRectMake(headerPadding, yPadding, CGRectGetWidth(self.view.bounds) - 2 * headerPadding, CGRectGetHeight(containerView.frame) - 2 * yPadding)];
    headerLabel.text = [self tableView:aTableView titleForHeaderInSection:section];
    headerLabel.font = [UIFont fontWithName:@"AvenirNext-DemiBold" size:14.0];
    headerLabel.textColor = [UIColor blackColor];
    //headerLabel.textAlignment = NSTextAlignmentCenter;
    //headerLabel.shadowColor = [UIColor whiteColor];
    //headerLabel.shadowOffset = CGSizeMake(0.0, -1.0);
    //headerLabel.adjustsFontSizeToFitWidth = YES;
    headerLabel.backgroundColor = [UIColor clearColor];
    
    [containerView addSubview:headerLabel];
    
    return containerView;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    // Configure the cell...
    
    if (indexPath.section < [[ATActivityDataController sharedController] numberOfSections]) {

        ATActivityModel *activity = [[ATActivityDataController sharedController] activityModelForIndexPath:indexPath];
        cell.textLabel.text = activity.menuName;
        
    } else {
        cell.textLabel.text = @"Toggle Toolbox Side";
    }

    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    if (indexPath.section < [[ATActivityDataController sharedController] numberOfSections]) {
        
        ATActivityModel *activity = [[ATActivityDataController sharedController] activityModelForIndexPath:indexPath];
        [self.delegate didSelectActivityModel:activity fromMenu:self];

    } else {
        [self toggleToolboxSidePressed:nil];
    }
    
}


@end
