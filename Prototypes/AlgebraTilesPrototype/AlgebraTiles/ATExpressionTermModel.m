//
//  ATExpressionTermModel.m
//  AlgebraTiles
//
//  Created by Reese McLean on 12/20/12.
//  Copyright (c) 2012 Reese McLean. All rights reserved.
//

#import "ATExpressionTermModel.h"

@implementation ATExpressionTermModel

-(id) init {
    self = [super init];
    if (self) {
        self.tileModels = [NSSet set];
    }
    return self;
}

-(ATTileType) tileType {
    return [[self.tileModels anyObject] tileType];
}

-(BOOL) negative {
    return [[self.tileModels anyObject] negative];
}

-(int) coefficient {
    return [self.tileModels count];
}

@end
