//
//  ATViewController.m
//  AlgebraTiles
//
//  Created by Reese McLean on 12/7/12.
//  Copyright (c) 2012 Reese McLean. All rights reserved.
//

#import "ATContainerViewController.h"

#import <QuartzCore/QuartzCore.h>

NSString * const DID_PRESS_MENU_BUTTON = @"DID_PRESS_MENU_BUTTON";
NSString * const TILE_TOOLBOX_ON_LEFT = @"TILE_TOOLBOX_ON_LEFT";

#define kMenuAnimationSpeed .33

#import "ATActivityModel.h"
#import "ATActivityDataController.h"

@interface ATContainerViewController ()

@end

@implementation ATContainerViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
        
    NSDictionary *defaults = @{TILE_TOOLBOX_ON_LEFT : @YES };
    
    [[NSUserDefaults standardUserDefaults] registerDefaults:defaults];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    BOOL positionToolboxOnLeft = [[NSUserDefaults standardUserDefaults] boolForKey:TILE_TOOLBOX_ON_LEFT];
    [self positionToolboxOnLeft:positionToolboxOnLeft animated:NO];

    [self showLastUsedMatVC];

}

-(void) positionToolboxOnLeft:(BOOL) onLeft animated:(BOOL)animated {
    
    CGRect toolboxRect;
    CGRect matViewRect;
    
    [self.contentContainer sendSubviewToBack:self.matViewContainer];
    
    if (onLeft) {
        toolboxRect = CGRectMake(0.0, 0.0, self.tileToolboxContainer.frame.size.width, self.tileToolboxContainer.frame.size.height);
        matViewRect = CGRectMake(self.tileToolboxContainer.frame.size.width, 0.0, self.matViewContainer.frame.size.width, self.matViewContainer.frame.size.height);
    } else {
        toolboxRect = CGRectMake(self.matViewContainer.frame.size.width, 0.0, self.tileToolboxContainer.frame.size.width, self.tileToolboxContainer.frame.size.height);
        matViewRect = CGRectMake(0, 0, self.matViewContainer.frame.size.width, self.matViewContainer.frame.size.height);
    }
    
    [UIView animateWithDuration:animated ? .5 : 0.0
                          delay:0.0
                        options:UIViewAnimationCurveEaseInOut
                     animations:^{
                         self.tileToolboxContainer.frame = toolboxRect;
                         self.matViewContainer.frame = matViewRect;
                     }
                     completion:^(BOOL finished) {
                         [self.contentContainer sendSubviewToBack:self.tileToolboxContainer];
                     }];
    
}

-(void) toggleToolboxPositionAnimated:(BOOL)animated {
    
    BOOL currentStatus = [[NSUserDefaults standardUserDefaults] boolForKey:TILE_TOOLBOX_ON_LEFT];
    BOOL newStatus = !currentStatus;
    [[NSUserDefaults standardUserDefaults] setBool:newStatus forKey:TILE_TOOLBOX_ON_LEFT];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [self positionToolboxOnLeft:newStatus animated:animated];
    
}

- (void)menuButtonPushed:(id)sender {

    [self showMenuAnimated:YES];
    
}

-(void) showMenuAnimated:(BOOL) animated {
    
    ATMenuViewController *menuViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"MenuViewControllerID"];
    [self addChildViewController:menuViewController];
    
    CGFloat menuWidth = [ATMenuViewController menuWidth];
    
    menuViewController.view.frame = CGRectMake(-menuWidth, 0.0, menuWidth, self.view.bounds.size.height);
    [self.view insertSubview:menuViewController.view aboveSubview:self.contentContainer];
    
    menuViewController.view.layer.shadowRadius = 4.0;
    menuViewController.view.layer.shadowColor = [[UIColor blackColor] CGColor];
    menuViewController.view.layer.shadowOpacity = 1.0;
    
    CGPathRef shadowPath = CGPathCreateWithRect(menuViewController.view.bounds, NULL);
    
    menuViewController.view.layer.shadowPath = shadowPath;
    
    CGPathRelease(shadowPath);

    self.menuViewController = menuViewController;
    self.menuViewController.delegate = self;
    
    self.blackoutView = [[UIView alloc] initWithFrame:self.contentContainer.bounds];
    self.blackoutView.alpha = 0.0;
    self.blackoutView.backgroundColor = [UIColor blackColor];
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapInBlackoutView:)];
    [self.blackoutView addGestureRecognizer:tapGesture];
    
    [self.contentContainer addSubview:self.blackoutView];
        
    [UIView animateWithDuration:animated ? kMenuAnimationSpeed : 0.0
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         self.menuViewController.view.frame = CGRectMake(0.0, 0.0, self.menuViewController.view.frame.size.width, self.menuViewController.view.frame.size.height);
                        
                         self.blackoutView.alpha = .25;
                     }
                     completion:^(BOOL finished) {
                         [self.menuViewController didMoveToParentViewController:self];
                     }];

}

-(void) hideMenuAnimated:(BOOL)animated {
    
    [self.menuViewController willMoveToParentViewController:nil];
    [UIView animateWithDuration:animated ? kMenuAnimationSpeed : 0.0
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         self.menuViewController.view.frame = CGRectMake(-self.menuViewController.view.frame.size.width, 0.0, self.menuViewController.view.frame.size.width, self.menuViewController.view.frame.size.height);
                         self.blackoutView.alpha = 0.0;
                     }
                     completion:^(BOOL finished) {
                         [self.menuViewController.view removeFromSuperview];
                         [self.menuViewController removeFromParentViewController];
                         self.menuViewController = nil;
                         [self.blackoutView removeFromSuperview];
                         self.blackoutView = nil;
                     }];
    
}

-(void) handleTapInBlackoutView:(UITapGestureRecognizer*)tapGestureRecognizer {
    [self hideMenuAnimated:YES];
}

-(void) transitionToMatViewController:(ATBaseActivityViewController*) newMatViewController animated:(BOOL)animated {
    
    newMatViewController.view.frame = self.matViewContainer.bounds;
    
    [self addChildViewController:newMatViewController];
    [self.currentMatViewController willMoveToParentViewController:nil];
    
    [UIView transitionWithView:self.matViewContainer
                      duration:animated ? kMenuAnimationSpeed : 0.0
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^{
                        
                        [self.matViewContainer addSubview:newMatViewController.view];
                        [self.currentMatViewController.view removeFromSuperview];
                        
                    }
                    completion:^(BOOL finished) {
                        [self.currentMatViewController removeFromParentViewController];
                        [newMatViewController didMoveToParentViewController:self];
                        self.currentMatViewController = newMatViewController;
                    }];
    
    
}

-(void) showLastUsedMatVC {
        
    ATBaseActivityViewController *vc = [[ATActivityDataController sharedController] lastPlayedActivityViewControllerForStoryboard:self.storyboard];

    [self transitionToMatViewController:vc animated:NO];
}

#pragma mark Menu View Delgate

-(void) shouldToggleToolboxSide:(ATMenuViewController*) menuViewController {
    
    [self toggleToolboxPositionAnimated:YES];
    
}

-(void) didSelectActivityModel:(ATActivityModel*)activity fromMenu:(ATMenuViewController*)menuViewController {
    
    if ([self.currentMatViewController.activityModel.activityID isEqualToString:activity.activityID]) {
        return;
    }

    ATBaseActivityViewController *vc = [[ATActivityDataController sharedController] viewControllerForActivty:activity
                                                                                   forStoryboard:self.storyboard];
    [self transitionToMatViewController:vc animated:YES];
    [self hideMenuAnimated:YES];
    [[ATActivityDataController sharedController] didStartActivity:activity];
    
}

@end
