//
//  ATBaseComparisionMatViewController.m
//  AlgebraTiles
//
//  Created by Reese McLean on 12/23/12.
//  Copyright (c) 2012 Reese McLean. All rights reserved.
//

#import "ATBaseComparisionMatViewController.h"
#import "ATTileGroupCollectionModel.h"

@interface ATBaseComparisionMatViewController ()

@end

@implementation ATBaseComparisionMatViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self setupTileNotifications];
}

-(void) viewWillDisappear:(BOOL)animated {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [super viewWillDisappear:animated];
}

-(void) setupTileNotifications {
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didSpawnTileNotification:)
                                                 name:kATDidSpawnTileViewNotification
                                               object:nil];
    
}

-(void) didSpawnTileNotification:(NSNotification*)notification {
    
    ATTileView *spawnedView = (ATTileView*)[notification object];
    spawnedView.delegate = self;
    
    [self.view addSubview:spawnedView];
    [self.view bringSubviewToFront:spawnedView];
    
}

#pragma mark Tile View Delegate

-(void) didStartTileViewPan:(ATTileView*)tileView {
    
    ATTileGroupCollectionModel *group = nil;
    
    if ([self.leftExpressionMat ownsTile:tileView]) {
        group = [self.leftExpressionMat groupForTile:tileView];
    } else if ([self.rightExpressionMat ownsTile:tileView]) {
        group = [self.rightExpressionMat groupForTile:tileView];
    }
    
    if (group) {
        
        for (ATTileView *tile in group.tiles) {
            
            CGPoint convertedCenter = [[tile superview] convertPoint:tile.center toView:self.view];
            tile.center = convertedCenter;
            [self.view addSubview:tile];
            
        }
        
    } else {
        CGPoint convertedCenter = [[tileView superview] convertPoint:tileView.center toView:self.view];
        tileView.center = convertedCenter;
        [self.view addSubview:tileView];
    }
    
    
}

-(void) didPanTileView:(ATTileView*)tileView withTranslation:(CGPoint)translation {
    
    [self.leftExpressionMat didPanTile:tileView withTranslation:translation];
    [self.rightExpressionMat didPanTile:tileView withTranslation:translation];
}

-(void) didFinishTileViewPan:(ATTileView*)tileView {
    
    ATTileGroupCollectionModel *group = nil;
    
    if ([self.leftExpressionMat ownsTile:tileView]) {
        group = [self.leftExpressionMat groupForTile:tileView];
    } else if ([self.rightExpressionMat ownsTile:tileView]) {
        group = [self.rightExpressionMat groupForTile:tileView];
    }
    
    if (![self.leftExpressionMat tryToClaimTile:tileView inGroup:group] && ![self.rightExpressionMat tryToClaimTile:tileView inGroup:group]) {
        
        if ([self.leftExpressionMat ownsTile:tileView]) {
            [self.leftExpressionMat removeGroupForTile:tileView animated:YES];
        } else if ([self.rightExpressionMat ownsTile:tileView]) {
            [self.rightExpressionMat removeGroupForTile:tileView animated:YES];
        } else {
            [tileView removeAnimated:YES completion:nil];
        }
    }
    
    [self.leftExpressionMat cleanupTilesWithDroppedTileView:tileView
                                                   andGroup:group];
    [self.rightExpressionMat cleanupTilesWithDroppedTileView:tileView
                                                    andGroup:group];
}

-(void) didFlipTile:(ATTileView*)tileView {
    
    [self.leftExpressionMat didFlipTile:tileView];
    [self.rightExpressionMat didFlipTile:tileView];
}


@end
