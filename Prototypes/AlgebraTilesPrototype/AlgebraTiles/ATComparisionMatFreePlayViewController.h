//
//  ATComparisionMatFreePlayViewController.h
//  AlgebraTiles
//
//  Created by Reese McLean on 12/23/12.
//  Copyright (c) 2012 Reese McLean. All rights reserved.
//

#import "ATBaseComparisionMatViewController.h"

@interface ATComparisionMatFreePlayViewController : ATBaseComparisionMatViewController

@property (weak, nonatomic) IBOutlet UILabel *leftExpressionLabel;
@property (weak, nonatomic) IBOutlet UILabel *rightExpressionLabel;

@end
