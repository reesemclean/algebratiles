//
//  ATExpressionGuidedSimplifyingViewController.m
//  AlgebraTiles
//
//  Created by Reese McLean on 12/21/12.
//  Copyright (c) 2012 Reese McLean. All rights reserved.
//

#import "ATExpressionMatGuidedViewController.h"

@interface ATExpressionMatGuidedViewController ()

@end

@implementation ATExpressionMatGuidedViewController

+(NSString*)storyboardID {
    return @"ATExpressionMatGuidedViewController";
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark Tile View Delegate

-(void) didStartTileViewPan:(ATTileView*)tileView {
    
    [super didStartTileViewPan:tileView];
}

-(void) didPanTileView:(ATTileView*)tileView withTranslation:(CGPoint)translation {
    [super didPanTileView:tileView withTranslation:translation];
}

-(void) didFinishTileViewPan:(ATTileView*)tileView {
    
    [super didFinishTileViewPan:tileView];

}

-(void) didFlipTile:(ATTileView*)tileView {
    
    [super didFlipTile:tileView];
        
}


@end
