//
//  ATActivityDataController.m
//  AlgebraTiles
//
//  Created by Reese McLean on 12/22/12.
//  Copyright (c) 2012 Reese McLean. All rights reserved.
//

#import "ATActivityDataController.h"

#import "ATExpressionMatFreePlayViewController.h"
#import "ATExpressionMatGuidedViewController.h"
#import "ATAreaFreePlayViewController.h"
#import "ATComparisionMatFreePlayViewController.h"

NSString * const AT_LAST_USED_ACTIVITY_ID = @"AT_LAST_USED_ACTIVITY_ID";

@interface ATActivityDataController ()

@property (nonatomic, strong) NSArray *sections;

@end

@implementation ATActivityDataController

+(id) sharedController {
    static ATActivityDataController *sharedSingleton;
    
    @synchronized(self)
    {
        if (!sharedSingleton)
            sharedSingleton = [[ATActivityDataController alloc] init];
        
        return sharedSingleton;
    }
}

-(id) init {
    self = [super init];
    if (self) {
        [self setupData];
    }
    return self;
}

-(void) setupData {
    
    ATActivityModel *freePlayExpressionMat = [ATActivityModel activityModelWithMenuName:@"Expression Mat"
                                                                             andOptions:nil
                                                                        andStoryboardID:[ATExpressionMatFreePlayViewController storyboardID]
                                                                        andActivityID:@"kFreePlayExpressionMatID"];
    
    ATActivityModel *freePlayComparisionMat = [ATActivityModel activityModelWithMenuName:@"Comparision Mat"
                                                                              andOptions:nil
                                                                         andStoryboardID:[ATComparisionMatFreePlayViewController storyboardID]
                                                                        andActivityID:@"kFreePlayComparisionMatID"];
    
    ATActivityModel *freePlayAreaMat = [ATActivityModel activityModelWithMenuName:@"Area Mat"
                                                                             andOptions:nil
                                                                        andStoryboardID:[ATAreaFreePlayViewController storyboardID]
                                                                    andActivityID:@"kFreePlayAreaMatID"];
    

    
    NSArray *sectionOneActivities = @[freePlayExpressionMat, freePlayComparisionMat, freePlayAreaMat];
    NSDictionary *sectionOne = @{ @"title" : @"Free Play", @"activities" : sectionOneActivities};
    
    //Section 2: Guided Activities
    ATActivityModel *guidedSimplifyExpression = [ATActivityModel activityModelWithMenuName:@"Simplify Expressions"
                                                                                andOptions:nil
                                                                           andStoryboardID:[ATExpressionMatGuidedViewController storyboardID]
                                                                             andActivityID:@"kGuidedSimplifyExpressionID"];
;
    NSArray *sectionTwoActivities = @[guidedSimplifyExpression];
    NSDictionary *sectionTwo = @{ @"title" : @"Guided Practice", @"activities" : sectionTwoActivities};

    self.sections = @[sectionOne, sectionTwo];
    
}

-(ATBaseActivityViewController*) lastPlayedActivityViewControllerForStoryboard:(UIStoryboard*)storyboard {
    ATActivityModel *activityModel = [self lastPlayedActivity];
    ATBaseActivityViewController *vc = [storyboard instantiateViewControllerWithIdentifier:activityModel.storyboardID];
    vc.activityModel = activityModel;
    return vc;
}

-(ATActivityModel *)lastPlayedActivity {
    
    NSDictionary *defaults = @{AT_LAST_USED_ACTIVITY_ID : @"kFreePlayExpressionMatID" };
    
    [[NSUserDefaults standardUserDefaults] registerDefaults:defaults];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    NSString *lastUsedActivityID = [[NSUserDefaults standardUserDefaults] stringForKey:AT_LAST_USED_ACTIVITY_ID];
    
    return [self activityForActivityID:lastUsedActivityID];
    
}

-(void) didStartActivity:(ATActivityModel*) activityModel {
    
    [[NSUserDefaults standardUserDefaults] setObject:activityModel.activityID forKey:AT_LAST_USED_ACTIVITY_ID];
    
}

-(ATBaseActivityViewController*)viewControllerForActivty:(ATActivityModel*)activity
                                forStoryboard:(UIStoryboard*)storyboard {
    
    ATBaseActivityViewController *vc = [storyboard instantiateViewControllerWithIdentifier:activity.storyboardID];
    vc.activityModel = activity;
    return vc;
    
}

-(ATActivityModel*)activityForActivityID:(NSString*)activityID {
    
    __block ATActivityModel *foundModel = nil;
    
    [self.sections enumerateObjectsUsingBlock:^(NSDictionary* section, NSUInteger idx, BOOL *stop) {
       
        NSArray *activities = section[@"activities"];
        [activities enumerateObjectsUsingBlock:^(ATActivityModel* activityModel, NSUInteger idx, BOOL *stop) {
           
            if ([activityModel.activityID isEqualToString:activityID]) {
                foundModel = activityModel;
                *stop = YES;
            }
            
        }];
        
    }];
    
    return foundModel;
    
}

-(NSUInteger)numberOfSections {
    return [self.sections count];
}

-(NSUInteger)numberOfRowsInSection:(NSUInteger)sectionIndex {
    NSDictionary *section = [self.sections objectAtIndex:sectionIndex];
    NSArray *activities = section[@"activities"];
    return [activities count];
}

-(NSString*)titleForSection:(NSUInteger)sectionIndex {
    NSDictionary *section = [self.sections objectAtIndex:sectionIndex];
    return section[@"title"];
}

-(ATActivityModel*)activityModelForIndexPath:(NSIndexPath*)indexPath {
    NSDictionary *section = [self.sections objectAtIndex:indexPath.section];
    NSArray *activities = section[@"activities"];
    return [activities objectAtIndex:indexPath.row];
}

@end
