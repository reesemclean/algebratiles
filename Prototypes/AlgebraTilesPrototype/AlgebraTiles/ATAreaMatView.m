//
//  ATAreaMatView.m
//  AlgebraTiles
//
//  Created by Reese McLean on 12/13/12.
//  Copyright (c) 2012 Reese McLean. All rights reserved.
//

#import "ATAreaMatView.h"

#import "TileConstants.h"
#import "ATTileView.h"
#import "ATTileLengthLabel.h"

@interface ATAreaMatView ()

@property (nonatomic, strong) NSMutableSet *pannedTiles;
@property (nonatomic, strong) NSMutableSet *leftTiles;
@property (nonatomic, strong) NSMutableSet *bottomTiles;
@property (nonatomic, strong) NSMutableSet *insideTiles;
@property (nonatomic, strong) NSMutableSet *lengthLabels;

@end

@implementation ATAreaMatView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self commonSetup];
    }
    return self;
}

-(id) initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self commonSetup];
    }
    
    return self;
}

-(void) commonSetup {
    self.backgroundColor = [UIColor whiteColor];
    self.pannedTiles = [NSMutableSet set];
    self.leftTiles = [NSMutableSet set];
    self.bottomTiles = [NSMutableSet set];
    self.insideTiles = [NSMutableSet set];
    self.lengthLabels = [NSMutableSet set];
}

#define OUTSIDE_TILESPACE 80.0

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetStrokeColorWithColor(context, [[UIColor lightGrayColor] CGColor]);
    CGContextSetLineWidth(context, 1.0);
    
    CGRect insetRect = CGRectInset(self.bounds, .5, .5);
    CGContextStrokeRect(context, insetRect);
    
    CGContextSetStrokeColorWithColor(context, [[UIColor darkGrayColor] CGColor]);
    
    CGMutablePathRef verticalPath = CGPathCreateMutable();
    CGPathMoveToPoint(verticalPath, NULL, OUTSIDE_TILESPACE + .5, .5);
    CGPathAddLineToPoint(verticalPath, NULL, OUTSIDE_TILESPACE + .5, self.bounds.size.height - .5);
    CGContextAddPath(context, verticalPath);
    CGContextStrokePath(context);
    CGPathRelease(verticalPath);
    
    CGMutablePathRef horizontalPath = CGPathCreateMutable();
    CGPathMoveToPoint(horizontalPath, NULL, .5, self.bounds.size.height - OUTSIDE_TILESPACE - .5);
    CGPathAddLineToPoint(horizontalPath, NULL, self.bounds.size.width - .5, self.bounds.size.height - OUTSIDE_TILESPACE - .5);
    CGContextAddPath(context, horizontalPath);
    CGContextStrokePath(context);
    CGPathRelease(horizontalPath);
    
    CGContextSetFillColorWithColor(context, [[UIColor whiteColor] CGColor]);
    
    //CGRect insideRect = CGRectMake(OUTSIDE_TILESPACE + 1.0, 1.0, self.bounds.size.width - OUTSIDE_TILESPACE - 2.0, self.bounds.size.height - 2.0 - OUTSIDE_TILESPACE);
    //CGContextFillRect(context, insideRect);
    
}

-(BOOL) tryToClaimTile:(ATTileView*) tileView {
    
    CGPoint convertedCenter = [tileView.superview convertPoint:tileView.center toView:self];
    
    if (CGRectContainsPoint(self.bounds, convertedCenter)) {
        //Keep on mat
        tileView.center = convertedCenter;
        [self addSubview:tileView];
        
        return YES;
        
    }
    
    [self.pannedTiles removeObject:tileView];
    [self.leftTiles removeObject:tileView];
    [self.bottomTiles removeObject:tileView];
    [self.insideTiles removeObject:tileView];
    
    return NO;
}

-(void) didStartPanTile:(ATTileView*)tileView {
    
    [self.pannedTiles addObject:tileView];
    [self.leftTiles removeObject:tileView];
    [self.bottomTiles removeObject:tileView];
    [self.insideTiles removeObject:tileView];
    
    //[self snapTilesToOutsideGrid];
    [self adjustOutsideLabels];
    
}

-(void) didPanTile:(ATTileView*)tileView withTranslation:(CGPoint)translation {
    
    CGPoint convertedCenter = [tileView.superview convertPoint:tileView.center toView:self];
    
    if (convertedCenter.x < AT_UNIT_LENGTH && convertedCenter.x > 0.0) {
        [tileView rotateToVertical:YES animated:YES];
    } else if (convertedCenter.y > self.bounds.size.height - OUTSIDE_TILESPACE && convertedCenter.y < self.bounds.size.height) {
        [tileView rotateToVertical:NO animated:YES];
    }
    
    [self snapTilesToOutsideGrid];
    
}

-(void) didFinishTilePan:(ATTileView*) tileView {
    
    BOOL stillOnMat = [self.pannedTiles containsObject:tileView];
    
    [self.pannedTiles removeObject:tileView];
    
    CGPoint convertedCenter = [tileView.superview convertPoint:tileView.center toView:self];
    
    if (convertedCenter.x < OUTSIDE_TILESPACE && stillOnMat && [tileView isSnapableToOutSideAreaModel]) {
        
        //On Left
        [self.leftTiles addObject:tileView];
        [self adjustOutsideLabels];
        
    } else if (convertedCenter.y > self.bounds.size.height - OUTSIDE_TILESPACE && stillOnMat && [tileView isSnapableToOutSideAreaModel]) {
        
        //On Bottom
        [self.bottomTiles addObject:tileView];
        [self adjustOutsideLabels];
        
    } else if (convertedCenter.x >= AT_UNIT_LENGTH && convertedCenter.y <= self.bounds.size.height - OUTSIDE_TILESPACE && stillOnMat) {
        
        //On Inside
        [self.insideTiles addObject:tileView];
        [self snapTileToInside:tileView];
        
    }
    
    [self snapTilesToOutsideGrid];
    
}

CGFloat distanceSquaredBetweenPoints(CGPoint p1, CGPoint p2) {
    return (p1.x - p2.x)*(p1.x - p2.x) + (p1.y - p2.y)*(p1.y - p2.y);
}

-(void) snapTileToInside:(ATTileView*)tileView {
    
    CGFloat distanceSquaredThreshold = (AT_UNIT_LENGTH/2.0)*(AT_UNIT_LENGTH/2.0);
    
    NSArray *leftTilesOrderedByY = [self tilesCurrentlyOnLeftOrderedByY];
    NSArray *bottomTilesOrderedByX = [self tilesCurrentlyOnBottomOrderedByX];
    
    __block CGPoint foundGridPoint = CGPointZero;
    __block BOOL found = NO;
    int outerIndex = 0;
    
    CGFloat xValueToTest = tileView.center.x - [tileView trueWidth]/2.0;
    CGFloat yValueToTest = tileView.center.y + [tileView trueHeight]/2.0;
    CGPoint pointToTestAgainst = CGPointMake(xValueToTest, yValueToTest);
    
    while (!found && outerIndex < [leftTilesOrderedByY count]) {
        
        ATTileView *leftTile = [leftTilesOrderedByY objectAtIndex:outerIndex];
        CGFloat gridPointY = leftTile.center.y + leftTile.bounds.size.width/2.0;
        int innerIndex = 0;
        
        while (!found && innerIndex < [bottomTilesOrderedByX count]) {
            
            ATTileView *bottomTile = [bottomTilesOrderedByX objectAtIndex:innerIndex];
            CGPoint gridPoint = CGPointMake(bottomTile.frame.origin.x, gridPointY);
            CGFloat distanceSquared = distanceSquaredBetweenPoints(gridPoint, pointToTestAgainst);
            if (distanceSquared <= distanceSquaredThreshold) {
                found = YES;
                foundGridPoint = gridPoint;
            } else {
                innerIndex++;
            }
            
        }
        
        outerIndex++;
    }
    
    if (!found) {
        
        //Next try to align to origin
        CGPoint originPoint = CGPointMake(OUTSIDE_TILESPACE, self.bounds.size.height - OUTSIDE_TILESPACE);
        CGFloat distanceSquared = distanceSquaredBetweenPoints(originPoint, pointToTestAgainst);
        if (distanceSquared <= distanceSquaredThreshold) {
            found = YES;
            foundGridPoint = originPoint;
        }
        
    }
    
    __block CGFloat smallestDistanceFound = CGFLOAT_MAX;
    
    if (!found) {
        
        [self.insideTiles enumerateObjectsUsingBlock:^(ATTileView *insideTileView, BOOL *stop) {
            
            CGFloat distanceSquared;
            
            //Top Left (Stationary)
            CGPoint topLeftPoint = [insideTileView topLeftPoint];
            
            //Top Left vs bottom left
            distanceSquared = distanceSquaredBetweenPoints(topLeftPoint, [tileView bottomLeftPoint]);
            if ( distanceSquared < distanceSquaredThreshold && distanceSquared < smallestDistanceFound) {
                smallestDistanceFound = distanceSquared;
                found = YES;
                foundGridPoint = topLeftPoint;
            }
            
            //Top Left vs Bottom Right
            distanceSquared = distanceSquaredBetweenPoints(topLeftPoint, [tileView bottomRightPoint]);
            if ( distanceSquared < distanceSquaredThreshold && distanceSquared < smallestDistanceFound) {
                smallestDistanceFound = distanceSquared;
                found = YES;
                foundGridPoint = CGPointMake(topLeftPoint.x - [tileView trueWidth], topLeftPoint.y);
            }
            
            //Top Left vs Top Right
            distanceSquared = distanceSquaredBetweenPoints(topLeftPoint, [tileView topRightPoint]);
            if ( distanceSquared < distanceSquaredThreshold && distanceSquared < smallestDistanceFound) {
                smallestDistanceFound = distanceSquared;
                found = YES;
                foundGridPoint = CGPointMake(topLeftPoint.x - [tileView trueWidth], topLeftPoint.y + [tileView trueHeight]);
            }
            
            
            //Top Right (Stationary)
            CGPoint topRightPoint = [insideTileView topRightPoint];
            
            //Top Right vs bottom right
            distanceSquared = distanceSquaredBetweenPoints(topRightPoint, [tileView bottomRightPoint]);
            if ( distanceSquared < distanceSquaredThreshold && distanceSquared < smallestDistanceFound) {
                smallestDistanceFound = distanceSquared;
                found = YES;
                foundGridPoint = CGPointMake(topRightPoint.x - [tileView trueWidth], topRightPoint.y);
            }
            
            //Top Right vs bottom left
            distanceSquared = distanceSquaredBetweenPoints(topRightPoint, [tileView bottomLeftPoint]);
            if ( distanceSquared < distanceSquaredThreshold && distanceSquared < smallestDistanceFound) {
                smallestDistanceFound = distanceSquared;
                found = YES;
                foundGridPoint = CGPointMake(topRightPoint.x, topRightPoint.y);
            }
            
            //Top Right vs Top Left
            distanceSquared = distanceSquaredBetweenPoints(topRightPoint, [tileView topLeftPoint]);
            if ( distanceSquared < distanceSquaredThreshold && distanceSquared < smallestDistanceFound) {
                smallestDistanceFound = distanceSquared;
                found = YES;
                foundGridPoint = CGPointMake(topRightPoint.x, topRightPoint.y + [tileView trueHeight]);
            }
            
            //Bottom Left (Stationary)
            CGPoint bottomLeftPoint = [insideTileView bottomLeftPoint];
            
            //Bottom Left Vs Bottom Right
            distanceSquared = distanceSquaredBetweenPoints(bottomLeftPoint, [tileView bottomRightPoint]);
            if ( distanceSquared < distanceSquaredThreshold && distanceSquared < smallestDistanceFound) {
                smallestDistanceFound = distanceSquared;
                found = YES;
                foundGridPoint = CGPointMake(bottomLeftPoint.x - [tileView trueWidth], bottomLeftPoint.y);
            }
            
            //Bottom Left Vs Top Right
            distanceSquared = distanceSquaredBetweenPoints(bottomLeftPoint, [tileView topRightPoint]);
            if ( distanceSquared < distanceSquaredThreshold && distanceSquared < smallestDistanceFound) {
                smallestDistanceFound = distanceSquared;
                found = YES;
                foundGridPoint = CGPointMake(bottomLeftPoint.x - [tileView trueWidth], bottomLeftPoint.y + [tileView trueHeight]);
            }
            
            //Bottom Left Vs Top Left
            distanceSquared = distanceSquaredBetweenPoints(bottomLeftPoint, [tileView topLeftPoint]);
            if ( distanceSquared < distanceSquaredThreshold && distanceSquared < smallestDistanceFound) {
                smallestDistanceFound = distanceSquared;
                found = YES;
                foundGridPoint = CGPointMake(bottomLeftPoint.x, bottomLeftPoint.y + [tileView trueHeight]);
            }
            
            //Bottom Right (Stationary)
            CGPoint bottomRightPoint = [insideTileView bottomRightPoint];
            
            //Bottom Right Vs Top Right
            distanceSquared = distanceSquaredBetweenPoints(bottomRightPoint, [tileView topRightPoint]);
            if ( distanceSquared < distanceSquaredThreshold && distanceSquared < smallestDistanceFound) {
                smallestDistanceFound = distanceSquared;
                found = YES;
                foundGridPoint = CGPointMake(bottomRightPoint.x - [tileView trueWidth], bottomRightPoint.y + [tileView trueHeight]);
            }
            
            //Bottom Right vs Top Left
            distanceSquared = distanceSquaredBetweenPoints(bottomRightPoint, [tileView topLeftPoint]);
            if ( distanceSquared < distanceSquaredThreshold && distanceSquared < smallestDistanceFound) {
                smallestDistanceFound = distanceSquared;
                found = YES;
                foundGridPoint = CGPointMake(bottomRightPoint.x, bottomRightPoint.y + [tileView trueHeight]);
            }
            
            //Bottom Right vs Bottom Left
            distanceSquared = distanceSquaredBetweenPoints(bottomRightPoint, [tileView bottomLeftPoint]);
            if ( distanceSquared < distanceSquaredThreshold && distanceSquared < smallestDistanceFound) {
                smallestDistanceFound = distanceSquared;
                found = YES;
                foundGridPoint = CGPointMake(bottomRightPoint.x, bottomRightPoint.y);
            }
            
        }];
        
    }
    
    
    if (found) {
        
        [UIView animateWithDuration:.1
                              delay:0.0
                            options:UIViewAnimationOptionAllowAnimatedContent
                         animations:^{
                             
                             tileView.center = CGPointMake(foundGridPoint.x + [tileView trueWidth]/2.0, foundGridPoint.y - [tileView trueHeight]/2.0);
                             
                         }
                         completion:^(BOOL finished) {
                             
                         }];
        
    }
    
    
}

-(NSArray*)tilesCurrentlyOnLeftOrderedByY {
    
    NSSet *pannedTilesCurrentlyOnLeft = [self.pannedTiles objectsPassingTest:^BOOL(ATTileView *tileView, BOOL *stop) {
        
        CGPoint convertedCenter = [tileView.superview convertPoint:tileView.center toView:self];
        return (convertedCenter.x < OUTSIDE_TILESPACE) && (convertedCenter.x >= 0) && (convertedCenter.y <= self.bounds.size.height - OUTSIDE_TILESPACE) && [tileView isSnapableToOutSideAreaModel];
        
    }];
    
    NSSet *tilesCurrentlyOnLeft = [self.leftTiles setByAddingObjectsFromSet:pannedTilesCurrentlyOnLeft];
    NSArray *leftTilesOrderedByY = [[tilesCurrentlyOnLeft allObjects] sortedArrayUsingComparator:^NSComparisonResult (ATTileView *tileView, ATTileView *otherTileView) {
        
        CGPoint convertedCenter = [tileView.superview convertPoint:tileView.center toView:self];
        CGPoint otherConvertedCenter = [otherTileView.superview convertPoint:otherTileView.center toView:self];
        
        if (convertedCenter.y > otherConvertedCenter.y) {
            return NSOrderedAscending;
        } else {
            return NSOrderedDescending;
        }
        
    }];
    
    return leftTilesOrderedByY;
}

-(NSArray *) tilesCurrentlyOnBottomOrderedByX {
    NSSet *pannedTilesCurrentlyOnBottom = [self.pannedTiles objectsPassingTest:^BOOL(ATTileView *tileView, BOOL *stop) {
        
        CGPoint convertedCenter = [tileView.superview convertPoint:tileView.center toView:self];
        return (convertedCenter.y > self.bounds.size.height - OUTSIDE_TILESPACE) && (convertedCenter.y < self.bounds.size.height) && (convertedCenter.x >= OUTSIDE_TILESPACE) && [tileView isSnapableToOutSideAreaModel];
        
    }];
    
    NSSet *tilesCurrentlyOnBottom = [self.bottomTiles setByAddingObjectsFromSet:pannedTilesCurrentlyOnBottom];
    NSArray *bottomTilesOrderedByX = [[tilesCurrentlyOnBottom allObjects] sortedArrayUsingComparator:^NSComparisonResult (ATTileView *tileView, ATTileView *otherTileView) {
        
        CGPoint convertedCenter = [tileView.superview convertPoint:tileView.center toView:self];
        CGPoint otherConvertedCenter = [otherTileView.superview convertPoint:otherTileView.center toView:self];
        
        if (convertedCenter.x < otherConvertedCenter.x) {
            return NSOrderedAscending;
        } else {
            return NSOrderedDescending;
        }
        
    }];
    
    return bottomTilesOrderedByX;
}

-(void) snapTilesToOutsideGrid {
    
    //On Left
    //Iterate through left tiles & panning tile views
    NSArray *leftTilesOrderedByY = [self tilesCurrentlyOnLeftOrderedByY];
    NSArray *bottomTilesOrderedByX = [self tilesCurrentlyOnBottomOrderedByX];
    
    [UIView animateWithDuration:.1
                          delay:0.0
                        options:UIViewAnimationOptionAllowAnimatedContent
                     animations:^{
                         
                         //Left Tiles
                         CGFloat currentY = self.bounds.size.height - OUTSIDE_TILESPACE;
                         for (ATTileView *tile in leftTilesOrderedByY) {
                             
                             if (![self.pannedTiles containsObject:tile]) {
                                 CGFloat currentX = OUTSIDE_TILESPACE - tile.bounds.size.height/2.0;
                                 tile.center = CGPointMake(currentX, currentY - tile.bounds.size.width/2.0);
                                 
                             }
                             
                             currentY -= tile.bounds.size.width;
                         }
                         
                         //Bottom Tiles
                         CGFloat currentX = OUTSIDE_TILESPACE;
                         
                         for (ATTileView *tile in bottomTilesOrderedByX) {
                             
                             if (![self.pannedTiles containsObject:tile]) {
                                 CGFloat currentY = self.bounds.size.height - OUTSIDE_TILESPACE + tile.bounds.size.height/2.0;
                                 tile.center = CGPointMake(currentX + tile.bounds.size.width/2.0, currentY);
                                 
                             }
                             
                             currentX += tile.bounds.size.width;
                         }
                         
                         [self adjustOutsideLabels];
                         
                     }
                     completion:^(BOOL finished) {
                         
                     }];
    
    
    
}

-(void) adjustOutsideLabels {
    
    NSArray *leftTilesOrderedByY = [self tilesCurrentlyOnLeftOrderedByY];
    NSArray *bottomTilesOrderedByX = [self tilesCurrentlyOnBottomOrderedByX];
    
    NSMutableSet *labelsToRemove = [NSMutableSet set];
    for (ATTileLengthLabel *label in self.lengthLabels) {
        
        if (![self.leftTiles containsObject:label.attachedTileView] && ![self.bottomTiles containsObject:label.attachedTileView]) {
            [labelsToRemove addObject:label];
        }
    
    }
    
    for (ATTileLengthLabel *labelToRemove in labelsToRemove) {
        [labelToRemove removeFromSuperview];
        [self.lengthLabels removeObject:labelsToRemove];
    }
        
    for (ATTileView *tileView in leftTilesOrderedByY) {
        
        if (![self.pannedTiles containsObject:tileView]) {
            
            ATTileLengthLabel *label = [self dequeueLengthLabelForTile:tileView];
            label.center = CGPointMake(label.bounds.size.width/2.0, tileView.center.y);
            label.titleLabel.text = [tileView tileLengthTitleForSide:CGRectMinXEdge];
        }
        
    }
    
    for (ATTileView *tileView in bottomTilesOrderedByX) {
        
        if (![self.pannedTiles containsObject:tileView]) {
            
            ATTileLengthLabel *label = [self dequeueLengthLabelForTile:tileView];
            label.center = CGPointMake(tileView.center.x, self.bounds.size.height - label.bounds.size.height/2.0);
            label.titleLabel.text = [tileView tileLengthTitleForSide:CGRectMinYEdge];
        }
        
        
    }
    
}

-(ATTileLengthLabel*)dequeueLengthLabelForTile:(ATTileView*) tileView {
    
    __block ATTileLengthLabel *labelToReturn = nil;
    [self.lengthLabels enumerateObjectsUsingBlock:^(ATTileLengthLabel *label, BOOL *stop) {
        
        if (label.attachedTileView == tileView) {
            labelToReturn = label;
            *stop = YES;
        }
        
    }];
    
    if (!labelToReturn) {
        
        labelToReturn = [[ATTileLengthLabel alloc] initWithFrame:CGRectMake(0.0, 0.0, OUTSIDE_TILESPACE - AT_UNIT_LENGTH, OUTSIDE_TILESPACE - AT_UNIT_LENGTH)];
        labelToReturn.backgroundColor = [UIColor clearColor];
        labelToReturn.alpha = 1.0;
        labelToReturn.attachedTileView = tileView;
    }
    
    [self.lengthLabels addObject:labelToReturn];
    [self addSubview:labelToReturn];
    
    return labelToReturn;
}

@end
