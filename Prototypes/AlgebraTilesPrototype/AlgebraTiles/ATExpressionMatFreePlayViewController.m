//
//  ATFreePlayMatViewController.m
//  AlgebraTiles
//
//  Created by Reese McLean on 12/8/12.
//  Copyright (c) 2012 Reese McLean. All rights reserved.
//

#import "ATExpressionMatFreePlayViewController.h"

#import "ATExpressionMatView.h"
#import "ATExpressionModel.h"

@interface ATExpressionMatFreePlayViewController ()

@end

@implementation ATExpressionMatFreePlayViewController

+(NSString*)storyboardID {
    return @"ATExpressionMatFreePlayViewController";
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) updateExpressionLabel {
    
    ATExpressionModel *expressionModel = [self.expressionMat expressionModel];
    NSString *stringValue = [expressionModel expressionStringValue];
    
    self.expressionLabel.text = stringValue;
    
}

#pragma mark Tile View Delegate

-(void) didStartTileViewPan:(ATTileView*)tileView {
    
    [super didStartTileViewPan:tileView];
}

-(void) didPanTileView:(ATTileView*)tileView withTranslation:(CGPoint)translation {
    [super didPanTileView:tileView withTranslation:translation];
}

-(void) didFinishTileViewPan:(ATTileView*)tileView {
    
    [super didFinishTileViewPan:tileView];
    [self updateExpressionLabel];
}

-(void) didFlipTile:(ATTileView*)tileView {
    
    [super didFlipTile:tileView];
    
    [self updateExpressionLabel];
    
}

@end
