//
//  ATTileView.h
//  AlgebraTiles
//
//  Created by Reese McLean on 12/7/12.
//  Copyright (c) 2012 Reese McLean. All rights reserved.
//

#import <UIKit/UIKit.h>

extern NSString * const kATDidSpawnTileViewNotification;

@protocol ATTileViewDelegate;

typedef void (^CompletionBlock)();

@class ATTileModel;

@interface ATTileView : UIView

+(id) tileWithTileModel:(ATTileModel*)tileModel;
+(CGSize) sizeForTileModel:(ATTileModel*) tileModel;

@property (nonatomic, weak) id<ATTileViewDelegate> delegate;

@property (nonatomic, assign) BOOL spawnsNewTiles;
@property (nonatomic, assign) BOOL canFlipToNegative;

-(void) updateTileColorAnimated:(BOOL)animated;

@property (nonatomic, readonly) BOOL isSnapableToOutSideAreaModel;

@property (nonatomic, assign) BOOL vertical;

@property (nonatomic, copy) ATTileModel *tileModel;

@property (nonatomic, strong) UITapGestureRecognizer *tapGestureRecognizer;

-(void) rotateToVertical:(BOOL)vertical animated:(BOOL)animated;

-(BOOL) isSameTypeOfTile:(ATTileView*)tileToTest;

-(NSString*)tileLengthTitleForSide:(CGRectEdge) rectEdge;

-(CGPoint) topLeftPoint;
-(CGPoint) topRightPoint;
-(CGPoint) bottomLeftPoint;
-(CGPoint) bottomRightPoint;
-(CGFloat) trueWidth;
-(CGFloat) trueHeight;

-(void) removeAnimated:(BOOL)animated completion:(CompletionBlock)block;

+(void) addShadowToTileView:(ATTileView*)tileView;
+(void) removeShadowFromTileView:(ATTileView*)tileView;

@end

@protocol ATTileViewDelegate <NSObject>

-(void) didStartTileViewPan:(ATTileView*)tileView;
-(void) didPanTileView:(ATTileView*)tileView withTranslation:(CGPoint)translation;
-(void) didFinishTileViewPan:(ATTileView*)tileView;

@optional
-(void) didFlipTile:(ATTileView*)tileView;

@end
