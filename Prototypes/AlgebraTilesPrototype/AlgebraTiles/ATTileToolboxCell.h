//
//  ATTileToolboxCell.h
//  AlgebraTiles
//
//  Created by Reese McLean on 12/7/12.
//  Copyright (c) 2012 Reese McLean. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "TileConstants.h"
#import "ATTileView.h"

@class ATTileModel;

@interface ATTileToolboxCell : UICollectionViewCell

+(CGFloat) cellHeightForTileModel:(ATTileModel*)tileModel;

@property (weak, nonatomic) IBOutlet UILabel *label;
@property (weak, nonatomic) IBOutlet ATTileView *tileView;

@property (nonatomic, strong) ATTileModel *tileModel;

@end
