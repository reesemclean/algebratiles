//
//  ATExpressionGuidedSimplifyingViewController.h
//  AlgebraTiles
//
//  Created by Reese McLean on 12/21/12.
//  Copyright (c) 2012 Reese McLean. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ATBaseExpressionMatViewController.h"

@interface ATExpressionMatGuidedViewController : ATBaseExpressionMatViewController

@property (weak, nonatomic) IBOutlet UILabel *expressionLabel;
@end
